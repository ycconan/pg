#include "Resource.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "Character.h"
#include "CharManager.h"
#include "Items.h"
#include "ItemManager.h"
#include "WorldPacket.h"
#include "ScriptMgr.h"
#include "WSSocket.h"
#include "Opcodes.h"
#include "Users.h"
#include "UserManager.h"
#include "MD5.h"
#include "Towns.h"
#include "TownManager.h"
#include "Mails.h"
#include "MailManager.h"
#include "ChannelManager.h"
#include "Message.h"
#include "WSSocketManager.h"
#include "Tools.h"
#include "NoticeManager.h"
#include "GameGoodsManager.h"
#include "CenterBankManager.h"
#include "VipCardManager.h"
#include "DataTransferManager.h"
#include "PGOpcodesEx.h"
#include "PGManualThreadsManager.h"
#include "PGHallManager.h"
#include "PGWorldAIInterface.h"
#include "PGPubStruct.h"
#include "PGProtocolDealEnums.h"
#include "GameDefine.h"
#include ENUM_PATH
#include LOGIC_MGR_PATH
#include XMLDATA_MGR_PATH
#include PROTOCOLS_MGR_PATH
#include LOG_MGR_PATH
using namespace AIScript;

//-----------------------------------------------------------------------
bool pg_script_world_process_playercutcards(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint8 cutresult;
	packet >> cutresult;
	//Log.Notice("pg_script_world_process_playercutcards", "庄家是否切牌: %d ", cutresult);
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayerCutCards(pChr, cutresult);

	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_playercutcardspos(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint32 cutcardpos;
	packet >> cutcardpos;
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayerCutCardsPos(pChr,cutcardpos);
	//Log.Notice("pg_script_world_process_playercutcardspos", "切牌位置：%d", cutcardpos);
	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_playercutcardfinish(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayerCutCardsFinsh(pChr);
	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_applybebanker(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint8 mode;
	packet >> mode;

	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
	{
		//Log.Notice("pg_script_world_process_applybebanker", "玩家是否抢庄: %d", mode);
			pChnAiInfe->LootBanker(pChr, mode);

		//	pChnAiInfe->Ready(pChr);
	}

	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_continueorabandonbanker(ResourcePtr &sock, WorldPacket &packet) 
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint8 continueorabandon;
	packet >> continueorabandon;

	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		//Log.Notice("pg_script_world_process_continueorabandonbanker", "庄家是否切锅: %d", continueorabandon);
		pChnAiInfe->BankerAnswer(pChr, continueorabandon);

	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_chipin(ResourcePtr &sock, WorldPacket &packet) 
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	// std::map<uint32, uint32> mapNumScore;
	// uint32 score1, score2, score3;
	// packet >> score1 >> score2 >> score3;
	// mapNumScore.insert(std::make_pair(1, score1));
	// mapNumScore.insert(std::make_pair(2, score2));
	// mapNumScore.insert(std::make_pair(3, score3));

	uint32 num,score;
	packet >> num >> score;

	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	pChnAiInfe->CurNum(pChr, num);
	//Log.Notice("pg_script_world_process_chipin", "玩家%d   score1:%d score2:%d score3:%d ", pChnAiInfe->PlayerSeatNumber(pChr), score1, score2, score3)
	if (NULL != pChnAiInfe)
	pChnAiInfe->BroadPlayerChinResult(pChr, pChnAiInfe->GetNumChips(pChr), num, pChnAiInfe->AddChips(pChr, num, score, true));

	return true;
}
//-----------------------------------------------------------------------
//玩家获取提示牌型
bool pg_script_world_process_playerpromptcardsytpe(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayerPromptCardsType(pChr);

	return true;
}
//-----------------------------------------------------------------------
//小结算确认
bool pg_script_world_process_askIsroundpgcalc(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;
	uint8 ask;
	packet >> ask;
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->AskIsRoundPGCalc(pChr, ask);

	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_broadotherplayerlookcards(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint8 otherpos;
	packet >> otherpos;
	//Log.Notice("pg_script_world_process_broadotherplayerlookcards", "选择看牌玩家座位号:%d ", otherpos);
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->BroadLookOtherPlayerCards(pChr, otherpos);
		

	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_playermovecards(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	uint8 index1,index2;
	packet >> index1 >> index2;
	//Log.Notice("pg_script_world_process_playermovecards", "玩家 %d 交换牌位置(%d , %d)", pChr->getHandle(),index1,index2);
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayermoveCard(pChr, index1, index2);
	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_playermovecardsfinish(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;

	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		pChnAiInfe->PlayerMoveCardFinish(pChr);
	return true;
}
//-----------------------------------------------------------------------
bool pg_script_world_process_getoutcards(ResourcePtr &sock, WorldPacket &packet)
{
	if (sock.isNull())
		return true;

	WSSocketPtr socket = sock;
	char cCode[256] = "";
	if (!ChkSocketPacket(socket, packet, cCode))
		return true;

	CharPtr pChr = socket->getCurrentCharacter();
	if (pChr.isNull())
		return true;

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return true;

	ChannelPtr channel = aii->GetChannelPtr();
	if (channel.isNull())
		return true;
	PGChannelInterface *pChnAiInfe = TO_CHANNEL_INTERFACE(channel->getAIInterface());
	if (NULL != pChnAiInfe)
		if (sProtocolsMgr.CreateGetOutcardsPacket(&packet, pChnAiInfe->GetOutCardsList(), true))
		{
			Log.Debug("pg_script_world_process_getoutcards", "CreateGetOutcardsPacket");
			socket->SendPacket(&packet);
		}

	return true;
}

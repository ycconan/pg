#include "WorldProcess.h"
#include "ScriptMgr.h"
#include "NGLog.h"
#include "PGGameUserCommand.h"
#include "PGOpcodesEx.h"
#include "PGScriptWorldProcess.h"
#include "ScriptWorldProcess.h"

//-----------------------------------------------------------------------
void  register_custom_world_process(ScriptMgr * mgr)
{
	mgr->register_server_process(0x0010,script_world_process_authentication);
	Log.Debug("World Process", "Register opcode 0x%02x Process:[%s]",0x0010,"script_world_process_authentication");
	//当前玩家切牌位置索引
	mgr->register_server_process(CMSG_CUTCARDSPOS, pg_script_world_process_playercutcardspos);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_CUTCARDSPOS, "pg_script_world_process_playercutcardspos");
	//玩家切牌
	mgr->register_server_process(CMSG_CUTCARDS, pg_script_world_process_playercutcards);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_CUTCARDS, "pg_script_world_process_playercutcards");

	mgr->register_server_process(CMSG_CUTCARDSFINSH,pg_script_world_process_playercutcardfinish);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_CUTCARDSFINSH, "pg_script_world_process_playercutcardfinish");
	// 玩家抢庄
	mgr->register_server_process(CMSG_PG_GETBANKER, pg_script_world_process_applybebanker);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_PG_GETBANKER, "pg_script_world_process_applybebanker");
	// 连庄或让庄
	mgr->register_server_process(CMSG_CONTINUEBANKER, pg_script_world_process_continueorabandonbanker);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_CONTINUEBANKER, "pg_script_world_process_continueorabandonbanker");
	// 下注
	mgr->register_server_process(CMSG_CHIPIN, pg_script_world_process_chipin);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_CHIPIN, "pg_script_world_process_chipin");
	//玩家获取提示牌
	mgr->register_server_process(CMSG_PLAYLOOKCARD, pg_script_world_process_playerpromptcardsytpe);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_PLAYLOOKCARD, "pg_script_world_process_playerpromptcardsytpe");
	//广播玩家牌信息 
	mgr->register_server_process(CMSG_LOOKOTHERPLAYERCARD, pg_script_world_process_broadotherplayerlookcards);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_LOOKOTHERPLAYERCARD, "pg_script_world_process_broadotherplayerlookcards");
	//玩家摆牌组合
	mgr->register_server_process(CMSG_PLAYMOVECARD, pg_script_world_process_playermovecards);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_PLAYMOVECARD, "pg_script_world_process_playermovecards");
	//玩家完成摆牌操作
	mgr->register_server_process(CMSG_PLAYMOVECARDFINISH,pg_script_world_process_playermovecardsfinish);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_PLAYMOVECARDFINISH, "pg_script_world_process_playermovecardsfinish");
	// 获取出牌列表
	mgr->register_server_process(CMSG_GETOUTCARDS, pg_script_world_process_getoutcards);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_GETOUTCARDS, "pg_script_world_process_getoutcards");
	// 小结算确认
	mgr->register_server_process(CMSG_ASKISROUNDPGCALC, pg_script_world_process_askIsroundpgcalc);
	Log.Debug("World Process", "Register opcode 0x%04x Process:[%s]", CMSG_ASKISROUNDPGCALC, "pg_script_world_process_askIsroundpgcalc");
}


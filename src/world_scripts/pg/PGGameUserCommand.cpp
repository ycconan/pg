#include "PGGameUserCommand.h"
#include "Common.h"
#include "Resource.h"
#include "ResourceManager.h"
#include "NGLog.h"
#include "ChannelManager.h"
#include "WSSocket.h"
#include "BSSocket.h"
#include "WSSocketManager.h"
#include "BSSocketManager.h"
#include "WorldPacket.h"
#include "String.h"
#include "StringConverter.h"
#include "Users.h"
#include "UserManager.h"
#include "Character.h"
#include "CharManager.h"
#include "Items.h"
#include "ItemManager.h"
#include "Towns.h"
#include "MissionModelManager.h"
#include "ItemModelManager.h"
#include "TownManager.h"
#include "EffectModelManager.h"
#include "Message.h"
#include "Mails.h"
#include "MailManager.h"
#include "json/json.h"
#include <openssl/evp.h>
#include "Database/DatabaseEnv.h"
#include "NoticeManager.h"
#include "GameGoodsManager.h"
#include "MD5.h"
#include "VipCardManager.h"
#include "CenterBankManager.h"
#include "Tools.h"
#include "showip.h"
#include "PGWorldAIInterface.h"
#include "GameDefine.h"
#include ENUM_PATH
#include LOGIC_MGR_PATH
#include XMLDATA_MGR_PATH
#include PROTOCOLS_MGR_PATH
#include LOG_MGR_PATH


extern String unbase64(String input);
extern bool StringReplace(String & str,const String & src, const String & dest,String::size_type start_index = 0);
extern String DecryptBase64(String srcStr);
// /**
 // * base64解密
 // */
// #include <openssl/bio.h> 
// #include <openssl/buffer.h>
// #include <openssl/sha.h> 
// #include <openssl/hmac.h> 
// #include <openssl/err.h>
// //--------------------------------------------------------------------
// String unbase64(String input) 
// {
	// size_t length = input.size();
	// BIO *b64, *bmem; 
	// char *buffer = (char *)malloc(length); 
	// memset(buffer, 0, length); 

	// b64 = BIO_new(BIO_f_base64()); 
	// BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
	// bmem = BIO_new_mem_buf((void*)input.c_str(), length); 
	// bmem = BIO_push(b64, bmem); 

	// BIO_read(bmem, buffer, length); 

	// BIO_free_all(bmem);
	// String str(buffer);
	// free(buffer);
	// return str; 
// }
// //--------------------------------------------------------------------
// bool StringReplace(String & str,const String & src, const String & dest,String::size_type start_index = 0)
// {
	// String::size_type iter;
	// size_t srclen = src.size();
	// bool re = false;
	// while((iter = str.find(src,start_index)) != String::npos)
	// {
		// str.replace(iter, srclen, dest);
		// re = true;
	// }
	
	// return re;
// }
// //--------------------------------------------------------------------
// String DecryptBase64(String srcStr)
// {
	// String outStr = unbase64(srcStr);
	// StringReplace(outStr, " ", "+");
	// return outStr;
// }

using namespace AIScript;
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_register_user(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_register_user","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());	
	String str = command->parameter;
	
	Resource * user = sUserMgr.newDumyUser();
	if(!user)
		return SCR_RETURN_ERROR;
		
	String user_name;
	std::vector<String> ret = StringUtil::split(str,"=:");
	uint8 times = ret.size() / 2;
	for(uint8 i=0; i<times; ++i)
	{
		user_name = sPubLogic.RvRpDBCharacter(ret[i*2 + 1], false);
		if(ret[i*2].compare("name"))
		{
			if(sUserMgr.getDBCount("name = '%s'", ServerDatabase.EscapeString(user_name).c_str()))
			{
				sUserMgr.freeDumyUser(user);
				return SCR_RETURN_ERROR;
			}
		}
		user->setStringField(ServerDatabase.EscapeString(ret[i*2]),ServerDatabase.EscapeString(user_name));
	}

	user->setUInt32Field("reg_time",time(0));
	user->setUInt32Field("points",0);
	user->setUInt32Field("max_points",0);
	user->SaveDB();
	
	UserPtr userPtr = sUserMgr.loadUserDatabase(user);
	sUserMgr.freeDumyUser(user);
	if(userPtr.isNull())
		return SCR_RETURN_ERROR;
	uint32 uid = userPtr->getUInt32Field("uid");
	uint32 reg_time = userPtr->getUInt32Field("reg_time");
	
	String result = "uid=" + StringConverter::toString(uid) + ":" + "reg_time=" + StringConverter::toString(reg_time);
	command->result = result;
	Log.Debug("pg_script_process_world_game_command_register_user","==================result[%s]",result.c_str());
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_reuser(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reuser","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	String str = command->parameter;
	
	uint32 serial = command->uid;
	UserPtr user = sUserMgr.getByHandle(serial);
	if(user.isNull())
	{
		user = sUserMgr.load(serial);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	WSSocketPtr socket = user->m_Socket.getResourcePtr();
	if(!socket.isNull())
		socket->Disconnect();
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (json_object.isMember("passwd"))
		user->setStringField("passwd", ServerDatabase.EscapeString(unbase64(json_object["passwd"].asString())));
	if (json_object.isMember("passwd2"))
		user->setStringField("passwd2",ServerDatabase.EscapeString(unbase64(json_object["passwd2"].asString())));
	if (json_object.isMember("nick"))
		user->setStringField("nick",ServerDatabase.EscapeString(unbase64(json_object["nick"].asString())));
	if (json_object.isMember("email"))
		user->setStringField("email",ServerDatabase.EscapeString(unbase64(json_object["email"].asString())));
	if (json_object.isMember("group_id"))
		user->setUInt32Field("group_id",json_object["group_id"].asUInt());
	if (json_object.isMember("flag"))
		user->setUInt32Field("flag",json_object["flag"].asUInt());
	if (json_object.isMember("vip"))
		user->setUInt32Field("vip",json_object["vip"].asUInt());
	
	user->SaveDB();
	Log.Debug("pg_script_process_world_game_command_notice","command id[%u] excuted!", command->cmdid);
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_deluser(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_deluser","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	String str = command->parameter;
	
	uint32 serial = command->uid;
	UserPtr user = sUserMgr.getByHandle(serial);
	if(user.isNull())
	{
		user = sUserMgr.load(serial);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}

	WSSocketPtr socket = user->m_Socket.getResourcePtr();
	if(!socket.isNull())
		socket->Disconnect();

	uint32 char_id = user->getUInt32Field("char_id");
	if(char_id)
	{
		CharPtr chr = sCharMgr.getByHandle(char_id);
		if(chr.isNull())
		{
			chr = sCharMgr.load(char_id);
			if(!chr.isNull())
			{
				chr->Destroy();
			}
		}
	}
	user->Destroy();
	Log.Notice("pg_script_process_world_game_command_deluser","user[%u] destroy sucessfully!",serial);
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_userbuy(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;

	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_userbuy","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	String str = command->parameter;

	uint32 serial = command->uid;
	Log.Notice("pg_script_process_world_game_command_userbuy","command user id[%u]",serial);
	UserPtr user = sUserMgr.getByHandle(serial);
	if(user.isNull())
	{
		user = sUserMgr.load(serial);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	uint32 golds = json_object["golds"].asUInt();
	user->setUInt32Field("golds",user->getUInt32Field("golds") + golds);
	user->SaveDB();
	// 公告
	// char buf[256];
	// memset(buf, 0, sizeof(buf));
	// sprintf(buf, sPubLogic.GetMsgData("buy_golds").c_str(), user->getStringField("nick").c_str(), golds);
	// sNoticeMgr.AddMsgToQueue(buf, 1, 0, 10);
	// // 通知玩家
	// ResourcePtr socket = user->m_Socket.getResourcePtr();
	// if (!socket.isNull())
	// {
		// uint8 socket_type = socket->getType();
		// if(socket_type == ResourceTypeWorldSocket)
		// {
			// WSSocketPtr wsocket = socket;
			// WorldPacket packet;
			// // 更新元宝
			// if (sProtocolsMgr.CreateGoldsUpdatePacket(&packet, user->getUInt32Field("golds")))
				// wsocket->SendPacket(&packet);
			// // 下发消息
			// sprintf(buf, sPubLogic.GetMsgData("buygolds").c_str(), golds);
			// if(sProtocolsMgr.CreateLocalMessagePacket(&packet, buf, sPubLogic.GetMsgType("buygolds")))
				// wsocket->SendPacket(&packet);
		// }
	// }
	// // 记录日志
	// sLogDataMgr.UpdateGoldsFromGatewayLog(user, en_UNT_Add, golds);
	
	Log.Notice("pg_script_process_world_game_command_userbuy","user[%u] buy golds[%u] sucessfully!",user->getHandle(),golds);
	
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_lockuser(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;

	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_lockuser","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	String str = command->parameter;

	uint32 serial = command->uid;
	UserPtr user = sUserMgr.getByHandle(serial);
	if(user.isNull())
	{
		user = sUserMgr.load(serial);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}

	WSSocketPtr socket = user->m_Socket.getResourcePtr();
	if(!socket.isNull())
		socket->Disconnect();

	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	uint32 ban_time = json_object["ban_time"].asUInt();
	user->setUInt32Field("ban_time",time(0) + ban_time);
	user->SaveDB();
	Log.Notice("pg_script_process_world_game_command_lockuser","lock user[%u] sucessfully ban_time[%u]",user->getHandle(),ban_time);
	
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_unlockuser(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;

	GatewayCommand * command = (GatewayCommand *)info;
	Log.Notice("pg_script_process_world_game_command_unlockuser","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	String str = command->parameter;
	
	uint32 serial = command->uid;
	UserPtr user = sUserMgr.getByHandle(serial);
	if(user.isNull())
	{
		user = sUserMgr.load(serial);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	user->setUInt32Field("ban_time",0);
	user->SaveDB();
	
	Log.Notice("pg_script_process_world_game_command_unlockuser","UnLock user[%u] sucessfully!",user->getHandle());
	
	return SCR_RETURN_SUCCEED;
}

//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_notice(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;

	GatewayCommand * command = (GatewayCommand *)info;
	Log.Notice("pg_script_process_world_game_command_notice","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	// 用户ID
	uint32 uid = command->uid;
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	// 消息内容
	String strMsg(unbase64(json_object["message"].asString()));
	// 分区ID
	uint32 gzId = json_object["gz_id"].asUInt();
	// 消息类型
	uint32 type = json_object["emstype"].asUInt();
	if (uid)
		sPubLogic.SendMsgToPlayer(uid, strMsg, type); // 给某个玩家发信息
	else
	{
		// 起始日期
		uint32 startDate = json_object["start"].asUInt();
		// 结束日期
		uint32 finishDate = json_object["finish"].asUInt();
		// 开始时间
		uint32 beginTime = json_object["begin"].asUInt();
		// 结束时间
		uint32 endTime = json_object["end"].asUInt();
		// 频率
		uint32 interval = json_object["interval"].asUInt();
		sNoticeMgr.AddMsgToQueue(strMsg, 101, gzId, interval, startDate, finishDate, beginTime, endTime); // 公告
	}
	Log.Notice("pg_script_process_world_game_command_notice","command id[%u] excuted!", command->cmdid);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_additem(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_additem","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
			return SCR_RETURN_ERROR;
	}
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("script_id"))
		return SCR_RETURN_ERROR;
	uint32 modelId = json_object["script_id"].asUInt();
	if(0 == modelId)
		return SCR_RETURN_ERROR;
	// 数量
	uint32 num = json_object["num"].asUInt();
	// 消息
	String strMsg;
	if (json_object.isMember("msg"))
		strMsg = unbase64(json_object["msg"].asString());
	// 存入数据库
	ItemPtr item = sPubLogic.addItemNum(chr, modelId, num);
	// ItemPtr item = sPubLogic.CreateItemByScriptID(chr, script_id);
	if(item.isNull())
		return SCR_RETURN_ERROR;
	if (strMsg != "")
	{
		sPubLogic.SendMsgToPlayer(user_id, strMsg, 11);
	}
	// 记录日志
	sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_Gateway, modelId, en_UNT_Add, num);
	Log.Notice("pg_script_process_world_game_command_additem","user[%u] additem scriptid[%u] num[%u] sucessfully!",user->getHandle(), modelId,num);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_deleteitem(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_deleteitem","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
			return SCR_RETURN_ERROR;
	}
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	uint32 serial = json_object["serial"].asUInt();
	if(!serial)
		return SCR_RETURN_ERROR;

	ItemPtr item = sItemMgr.load(serial);
	if(item.isNull() || item->getUInt32Field("container") != char_id)
		return SCR_RETURN_ERROR;
	uint32 modelId = item->getUInt32Field("model_id");
	item->Destroy();
	// 记录日志
	sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_Gateway, modelId, en_UNT_Delete);
	Log.Notice("pg_script_process_world_game_command_deleteitem","delete user[%u]'s item sucessfully!",user->getHandle(),serial);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_addmail(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_addmail","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;

	GameMail *pMail = sChannelMgr.newDumyMail();
	if (NULL == pMail)
		return SCR_RETURN_ERROR;
	pMail->group_id = command->group_id; // 邮件组ID
	pMail->status = Mail::MailStatusRecv; // 邮件状态
	pMail->recv_serial = user->getUInt32Field("platform_id"); // 接收方用户ID
	pMail->title = ServerDatabase.EscapeString(DecryptBase64(json_object["title"].asString())); // 标题
	String strData = json_object["content"].asString();
	String outData = unbase64(strData);
	String::size_type start = outData.find("\n\n");
	StringReplace(outData, " ", "+",start);
	if (json_object["flag"].asUInt() == 1)
	{
		pMail->flag = 0x80;
		pMail->contenct = outData; // 内容
	}
	else
		pMail->contenct = ServerDatabase.EscapeString(outData); // 内容
		
	pMail->data1 = user->getUInt32Field("gz_id"); // 分区ID
	pMail->create_time = time(0); // 创建时间
	// 清理邮箱
	if (!sPubLogic.CleanUserMails(pMail->recv_serial))
	{
		Log.Error("pg_script_process_world_game_command_addmail", "clean user[%d] mail box faild!", user_id);
		return SCR_RETURN_ERROR;
	}
	// 保存到数据库
	if ((sChannelMgr.addMailDB(pMail) == 0))
	{
		Log.Error("pg_script_process_world_game_command_addmail", "save user[%d] mail box to DB faild!", user_id);
		return SCR_RETURN_ERROR;
	}
	
	sChannelMgr.freeDumyMail(pMail);
	// 添加成功，通知玩家
	WSSocketPtr socket = user->m_Socket.getResourcePtr();
	if (!socket.isNull())
	{
		WorldPacket packet;
		if (sProtocolsMgr.CreateSendUnreadMailNumberPacket(&packet, user))
			socket->SendPacket(&packet);
		// // 下发消息通知玩家
		// if(sProtocolsMgr.CreateLocalMessagePacket(&packet, sPubLogic.GetMsgData("newmail"), sPubLogic.GetMsgType("newmail")))
			// socket->SendPacket(&packet);
	}
	Log.Notice("pg_script_process_world_game_command_addmail","add user[%u]'s mail sucessfully!",user_id);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_repwd2(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadgameconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}	
	CMD5 md5;
	String pwd = md5.GenerateMD5(StringConverter::toString(sXmlDataMgr.GetConfXMLValue("2PWD_DEFAULT")).c_str());
	sBankMgr.SetPassworld(user, pwd);
	
	Log.Notice("pg_script_process_world_game_command_repwd2","repwd2 user[%u]'s item sucessfully!",user_id);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_userbuyitems(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_userbuyitems","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
		{
			chr = sPubLogic.CreateCreature(user, true);
			if (chr.isNull())
				return SCR_RETURN_ERROR;
		}
	}
	PGCreatureInterface *pAiChrInfe = TO_CREATURE_INTERFACE(chr->getAIInterface());
	if (!pAiChrInfe)
		return SCR_RETURN_ERROR;
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	std::vector<std::string> vecKeys = json_object.getMemberNames();
	std::vector<std::string>::iterator iter = vecKeys.begin();
	String strContent;
	uint32 amount = 0;
	uint32 curTime = time(0);
	WorldPacket packet;
	bool bOk = true;
	char buf[256];
	for (; iter != vecKeys.end(); ++iter)
	{
		if (StringUtil::startsWith((*iter),"addi"))
		{
			uint32 op;
			std::vector<String> strInfo = StringUtil::split(unbase64(json_object[(*iter)].asString()),"|");
			// 操作	
			uint32 size = strInfo.size();
			if(size < 3)
				return SCR_RETURN_ERROR;
			else if(size == 3)
			{
				op = 1;
			}
			else
			{
				op = StringConverter::parseUnsignedInt(strInfo[3]);
				if(op == 0)
					op = 1;
			}
			
			if(strInfo[0] == "" || strInfo[1] == "")
				return SCR_RETURN_ERROR;
			bool isMammon = true;	// 财神是否生效
			bool isDiscount = false; // 是否加送铜钱
			bool isLargess = true;	// 是否送金转盘次数			
			uint32 modelId = StringConverter::parseUnsignedInt(strInfo[0]);	// 模式ID
			uint32 goodsId = modelId;		// 商品ID	
			uint32 num = StringConverter::parseUnsignedInt(strInfo[1]);	// 数量
			uint32 nBaseNumber = num;	// 基础数量
			if (modelId < en_ItemModelId_Coins)
			{
				// vip卡ID
				VIPCard vipCard = sVipCardMgr.GetVipCard(modelId);
				if (0 == vipCard.id)
					return SCR_RETURN_ERROR;
				// 是否可购买更多			
				if ((vipCard.flag & en_VipFlag_NoMore) && 
					sVipCardMgr.HaveVIPCard(chr, modelId))
					return SCR_RETURN_ERROR;
				if (vipCard.type == en_VipType_Body)
				{// 形象
					uint32 bodyid = vipCard.money;
					pAiChrInfe->AddCreatureBody(bodyid, curTime + (vipCard.days*60*60*24));
					chr->setUInt32Field("id", bodyid);
					if (sProtocolsMgr.CreateBodyListPacket(&packet, chr))
						sPubLogic.SendProtocolsToChr(chr, &packet);
					vipCard.money = 0;
				}
				else
				{
					isDiscount = (vipCard.flag & en_VipFlag_Discount);
					vipCard.coins *= num;
					vipCard.money *=num;
					vipCard.days *= num;
					vipCard.morraNum *= num;
					pAiChrInfe->AddNewVIPInfo(vipCard);
				}
				
				//********************乐码相关*******************************//
				if(modelId >= 551 && modelId <= 576)	//打折会员
				{
					uint32 exchange_id = 0;
					uint32 pid = user->getUInt32Field("platform_id");
					uint32 count = sItemMgr.getExchangeDBCount(" (source = %u and status = %u)",
														pid,enExchangeStataus_YanZheng);
					if(count)
					{
						std::list<GameExchange> exchanges;
						sItemMgr.getExchangeListDB(&exchanges,0,count,"(source = %u and status = %u)",
															pid,enExchangeStataus_YanZheng);
						
						if(exchanges.size())
						{
							GameExchange gameEx = exchanges.front();
							exchange_id = gameEx.exchange_id;
							gameEx.status = enExchangeStataus_Exchaned;
							gameEx.token_user = pid;
							gameEx.token_time = sTools.GetCurDateTime();
							sItemMgr.updateExchangeDB(&gameEx);
							
							WSSocketPtr socket = chr->getSocket();
							if(!socket.isNull() && sProtocolsMgr.CreateLeCardsStatusPacket(&packet,gameEx.exchange_id,gameEx.status))
								socket->SendPacket(&packet);
						}
					}
					//使用日志
					sBankMgr.LeMaOpLog(pid,en_LMOM_Use,exchange_id,modelId);
					sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_Gateway_Buy, modelId, en_UNT_Add,num,exchange_id);
				}
				//********************乐码相关*******************************//
				uint32 now_time = time(0);
				uint32 give_luckyEndTime = sXmlDataMgr.GetConfXMLValue("BUTVIP_GIVELUCKY_CARD_ENDTIME");
				if(!give_luckyEndTime || (give_luckyEndTime && give_luckyEndTime >= time(0)))	//充值送财神卡 活动开关
				{
					if(!sTools.IsSameDate(pAiChrInfe->GetXMLData("USERBUYVIP_TIME"),now_time))
					{
						uint32 random_num = RandomUInt(100);
						uint32 lucky_model = ItemModel_Luck;
						if(random_num >= sXmlDataMgr.GetConfXMLValue("SUPER_LUCKY_PERCENT"))	//送超级幸运卡
						{
							lucky_model = ItemModel_Luck3;
						}
						else if(random_num >= sXmlDataMgr.GetConfXMLValue("BIG_LUCKY_PERCENT"))	//送大幸运卡
						{
							lucky_model = ItemModel_Luck2;
						}
						
						ItemModelPtr modelPtr = sItemModelMgr.getByHandle(lucky_model);
						if(!modelPtr.isNull())
						{
							sPubLogic.addItemNum(chr,lucky_model,1);
							sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_GiftBag, lucky_model, en_UNT_Add,1,en_GiftType_UserBuy);
							char msg[256] = "";
							sprintf(msg,sMsg.getMessage("BUYGIFT_MSG1").c_str(),modelPtr->getStringField("name").c_str(),1);
							WSSocketPtr socket = chr->getSocket();
							if(!socket.isNull() && sProtocolsMgr.CreateLocalMessagePacket(&packet, msg, sMsg.getType("BUYGIFT_MSG1")))
								socket->SendPacket(&packet);
						}
					}
				}
				
				pAiChrInfe->SetXMLData("USERBUYVIP_TIME",now_time);
				chr->SaveDB();
				
				if (vipCard.type == en_VipType_FirstBuy)
				{// 作首充标记
					sBankMgr.AddPublicData(user, en_PublicData_FirstBuy, num, en_ST_Gateway_Buy);
					sBankMgr.SetPublicData(user, en_PublicData_FirstBuyTimer, now_time);
					WSSocketPtr socket = chr->getSocket();
					if (!socket.isNull() && sProtocolsMgr.CreateTellToDoPacket(&packet, en_RemindType_CloseFirstBuy))
						socket->SendPacket(&packet);
				}
				
				if(vipCard.coins)
				{
					num = vipCard.coins;
					modelId = en_ItemModelId_Coins;
				}
				else
				{
					modelId = en_ItemModelId_Gold;
					num = vipCard.money;
				}
				// 财神卡是生效
				isMammon = !(vipCard.flag & en_VipFlag_NoMammon);
			}
	
			// 封装物品
			memset(buf, 0, sizeof(buf));
			ItemModelPtr pIModel = sItemModelMgr.load(modelId);
			if(pIModel.isNull())
				return SCR_RETURN_ERROR;
					
			if(op == 1)
			{
				if (modelId == en_ItemModelId_Coins)
				{
					uint32 n_time = time(0);
					uint32 begin_time = sXmlDataMgr.GetConfXMLValue("BUY_ACTIVITY_BEGIN");
					uint32 end_time = sXmlDataMgr.GetConfXMLValue("BUY_ACTIVITY_END");
					if(isDiscount && begin_time && end_time && n_time >= begin_time && n_time <= end_time)
					{
						isLargess = false;	// 打折商品，不能与金转盘
						num *= ((float)sXmlDataMgr.GetConfXMLValue("BUY_PREFER_PERCENT") * 0.01f);						
					}
					uint32 temp = num;
					if (isMammon && sPubLogic.ReduceMammonCard(chr, 1, num))
					{	
						if (sProtocolsMgr.CreateUsedMammonCardPacket(&packet, num-temp))
						{
							sPubLogic.SendProtocolsToChr(chr, &packet);
						}
					}
					// 加钱
					sPubLogic.addItemNum(chr, modelId, num);	
				}
				else if (pIModel->getUInt32Field("type") == 1 && pIModel->getUInt32Field("sub_type") == 1)
				{
					sPubLogic.AddMammonCard(chr, modelId, num);
				}
				else
				{
					if (pIModel->getUInt32Field("type") != 2)
					{
						uint32 base = pIModel->getUInt32Field("value");
						num *= (base?base:1);
					}
					sPubLogic.addItemNum(chr, modelId, num);
				}
				// 记录日志
				sLogDataMgr.UpdateItemsByUserBuyLog(chr, modelId, num);
			}
			else if(op == 2)
			{// 扣钱
				if (sPubLogic.reduceItemNum(chr, modelId, num))
				{	
					sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_Gateway, modelId, en_UNT_Reduce, num);
				}
				else
				{
					bOk = false;
				}
			}
			
			sprintf(buf, "%sx%u ", pIModel->getStringField("name").c_str(), num);
			strContent += buf;
			
			/*********************-- 金转盘及土豪经验 --*********************/
			uint32 begin_act = sXmlDataMgr.GetConfXMLValue("ACTIVITY_BEGIN");
			uint32 end_act = sXmlDataMgr.GetConfXMLValue("ACTIVITY_END");			
			const BuyLargess *pBuyLargess = sXmlDataMgr.GetBuyLargess(goodsId);
			if (pBuyLargess != NULL)
			{
				WSSocketPtr socket = chr->getSocket();
				if (isLargess && begin_act && curTime >= begin_act && curTime < end_act)
				{// 赠送金转盘次数，活动商品不参与				
					uint32 relNum = (uint32)ceil(0.0001 * nBaseNumber * pBuyLargess->number);
					uint32 largess = sBankMgr.AddPublicData(user, pBuyLargess->largess, relNum, en_ST_Gateway_Buy);					
					Log.Debug("pg_script_process_world_game_command_userbuyitems","user[%u] AddPublicData[%u] currnum[%u]", 
						user->getHandle(), pBuyLargess->largess, largess);					
					if (!socket.isNull() &&  
					sProtocolsMgr.CreateCharSpecificPacket(&packet, chr->getHandle(), en_CharSpecific_BuyLargess, pBuyLargess->largess, relNum))
					{
						socket->SendPacket(&packet);
					}
				}
				// 土豪经验
				uint32 exp = (uint32)ceil(0.01 * nBaseNumber * pBuyLargess->exp);
				pAiChrInfe->AddXMLValue(TUHAO_EXPS, exp);
			}
			/*********************-- 金转盘及土豪经验 --*********************/
		}
		else if (StringUtil::startsWith((*iter),"message"))
		{
			// 消息内容
			String strMsg(unbase64(json_object[*iter].asString()));
			// 发信息
			sPubLogic.SendMsgToPlayer(user_id, strMsg, 1);
		}
		else if (StringUtil::startsWith((*iter),"amount"))
		{
			amount = uint32(StringConverter::parseReal(unbase64(json_object[*iter].asString())) * 100.0f);
		}
	}

	// 记录结果日志
	sLogDataMgr.GatwayBuyResultLog(chr, strContent, amount);
	Log.Notice("pg_script_process_world_game_command_userbuyitems","user[%u] buyitem %s!",user->getHandle(), bOk?"sucessfully":"failed");
	
	return bOk ? SCR_RETURN_SUCCEED : SCR_RETURN_ERROR;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_removeChar(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_removeChar","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}	
	CharPtr pChr = sCharMgr.getByHandle(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	ResourceEvent event;
	event.event_id = ResourceEvent_SaveDB;
	pChr->SendEvent(&event);
	pChr->Delete();
	user->Delete();
	Log.Notice("pg_script_process_world_game_command_removeChar","removeChar user[%u]'s item sucessfully!",user_id);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_stoptalk(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_stoptalk","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	pChr->setUInt32Field("protect", time(0) + json_object["bantime"].asUInt());
	pChr->SaveDB();
	Log.Notice("pg_script_process_world_game_command_stoptalk","stoptalk user[%u] sucessfully!",user_id);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reduceitem(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reduceitem","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
			return SCR_RETURN_ERROR;
	}
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("script_id"))
		return SCR_RETURN_ERROR;
	uint32 modelId = json_object["script_id"].asUInt();
	if(0 == modelId)
		return SCR_RETURN_ERROR;
	// 数量
	uint32 num = json_object["num"].asUInt();
	// 存入数据库
	if (!sPubLogic.reduceItemNum(chr, modelId, num))
		return SCR_RETURN_ERROR;
	// 记录日志
	sLogDataMgr.UpdateItemsInfoLog(chr, en_ST_Gateway, modelId, en_UNT_Reduce, num);
	Log.Notice("pg_script_process_world_game_command_reduceitem","user[%u] reduce itemmodel[%u] num[%u] sucessfully!",user->getHandle(), modelId,num);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_noTrade(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_noTrade","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	ResourcePtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	ADD_CREATURE_FLAG(pChr, en_CreatureFlag_NoTrade);
		
	pChr->SaveDB();
	Log.Notice("pg_script_process_world_game_command_noTrade","user[%u] had stoped trad!",user->getHandle());
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_addviphonor(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_addviphonor","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
			return SCR_RETURN_ERROR;
	}
	PGCreatureInterface *pAiChrInfe = TO_CREATURE_INTERFACE(chr->getAIInterface());
	if (!pAiChrInfe)
		return SCR_RETURN_ERROR;
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	// vip卡ID
	uint32 vipId = json_object["id"].asUInt();
	uint32 endTime = json_object["mature_time"].asUInt();
	VIPCard vipCard = sVipCardMgr.GetVipCard(vipId);
	if (0 == vipCard.id)
		return SCR_RETURN_ERROR;
	pAiChrInfe->AddVipInfo(vipCard, endTime);
	Log.Notice("pg_script_process_world_game_command_addviphonor","user[%u] add vip honor sucessfully!",user->getHandle());
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_removeviphonor(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_removeviphonor","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr chr = sCharMgr.getByHandle(char_id);
	if(chr.isNull())
	{
		chr = sCharMgr.load(char_id);
		if(chr.isNull())
			return SCR_RETURN_ERROR;
	}
	PGCreatureInterface *pAiChrInfe = TO_CREATURE_INTERFACE(chr->getAIInterface());
	if (!pAiChrInfe)
		return SCR_RETURN_ERROR;
	pAiChrInfe->RemoveVipInfo();
	Log.Notice("pg_script_process_world_game_command_removeviphonor","user[%u] remove vip honor sucessfully!",user->getHandle());
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_canTrade(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_noTrade","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	ResourcePtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	CANCEL_CREATURE_FLAG(pChr, en_CreatureFlag_NoTrade);
	pChr->SaveDB();
	Log.Notice("pg_script_process_world_game_command_noTrade","user[%u] be agree trad!",user->getHandle());
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_enableMCount(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_enableMCount","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	uint32 user_id = command->uid;
	ResourcePtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	ADD_CREATURE_FLAG(pChr, en_CreatureFlag_EnMorraCnt);
	pChr->SaveDB();
	Log.Notice("pg_script_process_world_game_command_enableMCount","user[%u] be enableMCount!", user_id);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_disableMCount(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_disableMCount","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	ResourcePtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	CANCEL_CREATURE_FLAG(pChr, en_CreatureFlag_EnMorraCnt);
	pChr->SaveDB();
	Log.Notice("pg_script_process_world_game_command_disableMCount","user[%u] be disableMCount!", user_id);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_addMCount(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_addMCount","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	ResourcePtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("num"))
		return SCR_RETURN_ERROR;
	uint32 num = json_object["num"].asUInt();	
	sVipCardMgr.AddMorraNum(pChr, num);
	Log.Notice("pg_script_process_world_game_command_addMCount","user[%u] add num!", user_id);
	return SCR_RETURN_SUCCEED;
}

/***************************************************************************/
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloaditemmodel(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloaditemmodel","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
		
	std::list<ResourcePtr> lstItemModel;
	sItemModelMgr.getResourceList(&lstItemModel);
	std::list<ResourcePtr>::iterator iter = lstItemModel.begin();
	for(; iter != lstItemModel.end(); ++iter)
	{
		(*iter)->ReloadDB();
	}
	Log.Notice("pg_script_process_world_game_command_reloaditemmodel","reloaditemmodel sucessfully!");
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadtown(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadtown","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	std::list<ResourcePtr> towns;
	sTownMgr.getResourceList(&towns);
	std::list<ResourcePtr>::iterator ti, endti = towns.end();
	for(ti = towns.begin(); ti != endti; ti ++)
	{
		(*ti)->ReloadDB();
	}
	
	Log.Notice("pg_script_process_world_game_command_reloadtown","reloadtown[%u] sucessfully!",towns.size());
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadeffectmodel(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadeffectmodel","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	std::list<ResourcePtr> effects;
	sEffectModelMgr.getResourceList(&effects);
	std::list<ResourcePtr>::iterator ei = effects.begin();
	for(; ei != effects.end(); ++ei)
	{
		(*ei)->ReloadDB();
	}
	Log.Notice("pg_script_process_world_game_command_reloadeffectmodel","reloadeffectmodel sucessfully!");
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadmessage(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadmessage","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sMsg.clear();
	Log.Notice("pg_script_process_world_game_command_reloadmessage","reloadmessage sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadgameconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadgameconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadGameConfig();
	Log.Notice("pg_script_process_world_game_command_reloadgameconf","reloadgameconf sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadlevelconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadlevelconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadLevelExpList();
	Log.Notice("pg_script_process_world_game_command_reloadlevelconf","reloadlevelxml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloaddiamondconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloaddiamondconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadDiamondLogin();
	Log.Notice("pg_script_process_world_game_command_reloaddiamondconf","reloadldiamondxml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadpresentconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadpresentconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadPresents();
	Log.Notice("pg_script_process_world_game_command_reloadpresentconf","reloadlpresentxml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadkeywordsconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadkeywordsconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadKeyWords();
	Log.Notice("pg_script_process_world_game_command_reloadkeywordsconf","reloadlkeywords sucessfully!");
	sXmlDataMgr.ReloadNickKeyWords();
	Log.Notice("pg_script_process_world_game_command_reloadkeywordsconf","reloadlnickkeywords sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadgzidconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadgzidconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadGZIDName();
	Log.Notice("pg_script_process_world_game_command_reloadgzidconf","reloadlgzId sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_clearNotices(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_clearNotices","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sNoticeMgr.Clear();
	Log.Notice("pg_script_process_world_game_command_clearNotices","clear notices sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadVipCards(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadVipCards","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sVipCardMgr.ReloadVipCards();
	Log.Notice("pg_script_process_world_game_command_reloadVipCards","reload vipcards sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_changeuser(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_changeuser","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	
	if(!json_object.isMember("sid"))
		return SCR_RETURN_ERROR;
	
	if(!json_object.isMember("tid"))
		return SCR_RETURN_ERROR;
	
	uint32 src_pid = json_object["sid"].asUInt();
	uint32 tag_pid = json_object["tid"].asUInt();
	if(src_pid == tag_pid)
		return SCR_RETURN_ERROR;
	
	std::list<UserPtr> users1;
	sUserMgr.getUserList(&users1, 0, 1, "platform_id = %u", src_pid);
	if(users1.empty())
		return SCR_RETURN_ERROR;	
		
	UserPtr pUser1 = users1.front();
	
	std::list<UserPtr> users2;
	sUserMgr.getUserList(&users2, 0, 1, "platform_id = %u", tag_pid);
	if(users2.empty())
		return SCR_RETURN_ERROR;
		
	UserPtr pUser2 = users2.front();
	if(pUser2.isNull() || pUser1.isNull())
		return SCR_RETURN_ERROR;
	
	CharPtr pChr1 = sCharMgr.load(pUser1->getUInt32Field("char_id"));
	CharPtr pChr2 = sCharMgr.load(pUser2->getUInt32Field("char_id"));
	if(pChr1.isNull() || pChr2.isNull())
		return SCR_RETURN_ERROR;
	
	uint32 game_id = pUser1->getUInt32Field("gz_id");
	uint32 ishere = pChr1->getUInt32Field("ishere");
	if(game_id == 2)
		return SCR_RETURN_ERROR;
	
	if(ishere != 0)
	{
		if(ishere != game_id)
			return SCR_RETURN_ERROR;
	}
	
	if(pChr1->getStatus() != CharacterStatusFree || pChr2->getStatus() != CharacterStatusFree)
		return SCR_RETURN_ERROR;
	
	uint32 bantime = time(0) + 10;
	pUser1->setUInt32Field("ban_time", bantime);
	pUser2->setUInt32Field("ban_time", bantime);
	
	WSSocketPtr pSocket1 = pChr1->getSocket();
	WSSocketPtr pSocket2 = pChr2->getSocket();
	if(!pSocket1.isNull())
		pSocket1->Disconnect();
	if(!pSocket2.isNull())
		pSocket2->Disconnect();

	// 取出新号的资产
	uint32 coins2 = 0, exchange2 = 0, match2 = 0, gold2 = 0;
	sBankMgr.GetItemNum(pChr2, en_ItemModelId_Coins, &coins2);
	sBankMgr.GetItemNum(pChr2, en_ItemModelId_Exchange, &exchange2);
	sBankMgr.GetItemNum(pChr2, en_ItemModelId_Match, &match2);
	sBankMgr.GetItemNum(pChr2, en_ItemModelId_Gold, &gold2);
	
	// 累加到旧号上面
	sBankMgr.addItemNum(game_id, pChr1, en_ItemModelId_Coins, coins2);
	sBankMgr.addItemNum(game_id, pChr1, en_ItemModelId_Exchange, exchange2);
	sBankMgr.addItemNum(game_id, pChr1, en_ItemModelId_Match, match2);
	sBankMgr.addItemNum(game_id, pChr1, en_ItemModelId_Gold, gold2);
	
	// 交换新旧帐号绑定
	pUser2->setUInt32Field("char_id", pChr1->getHandle());
	pChr1->setUInt32Field("userid", pUser2->getHandle());
	
	pUser1->setUInt32Field("char_id", 0);
	pChr2->setUInt32Field("userid", 0);
	
	pUser1->SaveDB();
	pUser2->SaveDB();	
	pChr1->SaveDB();
	pChr2->SaveDB();
	
	pUser1->Delete();
	pUser2->Delete();	
	pChr1->Delete();
	pChr2->Delete();
	
	Log.Notice("pg_script_process_world_game_command_changeuser","srcpid[%u] change to tagpid[%u] sucessfully!", src_pid, tag_pid);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_getbankdata(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_getbankdata","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	
	if(!json_object.isMember("bankid"))
		return SCR_RETURN_ERROR;
	
	if(!json_object.isMember("pid"))
		return SCR_RETURN_ERROR;
	
	uint32 bankpid = json_object["bankid"].asUInt();
	uint32 userpid = json_object["pid"].asUInt();
	
	
	std::list<GameBank> lstBankInfo;
	if(!sChannelMgr.getBankList(&lstBankInfo, 0, 1, "`platform_id` = %u and `type` = %u", bankpid, 1))
		return SCR_RETURN_ERROR;
	
	if(lstBankInfo.empty())
		return SCR_RETURN_ERROR;
	
	GameBank bank = lstBankInfo.front();
	
	std::list<UserPtr> users1;
	sUserMgr.getUserList(&users1, 0, 1, "platform_id = %u", userpid);
	if(users1.empty())
		return SCR_RETURN_ERROR;	
	UserPtr ptagUser = users1.front();
	if(ptagUser.isNull())
		return SCR_RETURN_ERROR;
	
	CharPtr pChr = sCharMgr.load(ptagUser->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	sPubLogic.addItemNum(pChr, en_ItemModelId_Coins, bank.coins);
	sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_Bank, en_ItemModelId_Coins, en_UNT_Add, bank.coins);
	bank.coins = 0;
	sChannelMgr.updateBankDB(&bank);
	
	Log.Notice("pg_script_process_world_game_command_getbankdata", "pg_script_process_world_game_command_getbankdata userpid[%u] sucessfully!", userpid);
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_additemforbank(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_additemforbank","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("script_id"))
		return SCR_RETURN_ERROR;
	uint32 modelId = json_object["script_id"].asUInt();
	if(0 == modelId)
		return SCR_RETURN_ERROR;
	// 数量
	uint32 num = json_object["num"].asUInt();
	// 存款
	sBankMgr.SaveWealthInBank(user, modelId, num, en_Operate_GatewayAdd);
	Log.Notice("pg_script_process_world_game_command_additemforbank","user[%u] additem scriptid[%u] num[%u] sucessfully!",user->getHandle(), modelId,num);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_reduceitemforbank(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reduceitemforbank","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("script_id"))
		return SCR_RETURN_ERROR;
	uint32 modelId = json_object["script_id"].asUInt();
	if(0 == modelId)
		return SCR_RETURN_ERROR;
	// 数量
	uint32 num = json_object["num"].asUInt();
	// 取款
	sBankMgr.GetWealthFromBank(user, modelId, num, en_Operate_GatewayReduce);
	
	Log.Notice("pg_script_process_world_game_command_reduceitemforbank","user[%u] additem scriptid[%u] num[%u] sucessfully!",user->getHandle(), modelId,num);
	
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadChips(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadChips","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadChips();
	Log.Notice("pg_script_process_world_game_command_reloadChips","reload chips sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadGZIds(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadGZIds","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReLoadGameIDXml();
	Log.Notice("pg_script_process_world_game_command_reloadGZIds","reload game_ids.xml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadTurnplate(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadTurnplate","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.LoadChanceConf();
	sXmlDataMgr.LoadBuyLargessXml();
	Log.Notice("pg_script_process_world_game_command_reloadTurnplate","reload roulette.xml sucessfully!");
	return SCR_RETURN_SUCCEED;
}

int SERVER_DECL pg_script_process_world_game_command_setBothDisableMCount(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_setBothDisableMCount","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	
	CharPtr pChr = sCharMgr.load(user->getUInt32Field("char_id"));
	if(pChr.isNull())
		return SCR_RETURN_ERROR;
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("op"))
		return SCR_RETURN_ERROR;
	
	uint8 op = json_object["op"].asUInt();
	
	if(!op)
		CANCEL_CREATURE_FLAG(pChr,en_CreatureFlag_BothMCount);
	else if(op == 1)
		ADD_CREATURE_FLAG(pChr, en_CreatureFlag_BothMCount);
	else
		return SCR_RETURN_ERROR;
	
	pChr->SaveDB();
	
	return SCR_RETURN_SUCCEED;
}

int SERVER_DECL pg_script_process_world_game_command_operationitem(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_operationitem","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
		if(user.isNull())
			return SCR_RETURN_ERROR;
	}
	uint32 char_id = user->getUInt32Field("char_id");
	CharPtr pChr = sCharMgr.getByHandle(char_id);
	if(pChr.isNull())
	{
		pChr = sCharMgr.load(char_id);
		if(pChr.isNull())
			return SCR_RETURN_ERROR;
	}
	PGCreatureInterface *pAiChrInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!pAiChrInfe)
		return SCR_RETURN_ERROR;
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	std::vector<std::string> vecKeys = json_object.getMemberNames();
	std::vector<std::string>::iterator iter = vecKeys.begin();
	String strContent;
	uint32 amount = 0;
	bool bOk = true;
	Json::Value root; // 返回信息
	Json::FastWriter fWriter; 
	for (; iter != vecKeys.end(); ++iter)
	{
		if (StringUtil::startsWith((*iter),"addi"))
		{
			uint32 op;
			std::vector<String> strInfo = StringUtil::split(unbase64(json_object[(*iter)].asString()),"|");
			// 操作				
			if(strInfo.size() < 3)
				return SCR_RETURN_ERROR;
			else if(strInfo.size() == 3)
			{
				op = 1;
			}
			else
			{
				op = StringConverter::parseUnsignedInt(strInfo[3]);
				if(op == 0)
					op = 1;
			}
			
			if(strInfo[0] == "" || strInfo[1] == "")
				return SCR_RETURN_ERROR;
			// 模式ID
			uint32 modelId = StringConverter::parseUnsignedInt(strInfo[0]);
			// 数量
			uint32 num = StringConverter::parseUnsignedInt(strInfo[1]);
			// 名称
			String szName = ServerDatabase.EscapeString(strInfo[2]);
			
			uint8 re = 0;
			if(op == 1)
			{// 增加
				re = !sPubLogic.addItemNum(pChr, modelId, num).isNull();
			}
			else if(op == 2)
			{// 减少
				re = sPubLogic.reduceItemNum(pChr, modelId, num);
			}
			
			if (re > 0)
			{
				sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_Gateway, modelId, op == 0 ? en_UNT_Add : en_UNT_Reduce, num);
			}
			else
			{
				bOk = false;
			}
			uint32 residue = sPubLogic.GetItemNum(pChr, modelId);
		
			Json::Value child; // 返回信息
			child["script_id"] = Json::Value(modelId);	// 模式号
			child["num"] = Json::Value(num);			// 数量
			child["name"] = Json::Value(szName);		// 名称
			child["op"] = Json::Value(op);				// 操作
			child["re"]  	 = Json::Value(re);			// 结果
			child["balance"]  = Json::Value(residue);	// 余额
			
			root[(*iter)] = Json::Value(child);
		}
		else if (StringUtil::startsWith((*iter),"message"))
		{
			// 消息内容
			String strMsg(unbase64(json_object[*iter].asString()));
			// 发信息
			sPubLogic.SendMsgToPlayer(user_id, strMsg, 1);
		}
		else if (StringUtil::startsWith((*iter),"amount"))
		{
			amount = uint32(StringConverter::parseReal(unbase64(json_object[*iter].asString())) * 100.0f);
		}
	}
	
	command->result = fWriter.write(root);
	// 更新COMMAND
	GatewayDatabase.WaitExecute("update gw_command set `result` = '%s' where `cmdid` = %u", command->result.c_str(), command->cmdid);
	Log.Notice("pg_script_process_world_game_command_operationitem","user[%u] buyitem %s!command->result[%s]",user->getHandle(), bOk?"sucessfully":"failed", command->result.c_str());
	
	return bOk ? SCR_RETURN_SUCCEED : SCR_RETURN_ERROR;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadnewplayergiftconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadnewplayergiftconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadNewPlayerGift();
	Log.Notice("pg_script_process_world_game_command_reloadnewplayergiftconf","reloadnewplayergiftxml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadipsift(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadipsift","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sShowIP.ReloadXmlFile();
	Log.Notice("pg_script_process_world_game_command_reloadipsift","reload ipsift.xml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int  pg_script_process_world_game_command_reloadGzList(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadGzList","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sBankMgr.ReloadFieldXML();
	Log.Notice("pg_script_process_world_game_command_reloadGzList","reload serverdata.xml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int pg_script_process_world_game_command_reloadonlinegiftconf(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadonlinegiftconf","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReloadOnlineBounty();
	Log.Notice("pg_script_process_world_game_command_reloadonlinegiftconf","reloadonlinegiftxml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int SERVER_DECL pg_script_process_world_game_command_reloadreggivelecardsxml(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_reloadreggivelecardsxml","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	sXmlDataMgr.ReLoadGiveLeCardsXml();
	Log.Notice("pg_script_process_world_game_command_reloadreggivelecardsxml","reload giveLeCards.xml sucessfully!");
	return SCR_RETURN_SUCCEED;
}
//-----------------------------------------------------------------------
int SERVER_DECL pg_script_process_world_game_command_addlecards(void * info)
{
	if(!info)
		return SCR_RETURN_ERROR;
	
	GatewayCommand * command = (GatewayCommand *)info;
	Log.Debug("pg_script_process_world_game_command_addlecards","command id[%u],command_type[%u],command_str[%s]",command->cmdid,command->command_type,(command->parameter).c_str());
	
	uint32 user_id = command->uid;
	uint32 pid = 0;
	UserPtr user = sUserMgr.getByHandle(user_id);
	if(user.isNull())
	{
		user = sUserMgr.load(user_id);
	}
	
	if(!user.isNull())
		pid = user->getUInt32Field("platform_id");
	
	Json::Reader reader; 
	Json::Value json_object; 
	if (!reader.parse(command->parameter.c_str(), json_object))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("num"))
		return SCR_RETURN_ERROR;
	if (!json_object.isMember("type"))
		return SCR_RETURN_ERROR;
		
	uint32 days = 30;
	if(json_object.isMember("days"))
		days = json_object["days"].asUInt();
		
	uint32 num = json_object["num"].asUInt();
	uint32 type = json_object["type"].asUInt();
	if(!num || !type)
		return SCR_RETURN_ERROR;
	
	std::list<GameExchange> cards;
	sPubLogic.CreateLECards(&cards,pid,num,type, days);
	
	Log.Notice("pg_script_process_world_game_command_addlecards","add lecards to pid[%u] type[%u] num[%u] sucessfully!",num);
	return SCR_RETURN_SUCCEED;
}

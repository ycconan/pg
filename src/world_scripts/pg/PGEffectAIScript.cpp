#include "Character.h"
#include "CharManager.h"
#include "Channels.h"
#include "ChannelManager.h"
#include "PGEffectAIScript.h"
#include "PGWorldAIInterface.h"
#include "PGEnum.h"
#include "PGPublicLogic.h"
#include "PGPubStruct.h"

namespace AIScript
{
	//------------------------------------------------
	uint8 DemosResultEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GameOver || event->target.isNull())
			return EVERET_NONE;
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pChr->getHandle() == event->idata) // �Ƿ��м�
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		if (event->idata3)
			sPubLogic.CountEffectNumber(pEffect, pChr);
		else
		{
			pEffect->setUInt32Field("data1", 0);
			pEffect->SaveDB();
		}
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 BankerResultEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GameOver || event->target.isNull())
			return EVERET_NONE;
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pChr->getHandle() != event->idata || 
			event->idata2 != PGChannelInterface::en_ResultType_FullWin) // �Ƿ�ׯ����ͨɱ
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 BankerUpdateEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_BankerUpdate || event->target.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull() || pEffect->model->getUInt32Field("effect2") != event->idata) 
			return EVERET_NONE;
		sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 ChipTheCardsEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GameOver || event->target.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull()) 
			return EVERET_NONE;
		// ����Ƿ���ָ������
		uint32 cardsType = pEffect->model->getUInt32Field("effect2");
		if (pChr->getHandle() == event->idata && cardsType != event->idata4) 
			return EVERET_NONE;
		PGCreatureInterface *pChrAiInfe;
		PGChannelInterface *pChnAiInfe;
		if (event->source.isNull() || 
			NULL == (pChnAiInfe = TO_CHANNEL_INTERFACE(event->source.getResourcePtr()->getAIInterface())) || 
			NULL == (pChrAiInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface())))
			return EVERET_NONE;	
		std::vector<TablePlayer> &vecPos = pChnAiInfe->GetPositionInfo();
		for (size_t i = 1; i != vecPos.size(); ++i)
		{
			if (pChrAiInfe->GetChip(i))
			{
				if (vecPos[i].cards.firtype == cardsType)
				{
					sPubLogic.CountEffectNumber(pEffect, pChr);
					break;
				}
			}
		}
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 ChipOneWinEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GameOver || event->target.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull() || pChr->getHandle() == event->idata) // �Ƿ��м�
			return EVERET_NONE;
		// ����Ƿ�����ָ����λѺע��ʤ��
		PGCreatureInterface *pChrAiInfe;
		PGChannelInterface *pChnAiInfe;
		if (event->source.isNull() || 
			NULL == (pChnAiInfe = TO_CHANNEL_INTERFACE(event->source.getResourcePtr()->getAIInterface())) || 
			NULL == (pChrAiInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface())))
			return EVERET_NONE;
		std::vector<TablePlayer> &vecPos = pChnAiInfe->GetPositionInfo();
		uint32 pos = pEffect->model->getUInt32Field("effect2");
		if (pChrAiInfe->GetChip(pos) && vecPos[pos].isWin)
			sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 SameTypeCompareEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GameOver || event->target.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->target.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull() || pChr->getHandle() == event->idata) // �Ƿ��м�
			return EVERET_NONE;
		PGCreatureInterface *pChrAiInfe;
		PGChannelInterface *pChnAiInfe;
		if (event->source.isNull() || 
			NULL == (pChnAiInfe = TO_CHANNEL_INTERFACE(event->source.getResourcePtr()->getAIInterface())) || 
			NULL == (pChrAiInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface())))
			return EVERET_NONE;	
		uint32 cardsType = pEffect->model->getUInt32Field("effect2");
		// �ж�ׯ�������Ƿ�Ϊָ������
		if (event->idata4 == cardsType)
		{
			uint32 isGreater = pEffect->model->getUInt32Field("effect3");
			std::vector<TablePlayer> &vecPos = pChnAiInfe->GetPositionInfo();
			for (size_t i = 1; i != vecPos.size(); ++i)
			{
				if (pChrAiInfe->GetChip(i))
				{
					if (vecPos[i].cards.firtype == cardsType && vecPos[i].isWin == isGreater)
					{
						sPubLogic.CountEffectNumber(pEffect, pChr);
						break;
					}
				}
			}	
		}
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 WealthEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_CoinsUpdate || event->source.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->source.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull())
			return EVERET_NONE;
		uint64 allCoins = pChr->getUInt32Field("coins") + sPubLogic.GetBankerNum(pChr, ItemModel_Coins);
		if (allCoins >= pEffect->model->getUInt32Field("effect2"))
			sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 CountEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GetEffort || event->source.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->source.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull())
			return EVERET_NONE;
		sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
	//------------------------------------------------
	uint8 GivePresentEffortAIScript::OnEvent(ResourceEvent * event)
	{
		if (!event || event->event_id != en_ResourceEvent_GivePresent || event->source.isNull())
			return EVERET_NONE;
		EffectPtr pEffect = sEffectMgr.getByHandle(m_Unit->getHandle());
		CharPtr pChr = event->source.getResourcePtr();
		if (pChr.isNull() || pEffect.isNull())
			return EVERET_NONE;
		if (event->idata == pEffect->model->getUInt32Field("effect2"))
			sPubLogic.CountEffectNumber(pEffect, pChr);
		return EVERET_OK;
	}
}

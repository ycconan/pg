#include "Common.h"
#include "Resource.h"
#include "String.h"
#include "SharedPtr.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "ScriptMgr.h"

#include "PGWorldAIInterface.h"
#include "PGWorldAIInterface.h"
#include "PGAIScript.h"
#include "PGRuleManager.h"
#include "PGEffectAIScript.h"

void register_custom_world_create_ai(ScriptMgr * mgr)
{
	/*********************************************规则注册***********************************************/
	// 炸弹一
	sRuleMgr.RegisterRule(en_CardsType_BombOne, new BombOneRule);
	Log.Debug("register_PG_world_create_ai", "Register BombOneRule[%u] OK!", en_CardsType_BombOne);
	// 鬼子
	sRuleMgr.RegisterRule(en_CardsType_Devil, new DevilRule);
	Log.Debug("register_PG_world_create_ai", "Register DevilRule[%u] OK!", en_CardsType_Devil);
	// 至尊宝
	sRuleMgr.RegisterRule(en_CardsType_Extreme, new ExtremeRule);
	Log.Debug("register_PG_world_create_ai", "Register ExtremeRule[%u] OK!", en_CardsType_Extreme);
	// 天王九
	sRuleMgr.RegisterRule(en_CardsType_KingNine, new KingNineRule);
	Log.Debug("register_PG_world_create_ai", "Register KingNineRule[%u] OK!", en_CardsType_KingNine);
	// 双天
	sRuleMgr.RegisterRule(en_CardsType_DoubleDay, new doubleDayRule);
	Log.Debug("register_PG_world_create_ai", "Register doubleDayRule[%u] OK!", en_CardsType_DoubleDay);
	// 地九靓靓
	sRuleMgr.RegisterRule(en_CardsType_EarthNineLady, new EarthNineLadyRule);
	Log.Debug("register_PG_world_create_ai", "Register EarthNineLadyRule[%u] OK!", en_CardsType_EarthNineLady);
	// 双地
	sRuleMgr.RegisterRule(en_CardsType_DoubleEarth, new DoubleEarthRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleEarthRule[%u] OK!", en_CardsType_DoubleEarth);
	// 双人
	sRuleMgr.RegisterRule(en_CardsType_DoubleMan, new DoubleManRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleManRule[%u] OK!", en_CardsType_DoubleMan);
	// 双鹅
	sRuleMgr.RegisterRule(en_CardsType_TwoGoose, new TwoGooseRule);
	Log.Debug("register_PG_world_create_ai", "Register TwoGooseRule[%u] OK!", en_CardsType_TwoGoose);
	// 双梅
	sRuleMgr.RegisterRule(en_CardsType_DoublePlum, new DoublePlumRule);
	Log.Debug("register_PG_world_create_ai", "Register DoublePlumRule[%u] OK!", en_CardsType_DoublePlum);
	// 双长三
	sRuleMgr.RegisterRule(en_CardsType_DoubleLongThree, new DoubleLongthreeRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleLongthreeRule[%u] OK!", en_CardsType_DoubleLongThree);
	// 两板凳
	sRuleMgr.RegisterRule(en_CardsType_DoubleStool, new DoubleStoolRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleStoolRule[%u] OK!", en_CardsType_DoubleStool);
	// 两斧头
	sRuleMgr.RegisterRule(en_CardsType_DoubleAxe, new DoubleAxeRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleAxeRule[%u] OK!", en_CardsType_DoubleAxe);
	// 红头十
	sRuleMgr.RegisterRule(en_CardsType_RedHeadTen, new RedHreadRule);
	Log.Debug("register_PG_world_create_ai", "Register RedHreadRule[%u] OK!", en_CardsType_RedHeadTen);
	// 双高七
	sRuleMgr.RegisterRule(en_CardsType_DoubleHighSeven, new DoubleHightSevenRule);
	Log.Debug("register_PG_world_create_ai", "Register DoubleHightSevenRule[%u] OK!", en_CardsType_DoubleHighSeven);
	// 铜锤
	sRuleMgr.RegisterRule(en_CardsType_Pratia, new PratiaRule);
	Log.Debug("register_PG_world_create_ai", "Register PratiaRule[%u] OK!", en_CardsType_Pratia);
	// 杂九
	sRuleMgr.RegisterRule(en_CardsType_MiscellaneousNine, new MiscellaneousNineRule);
	Log.Debug("register_PG_world_create_ai", "Register MiscellaneousNineRule[%u] OK!", en_CardsType_MiscellaneousNine);
	// 杂八
	sRuleMgr.RegisterRule(en_CardsType_MiscellaneousEight, new MiscellaneousEightRule);
	Log.Debug("register_PG_world_create_ai", "Register MiscellaneousEightRule[%u] OK!", en_CardsType_MiscellaneousEight);
	//杂七
	sRuleMgr.RegisterRule(en_CardsType_MiscellaneousSeven, new MiscellaneousSevenRule);
	Log.Debug("register_PG_world_create_ai", "Register MiscellaneousSevenRule[%u] OK!", en_CardsType_MiscellaneousSeven);
	// 杂五
	sRuleMgr.RegisterRule(en_CardsType_MiscellaneousFive, new MiscellaneousFiveRule);
	Log.Debug("register_PG_world_create_ai", "Register MiscellaneousFiveRule[%u] OK!", en_CardsType_MiscellaneousFive);
	// 天杠
	sRuleMgr.RegisterRule(en_CardsType_DayBar, new DayBarRule);
	Log.Debug("register_PG_world_create_ai", "Register DayBarRule[%u] OK!", en_CardsType_DayBar);
	// 地杠
	sRuleMgr.RegisterRule(en_CardsType_EarthBar, new EarthBarRule);
	Log.Debug("register_PG_world_create_ai", "Register EarthBarRule[%u] OK!", en_CardsType_EarthBar);

		// 单牌
	sRuleMgr.OtherSingleRegisterRule(new OtherSingleRule);
	/*********************************************服务器钩子注册***********************************************/
	// mgr->register_server_hook(SERVER_HOOK_LOOP, ServerCheckUserHook);
	// Log.Debug("register_PG_world_create_ai","Register server loop hook OK!");
	
	/********************************************接口注册*****************************************************/
	// 角色
	mgr->register_interface_script((uint16) ResourceTypeCharacter, 0, &AIScript::PGCreatureInterface::Create);
	Log.Debug("register_PG_world_create_ai","Register Creature %d [%s]!",0,"CharacterInterface");	
	// 牌桌
	// mgr->register_interface_script((uint16)ResourceTypeChannel, 200, &AIScript::WAIChannelInterface::Create);
	// Log.Debug("register_PG_world_create_ai", "Register Channel %d [%s]!", 200, "WAIChannelInterface");

	mgr->register_interface_script((uint16)ResourceTypeChannel, 200, &AIScript::PGChannelInterface::Create);
	Log.Debug("register_PG_world_create_ai", "Register Channel %d [%s]!", 200, "PGChannelInterface");
	// 房间
	mgr->register_interface_script((uint16) ResourceTypeTown, 200, &AIScript::PGTownInterface::Create);
	Log.Debug("register_PG_world_create_ai","Register TownScriptAI %d [%s]",200,"TownMatchScriptAI");
	// 区域
	mgr->register_interface_script((uint16) ResourceTypeRegion, 0, &AIScript::PGRegionInterface::Create);
	Log.Debug("register_PG_world_create_ai","Register region %d [%s]!",0,"PGRegionInterface");	
	// socket连接
	mgr->register_interface_script((uint16) ResourceTypeWorldSocket, 0, &AIScript::PGSocketInterface::Create);
	Log.Debug("register_PG_world_create_ai","Register Socket %d [%s]!",0,"PGSocketInterface");	
	
	/********************************************脚本注册*****************************************************/
	/**
	 * 注册效果
	 */
	// 闲家连胜
	uint32 ef_model_id = 1;
	for(; ef_model_id <= 3; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::DemosResultEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"DemosResultEffortAIScript","WAIEffectInterface");	
	}
	// 庄家通吃
	for(; ef_model_id <= 6; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::BankerResultEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"BankerResultEffortAIScript","WAIEffectInterface");
	}
	// 庄家更新
	for(; ef_model_id <= 12; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::BankerUpdateEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"BankerUpdateEffortAIScript","WAIEffectInterface");
	}
	// 押到28天杠
	for(; ef_model_id <= 15; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::ChipTheCardsEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"ChipTheCardsEffortAIScript","WAIEffectInterface");
	}
	// 押某一门获胜
	for(; ef_model_id <= 18; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::ChipOneWinEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"ChipOneWinEffortAIScript","WAIEffectInterface");
	}
	// 押到豹子
	for(; ef_model_id <= 21; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::ChipTheCardsEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"ChipTheCardsEffortAIScript","WAIEffectInterface");
	}
	// 跟庄家同牌型比大小
	for(; ef_model_id <= 23; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::SameTypeCompareEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"SameTypeCompareEffortAIScript","WAIEffectInterface");
	}
	// 财富成就
	for(; ef_model_id <= 26; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WealthEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"WealthEffortAIScript","WAIEffectInterface");
	}
	// 统计成就数量
	for(; ef_model_id <= 28; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::CountEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"CountEffortAIScript","WAIEffectInterface");
	}
	// 赠送礼物
	for(; ef_model_id <= 30; ++ef_model_id)
	{
		mgr->register_ai_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::GivePresentEffortAIScript::Create);
		mgr->register_interface_script((uint16) ResourceTypeEffect, ef_model_id, &AIScript::WAIEffectInterface::Create);
		Log.Debug("register_PG_world_create_ai","Register Effect %d scripts on [%s],[%s]!", 
					ef_model_id,"GivePresentEffortAIScript","WAIEffectInterface");
	}
}

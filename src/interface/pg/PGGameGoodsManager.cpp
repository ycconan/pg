#include "GameGoodsManager.h"
#include "PGXmlDataManager.h"

GameGoodsManager::GameGoodsManager(void)
{
}

GameGoodsManager::~GameGoodsManager(void)
{

}

void GameGoodsManager::load(void)
{
	WGS_GOODSMGR_INNER_LOCK
	m_goodsList.clear();
	sItemMgr.getGoodsListDB(&m_goodsList, 0, 5000, "");
	Log.Debug("GameGoodsManager Test","Load Goods!! Size[%u]", m_goodsList.size());
}





void GameGoodsManager::Reload()
{
	load();
}

const GameGoods* GameGoodsManager::GetGameGoods(const uint32 & model_id, const GoodsFlagDef & buy_type)
{
	WGS_GOODSMGR_INNER_LOCK
	std::list<GameGoods>::iterator ii, endii = m_goodsList.end();
	for(ii = m_goodsList.begin(); ii != endii; ++ ii)
	{
		if((*ii).model_id == model_id && (*ii).flag & buy_type)
			return &*ii;
	}
	
	return NULL;
}

const GameGoods * GameGoodsManager::GetGameGoodsById(const uint32 & goodsId)
{
	WGS_GOODSMGR_INNER_LOCK
	std::list<GameGoods>::iterator ii, endii = m_goodsList.end();
	for(ii = m_goodsList.begin(); ii != endii; ++ ii)
	{
		if((*ii).goods_id == goodsId)
			return &*ii;
	}
	
	return NULL;
}

uint32 GameGoodsManager::GetGoodsPrice(const uint32 & model_id, const GoodsFlagDef & buy_type)
{
	WGS_GOODSMGR_INNER_LOCK
	std::list<GameGoods>::iterator ii, endii = m_goodsList.end();
	for(ii = m_goodsList.begin(); ii != endii; ++ ii)
	{
		if((*ii).model_id == model_id && (*ii).flag & buy_type)
			return (*ii).price;
	}
	
	return 0;
}

uint8 GameGoodsManager::GetGoodsList(std::list<GameGoods*> *pGoodsList, const uint8 shopType, const uint32 supId, const uint8 labelType, const uint8 page, const uint8 number)
{
	if (page == 0)
		return 0;
	WGS_GOODSMGR_INNER_LOCK
	uint8 allPage = 0;
	uint32 lowerLimit = (page - 1) * number;
	uint32 upperLimit = lowerLimit + number;
	std::list<GameGoods*> lstTempGoods;
	std::list<GameGoods>::iterator iter, iterEnd = m_goodsList.end();
	for(iter = m_goodsList.begin(); iter != iterEnd; ++iter)
	{
		if (shopType == en_ShopT_GoldShop) // 元宝商城
		{
			if (labelType == en_AT_RealObject)
				return 0;
			if (labelType == (*iter).type && (supId == (*iter).sup_id || 0 == (*iter).sup_id))
				lstTempGoods.push_back(&(*iter));
		}
		else
		{
			// 兑换券商城
			if ((*iter).type == en_AT_RealObject)
				lstTempGoods.push_back(&(*iter));
		}
	}
	uint32 size = lstTempGoods.size();
	if (0 == size)
		return 0;
	allPage = (size + number - 1) / number;
	if (allPage < page)
		return 0;
	uint32 count = 0;
	std::list<GameGoods*>::iterator ii, endii = lstTempGoods.end();
	for(ii = lstTempGoods.begin(); ii != endii; ++ ii)
	{
		if (++count > upperLimit)
			break;
		if (count > lowerLimit)
		{
			pGoodsList->push_back(*ii);
		}
	}
	return allPage;
}

#include "Channels.h"
#include "ChannelManager.h"
#include "Resource.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "Character.h"
#include "CharManager.h"
#include "Items.h"
#include "ItemManager.h"
#include "WorldPacket.h"
#include "Opcodes.h"
#include "RegionManager.h"
#include "Towns.h"
#include "TownManager.h"
#include "Users.h"
#include "UserManager.h"
#include "Mails.h"
#include "MailManager.h"
#include "WSSocket.h"
#include "WSSocketManager.h"
#include "Message.h"
#include "MD5.h"
#include "Titles.h"
#include "TitleManager.h"
#include "Missions.h"
#include "MissionManager.h"
#include "MissionModels.h"
#include "MissionModelManager.h"
#include "PGRuleManager.h"

#include "GameDefine.h"
#include XMLDATA_MGR_PATH
#include PROTOCOLS_MGR_PATH
#include LOG_MGR_PATH

#include "PGGameLogicManager.h"
#include "PGEnum.h"
#include "PGWorldAIInterface.h"

#include "CenterBankManager.h"
#include "GameDataEnum.h"
#include "ResourceEventEnum.h"

PGGameLogicManager::PGGameLogicManager(void)
{
	
}



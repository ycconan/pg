#include "Common.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "MersenneTwister.h"
#include "ScriptMgr.h"
#include "Threading/Mutex.h"
#include "Vector3.h"
#include "Game.h"
#include "Util.h"
#include "WorldPacket.h"
#include "Character.h"
#include "CharManager.h"
#include "Spawns.h"
#include "Towns.h"
#include "PGWorldAIInterface.h"
#include "PGAIScript.h"
#include "PGHallManager.h"
#include "PGManualThreadsManager.h"
#include "GameThreadLogic.h"
#include "GameGoodsManager.h"
#include "NoticeManager.h"
#include "GameDefine.h"
#include XMLDATA_MGR_PATH
#include LOGIC_MGR_PATH
#include "PubDataManager.h"

namespace AIScript {
static bool isSpawn = false;
static bool isInitGoods = false;
	//-----------------------------------------------------------------------
	PGRegionInterface::PGRegionInterface(Resource * channel):AIInterface(channel)
	{
		m_AIType = AITYPE_REGION;
		m_spawnLastTimer = 0;
		m_curSpawnCount= 0;
	}


	//-----------------------------------------------------------------------
	PGRegionInterface::~PGRegionInterface(void)
	{
	
	}
	void PGRegionInterface::Load(void)
	{	
		// Region * reg = (Region*)m_Unit;
		// if (reg->getRegionX() == 20)
			// Log.Debug("ssssssssssss", "////////// id = %d, pos[%u, %u, %u]", m_Unit->getHandle(),reg->getRegionX(), reg->getRegionY(), reg->common.position.z);
		AIInterface::Load();
	}
	
	
	void PGRegionInterface::Update(const uint32 & p_time)
	{		
		Region * reg = (Region*)m_Unit;
		std::list<CharPtr> chars;
		reg->getCharsList(&chars);
		std::list<CharPtr>::iterator ii, endii = chars.end();
		for(ii = chars.begin(); ii != endii; ++ ii)
		{
			(*ii)->CallScriptUpdate(p_time);
		}
		std::list<ChannelPtr> lstChannel;
		reg->getChannelsList(&lstChannel);
		std::list<ChannelPtr>::iterator iter;
		for(iter = lstChannel.begin(); iter != lstChannel.end(); ++iter)
		{
			(*iter)->CallScriptUpdate(p_time);
		}
		
		static Rectangle rg = sRegionMgr.getWorldRectangle();
		static Vector2 v1(RandomInt((int32)rg.left, (int32)rg.right), RandomInt((int32)rg.top, (int32)rg.bottom));
		static Vector2 v2(RandomInt((int32)rg.left, (int32)rg.right), RandomInt((int32)rg.top, (int32)rg.bottom));
		static Vector2 v3(RandomInt((int32)rg.left, (int32)rg.right), RandomInt((int32)rg.top, (int32)rg.bottom));
		
		if(!isSpawn && 10 == reg->getRegionX() && 1 == reg->getRegionY())		
		{

			sGLMgr.CheckZujuChannelOver();
			isSpawn = true;
			ServerDatabase.WaitExecute("update `battle_channel` set `status` = 255 where `status` <> 255");			

			ThreadPool.ExecuteTask(new PGSpawnThread(sXmlDataMgr.GetConfXMLValue("ROBOT_TOWN"), sXmlDataMgr.GetConfXMLValue("ROBOT_NUM")));	
			ThreadPool.ExecuteTask(new DealAuthenticationQueueThread());	
		}
		
		if (10 == reg->getRegionX() && 3 == reg->getRegionY())
			sHallMgr.Update();
		if (10 == reg->getRegionX() && 5 == reg->getRegionY())			
			sNoticeMgr.Update(p_time);
		if (10 == reg->getRegionX() && 10 == reg->getRegionY())
			sHBMgr.CheckHongBaoTimeout();
		if (!isInitGoods && 10 == reg->getRegionX() && 7 == reg->getRegionY())
		{
			isInitGoods = true;
			sGoodsMgr.load();	
			sHBMgr.LoadHongBao();
		}
		AIInterface::Update(p_time);
	}
		
	bool	PGRegionInterface::isLoadChildren(void)
	{
		return false;
	}
}


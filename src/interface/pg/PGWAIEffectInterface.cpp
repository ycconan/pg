#include "PGWorldAIInterface.h"
#include "PGAIScript.h"

namespace AIScript
{
	WAIEffectInterface::WAIEffectInterface(Resource * pUnit):AIInterface(pUnit)
	{
		m_AIType = AITYPE_EFFECT;
	}
	WAIEffectInterface::~WAIEffectInterface()
	{
	}
	void WAIEffectInterface::Load(void)
	{
		AIInterface::Load();
	}
	void WAIEffectInterface::Update(const uint32 & p_time)
	{
		AIInterface::Update(p_time);
	}
}

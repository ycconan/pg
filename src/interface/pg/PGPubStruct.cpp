#include "PGPubStruct.h"
#include "PGRuleManager.h"

/***************************************************************/
//----------------------------------------
void Cards::Init()
{
	vecCards.clear();
	firtype = 0;
	firpoint = 0;
	sectype = 0;
	secpoint = 0;
}
//----------------------------------------
uint32 Cards::InsertCard(PaiGowType &card, SpecialType &spety)
{
	if (vecCards.size() == 4)
	{
		vecCards.clear();
		firtype = 0;
		firpoint = 0;
		sectype = 0;
		secpoint = 0;
	}
	vecCards.push_back(card);
	if (vecCards.size() == 4)
	{
		std::vector<PaiGowType> temp = vecCards;
		std::vector<PaiGowType> temp1;
		std::vector<PaiGowType> temp2;
		PaiGowType tempCard;
		if (temp[1].point > temp[0].point)
		{
			tempCard = temp[0];
			temp[0] = temp[1];
			temp[1] = tempCard;
		}
		if (temp[3].point > temp[2].point)
		{
			tempCard = temp[2];
			temp[2] = temp[3];
			temp[3] = tempCard;
		}

		temp1.push_back(temp[0]);
		temp1.push_back(temp[1]);
		firtype = sRuleMgr.Check(temp1,spety);
		firpoint = (temp[0].point + temp[1].point) % 10;

		temp2.push_back(temp[2]);
		temp2.push_back(temp[3]);
		sectype = sRuleMgr.Check(temp2, spety);
		secpoint = (temp[2].point + temp[3].point) % 10;
	}
	return 0;
}
/***************************************************************/
//----------------------------------------
void TablePlayer::Init()
{
	cards.Init();
	Originalcards.Init();
	isWin = 0;
	roundWins = 0;
	iswinScore = 0;
	cutcardflag = false;
	cutresult = 255;
	cutcardfinsh = false;
	IsMovefinish = false;
	IsMovecard = false;
	IsLookcard = false;
	IsPrompt = false;
	isChin = false;
	IsCalC = false;
	isOnceChin = false;
	movecardresult = 0;
	surplusScore = 0;
	mapNumScore.clear();
}
void TablePlayer::InitPlayer()
{
	nAgreeSameIp = 0;
	player.setNull();
}
void TablePlayer::everyInit()
{
	nAgreeSameIp = 0;
	curScore = 0;
	status = 0;
	pos = 0;
	onceBanker = false;
	banker_count = 0;
	lose_count = 0;
	win_count = 0;
}
//----------------------------------------
void TablePlayer::AddSeatChip(const uint32 seatPos, const uint32 addChip)
{
	std::map<uint32, uint32>::iterator iter = mapNumScore.find(seatPos);
	if (iter == mapNumScore.end())
	{
		mapNumScore.insert(std::make_pair(seatPos, addChip));
	}
	else
		iter->second += addChip;
}



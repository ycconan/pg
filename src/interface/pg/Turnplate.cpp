#include "Character.h"
#include "Message.h"
#include "NGLog.h"
#include "PGPublicLogic.h"
#include "PGManualThreadsManager.h"
#include "PGLogDataManager.h"
#include "Tools.h"
#include "NoticeManager.h"
#include "VipCardManager.h"
#include "PGXmlDataManager.h"
#include "Turnplate.h"
#include "PGWorldAIInterface.h"
#include "PGProtocolsManager.h"

//-------------------------------------------
Turnplate::Turnplate()
{
	m_vecTopList.assign(3,"");
}
//-------------------------------------------
Turnplate::~Turnplate()
{
}
//--------------------------------------------------------------
void Turnplate::AddCombat(CharPtr & pChr)
{
	TurnplateConf conf = sXmlDataMgr.GetTPConf();
	if (pChr.isNull() || conf.type != 1 || conf.beginTime > time(0) || conf.endTime < time(0))
	{
		return;
	}
	using AIScript::WAICreatureInterface;
	WAICreatureInterface * pAIChar = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if(pAIChar == NULL)
	{
		return;
	}
	// 已转次数
	uint32 roundnum = pAIChar->GetXMLData(RROUND_NUM);
	// 可转次数
	uint32 canroundnum = pAIChar->GetXMLData(RSURPLUS_NUM);	
	if(roundnum + canroundnum >= conf.maxNum)
	{
		return;
	}
	
	// 当前对局进度
	uint32 num = pAIChar->GetXMLData(RCOMBAT_NUM);
	if(++num >= conf.torc)
	{// 当前可转次数
		++canroundnum;
		num = 0;
		pAIChar->SetXMLData(RSURPLUS_NUM, canroundnum);
	}
	pAIChar->SetXMLData(RCOMBAT_NUM, num);
	WorldPacket newP;
	if(sProtocolsMgr.CreateTurnplateUpdatePacket(&newP, num, conf.torc, canroundnum, conf.maxNum - roundnum))
		sPubLogic.SendProtocolsToChr(pChr, &newP);
}
//-------------------------------------------
void Turnplate::Turn(CharPtr &pChr, uint32 &id)
{
	if (pChr.isNull())
	{
		return;
	}
	TurnplateConf conf = sXmlDataMgr.GetTPConf();
	if (conf.beginTime > time(0) || conf.endTime < time(0))
	{
		return;
	}
	id = GetTurnplatePos(pChr);
	using AIScript::WAICreatureInterface;
	WAICreatureInterface * pAIChar = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (pAIChar == NULL)
		return;
	// 可转次数
	uint32 canroundnum = pAIChar->GetXMLData(RSURPLUS_NUM);	
	// 已转次数
	uint32 roundnum = pAIChar->GetXMLData(RROUND_NUM);
	if (canroundnum == 0 || roundnum >= conf.maxNum)
		return;
	pAIChar->SetXMLData(RSURPLUS_NUM, --canroundnum);
	pAIChar->SetXMLData(RROUND_NUM, ++roundnum);
	// 当前对局进度
	uint32 num = pAIChar->GetXMLData(RCOMBAT_NUM);
	WorldPacket newP;
	if(sProtocolsMgr.CreateTurnplateUpdatePacket(&newP, num, conf.torc, canroundnum, conf.maxNum - roundnum))
		sPubLogic.SendProtocolsToChr(pChr, &newP);
}
//-------------------------------------------
uint32 Turnplate::GetTurnplatePos(CharPtr &pChr)
{
	if (pChr.isNull())
	{
		return 0;
	}
	const Roulette* pRoulette = sXmlDataMgr.GetRoulette();
	if(pRoulette == NULL)
	{
		return 0;
	}
	Roulette rt = *pRoulette;
	if (rt.showNum > 0)
	{
		if (m_mapShowNum[rt.id] >= rt.showNum)
		{
			sXmlDataMgr.GetRoulette(9, rt);
		}
		else
		{
			++m_mapShowNum[rt.id];
		}
	}
	if (rt.id < 4)
	{
		m_vecTopList[rt.id - 1] = pChr->getStringField("name");
		WorldPacket newP;
		if(sProtocolsMgr.CreateTurnplateTopPacket(&newP, m_vecTopList))
			sPubLogic.BroadcastEx(&newP);
	}
	return rt.id;
}
//-------------------------------------------
bool Turnplate::GetBounty(CharPtr &pChr, const uint32 id)
{
	if (pChr.isNull())
		return false;
	char msg[512];
	memset(msg, 0, sizeof(msg));
	Roulette rt;
	if (!sXmlDataMgr.GetRoulette(id, rt))
		return false;
	UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
	// 发公告
	if ((rt.flag & 1) != 0)
	{
		switch (rt.id)
		{
			case 1:
				sprintf(msg, sPubLogic.GetMsgData("rt_one", pUser).c_str(), pChr->getStringField("name").c_str(), rt.desc.c_str());
				break;
			case 2:
				sprintf(msg, sPubLogic.GetMsgData("rt_two", pUser).c_str(), pChr->getStringField("name").c_str(), rt.desc.c_str());
				break;
			case 3:
				sprintf(msg, sPubLogic.GetMsgData("rt_three", pUser).c_str(), pChr->getStringField("name").c_str(), rt.desc.c_str());
				break;
			default:
				break;
		}
		sNoticeMgr.AddMsgToQueue(msg, 101);
	}
	if ((rt.flag & 8) != 0)
	{
		VIPCard vip = sVipCardMgr.GetVipCard(rt.model_id);
		if (vip.id > 0)
		{
			vip.coins *= rt.num;
			vip.money *= rt.num;
			vip.days *= rt.num;
			vip.morraNum *= rt.num;
			sVipCardMgr.AddNewVIPInfo(pChr, vip);
			if (vip.coins > 0)
			{
				sPubLogic.addItemNum(pChr, ItemModel_Coins, vip.coins);
			}
			sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_Roulette, ItemModel_Coins, en_UNT_Add, vip.coins);
		}
	}
	if ((rt.flag & 4) != 0)
	{
		sPubLogic.addItemNum(pChr, rt.model_id, rt.num);
	}
	sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_Roulette, rt.model_id, en_UNT_Add, rt.num);
	return true;
}
//-------------------------------------------
void Turnplate::ResetData()
{
	m_mapShowNum.clear();
}
//--------------------------------------------------------------
bool Turnplate::CheckCombat(CharPtr & pChr, const bool fromLogin)
{
	TurnplateConf conf = sXmlDataMgr.GetTPConf();
	if (pChr.isNull() || conf.type != 1 || conf.beginTime > time(0) || conf.endTime < time(0))
	{
		return false;
	}
	using AIScript::WAICreatureInterface;
	WAICreatureInterface * pAIChar = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (pAIChar == NULL)
		return false;
	if(fromLogin && !sTools.IsSameDate(time(0), pAIChar->GetLastLoginTime()))
	{
		pAIChar->SetXMLData(RCOMBAT_NUM, 0);
		pAIChar->SetXMLData(RROUND_NUM, 0);
		pAIChar->SetXMLData(RSURPLUS_NUM, 0);
	}
	// 已转次数
	uint32 roundnum = pAIChar->GetXMLData(RROUND_NUM);
	// 可转次数
	uint32 canroundnum = pAIChar->GetXMLData(RSURPLUS_NUM);	
	// 当前对局进度
	uint32 num = pAIChar->GetXMLData(RCOMBAT_NUM);
	WorldPacket newP;
	if(sProtocolsMgr.CreateTurnplateUpdatePacket(&newP, num, conf.torc, canroundnum, conf.maxNum - roundnum))
		sPubLogic.SendProtocolsToChr(pChr, &newP);
	if(sProtocolsMgr.CreateTurnplateTopPacket(&newP, m_vecTopList))
		sPubLogic.SendProtocolsToChr(pChr, &newP);
	return true;
}

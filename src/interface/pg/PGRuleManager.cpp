#include "PGRuleManager.h"


//----------------------------------------
CompareResult Rule::CompareCardValue(const uint32 sCard, const uint32 oCards)	
{
	return sCard >= oCards ? en_PGCompareResult_Greater : en_PGCompareResult_Less;
}
//----------------------------------------
CompareResult Rule::CompareCardtype(const uint32 sType, const uint32 oType)
{
	return sType <= oType ? en_PGCompareResult_Greater : en_PGCompareResult_Less;
}
//----------------------------------------
RuleManager::RuleManager()
{}	
//----------------------------------------
RuleManager::~RuleManager()
{//析构
	std::map<CardsType, Rule*>::iterator iter = m_mapRules.begin();
	for (; iter != m_mapRules.end(); ++iter)
	{
		try
		{
			delete iter->second;
			iter->second = NULL;
		}
		catch(...){}//捕获异常对象
	}
	m_mapRules.clear();
}
//----------------------------------------
void RuleManager::RegisterRule(CardsType type, Rule *pRule)//使用基类指针来调用子类的方法（虚函数只能借助指针或引用达到多态）
{
	std::map<CardsType, Rule*>::iterator iter = m_mapRules.find(type);
	if (iter != m_mapRules.end())
		iter->second = pRule;
	else
		m_mapRules.insert(std::make_pair(type,pRule));
	
}
//----------------------------------------
void RuleManager::OtherSingleRegisterRule(Rule *pRule)
{
	std::vector<CardsType> singleType;
	singleType.push_back(en_CardsType_Day);
	singleType.push_back(en_CardsType_Earth);
	singleType.push_back(en_CardsType_Man);
	singleType.push_back(en_CardsType_Goose);
	singleType.push_back(en_CardsType_Long);
	singleType.push_back(en_CardsType_Short);
	singleType.push_back(en_CardCheck_Miscellaneous);
	std::vector<CardsType>::iterator iter = singleType.begin();
	for (; iter != singleType.end(); ++iter)
	{
		RegisterRule(*iter, pRule);
		Log.Debug("register_PG_world_create_ai", "Register singleCard---[%u] OK!", *iter);
	}
}
//----------------------------------------
CardsType RuleManager::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		std::map<CardsType, Rule *>::iterator iter = m_mapRules.begin();
		for (; iter != m_mapRules.end(); ++iter)
		{
			type = iter->second->Check(vecCards,spety);
			if (en_CardsType_None != type)
				break;
		}
	}
	return type;
} 
//----------------------------------------
CompareResult RuleManager::Compare(const Cards &self, const Cards &other,const uint8 &opt)
{
	CompareResult result = en_PGCompareResult_None;
	if ((self.firtype != en_CardsType_None || other.firtype != en_CardsType_None)
	&& (self.sectype != en_CardsType_None || other.sectype != en_CardsType_None))
	{
		CompareResult FirResult = en_PGCompareResult_None;
		CompareResult SecResult = en_PGCompareResult_None;
		//key关键字    value储存的值
		std::map<CardsType, Rule *>::iterator iter1 = m_mapRules.find((CardsType)self.firtype); //传入键值key
		if (iter1 != m_mapRules.end())
			FirResult = iter1->second->Compare(self.firtype, other.firtype, self.firpoint, other.firpoint);
		if (opt == en_ComOpt_First)
			return FirResult;

			std::map<CardsType, Rule *>::iterator iter2 = m_mapRules.find((CardsType)self.sectype); //传入键值key
		if (iter2 != m_mapRules.end())
			SecResult = iter2->second->Compare(self.sectype, other.sectype, self.secpoint, other.secpoint);
		if (opt == en_ComOpt_Second)
			return SecResult;

		result = CalculationResults(FirResult, SecResult);//比较最终结果
	}


	return result;
}
//----------------------------------------
CompareResult RuleManager::CalculationResults(const uint32 &fresult, const uint32 &sresult)
{
	CompareResult Result = en_PGCompareResult_None;
	if (fresult == sresult)
	{
		if (fresult == en_PGCompareResult_Greater)
			Result = en_PGCompareResult_Greater;
		if (fresult == en_PGCompareResult_Less)
			Result = en_PGCompareResult_Less;
	}
	else
		Result = en_PGCompareResult_Draw;
	//Log.Debug("RuleManager::CalculationResults", "比牌结果:%d ", Result);
	return Result;
}
//----------------------------------------
BarType RuleManager::CheckBarType(const uint32 &bartype, const uint32 &barpoint)
{
	if (bartype == en_CardsType_None)
		return en_BarType_None;
	if (bartype < en_CardsType_Day)
		return en_BarType_Head;
	else
	{
		if ((bartype == en_CardsType_Day && barpoint == 0) ||
			(bartype == en_CardsType_Earth && barpoint == 0))
			return en_BarType_Head;
		else
		{
			if (barpoint > 8)
				return en_BarType_Head;
			else
			{
				if (barpoint == 8)
					return en_BarType_Second;
				else
					return en_BarType_Third;
			}
		}
	}
}
//----------------------------------------
BarType RuleManager::CompareBarType(const BarType firBarType, const BarType secBarType)
{
	return firBarType <= secBarType ? firBarType : secBarType;
}
//////////////////////////炸弹一///////////////////////////////
//----------------------------------------
CardsType BombOneRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		//Log.Debug("BombOneRule::Check", "炸弹一:%d 地九娘娘:%d 鬼子:%d 天王九:%d ", spety.IsZdy,  spety.Isdjnn, spety.Isgz,spety.Istwj);
		if (vecCards[0].point == 8 && vecCards[1].point == 3)
		{
			type = en_CardsType_BombOne;
			if (!spety.IsZdy) // 设置是否为特殊牌型
			{
				//Log.Debug("BombOneRule::Check", "没有勾选炸弹一");
				if ((vecCards[0].color == vecCards[1].color)
				&& (vecCards[0].two == true && vecCards[1].two == false))
				type = en_CardsType_Man; 
				if ((vecCards[0].two ==false && vecCards[1].two ==false))
					type = en_CardCheck_Miscellaneous;
			}
			if(type == en_CardsType_BombOne)
				Log.Debug("BombOneRule::Check", "炸弹一:%d", type);
			else if(type == en_CardsType_Man)
				Log.Debug("BombOneRule::Check", "人%d", 1);
			else
				Log.Debug("BombOneRule::Check", "杂%d", 1);
		}
		
	}
	return type;
}
//----------------------------------------
CompareResult BombOneRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
		CompareResult result = en_PGCompareResult_None;
		Log.Debug("BombOneRule::Compare", "炸弹一:%d", stype);
		if (en_CardsType_BombOne != stype || en_CardsType_None == otype)
		{
			Log.Debug("BombOneRule::Compare1", "炸弹一:%d", stype);
			return result;
		}
			
		result = CompareCardtype(stype, otype);
		return result;
}
////////////////////////////鬼子/////////////////////////////
//----------------------------------------
CardsType DevilRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if (vecCards[0].point == 11 && vecCards[1].point == 9)
		{
			type = en_CardsType_Devil;
			if (!spety.Isgz)
			{
				//Log.Debug("DevilRule::Check", "没有勾选鬼子");
				type = en_CardsType_Short;
			}
			if (type == en_CardsType_Devil)
				Log.Debug("DevilRule::Check", "鬼子:%d", type);
			else
				Log.Debug("DevilRule::Check", "短%d", 0);
		}
		
	}
	return type;
}
//----------------------------------------
CompareResult DevilRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_Devil != stype || en_CardsType_None == stype)
	{
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
//////////////////////////至尊宝///////////////////////////////
//----------------------------------------
CardsType ExtremeRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point == 6 && vecCards[1].point == 3 ) 
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_Extreme;
			Log.Debug("ExtremeRule::Check", "至尊宝:%d",type);
		}
	}
 	return type;
}
//----------------------------------------
CompareResult ExtremeRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_Extreme != stype || en_CardsType_None == otype)
	{
		Log.Debug("ExtremeRule::Compare", "至尊宝:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////天王九//////////////////////////////
//----------------------------------------
CardsType KingNineRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		
		if (vecCards[0].point == 12 && vecCards[1].point == 9)
		{
			type = en_CardsType_KingNine;
			if (!spety.Istwj)//非特殊牌时
			{
				//Log.Debug("KingNineRule::Check", "没有勾选天王九");
				type = en_CardsType_Day;
			}
			if (type == en_CardsType_KingNine)
				Log.Debug("KingNineRule::Check", "天王九:%d", type);
			else
				Log.Debug("KingNineRule::Check", "天%d", 1);
		}
	
	}
	return type;
}
//----------------------------------------
CompareResult KingNineRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_KingNine != stype || en_CardsType_None == stype)
	{
		Log.Debug("KingNineRule::Compare", "天王九:%d", stype);
		return en_PGCompareResult_None;
	}
	result = CompareCardtype(stype, otype);
	return result;
}
////////////////////////////双天/////////////////////////////
//----------------------------------------
CardsType doubleDayRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if (vecCards[0].point == 12 && vecCards[1].point == 12)
		{
			type = en_CardsType_DoubleDay;
			Log.Debug("doubleDayRule::Check", "双天:%d", type);
		}
			
	}
	return type;
}
//----------------------------------------
CompareResult doubleDayRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleDay != stype || en_CardsType_None == stype)
	{
		Log.Debug("doubleDayRule::Compare", "双天:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
////////////////////////////地九娘娘/////////////////////////////
//----------------------------------------
CardsType EarthNineLadyRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if (vecCards[0].point == 9 && vecCards[1].point== 2)
		{
			type = en_CardsType_EarthNineLady;
			if (!spety.Isdjnn)
			{
				//Log.Debug("EarthNineLadyRule::Check", "没有勾选地九娘娘");
				type = en_CardsType_Earth;
			}
			if (type == en_CardsType_EarthNineLady)
				Log.Debug("EarthNineLadyRule::Check", "地九娘娘:%d", type);
			else
				Log.Debug("EarthNineLadyRule::Check", "地%d", 1);
		}
		
	}
	return type;
}
//----------------------------------------
CompareResult EarthNineLadyRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_EarthNineLady != stype || en_CardsType_None == stype)
	{
		Log.Debug("EarthNineLadyRule::Compare", "地九娘娘:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////双地//////////////////////////////
//----------------------------------------
CardsType DoubleEarthRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if (vecCards[0].point ==2 && vecCards[1].point==2)
		{
			type = en_CardsType_DoubleEarth;
			Log.Debug("DoubleEarthRule::Check", "双地:%d", type);
		}

	}
	return type;
}
//----------------------------------------
CompareResult DoubleEarthRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleEarth != stype || en_CardsType_None == otype)
	{
		Log.Debug("DoubleEarthRule::Compare", "双地:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);  
	return result;
}
//////////////////////////双人///////////////////////////////
//----------------------------------------
CardsType DoubleManRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==8 && vecCards[1].point == 8) 
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_DoubleMan;
			Log.Debug("DoubleManRule::Check", "双人:%d", type);
		}
			
	}
	return type;
}
//----------------------------------------
CompareResult DoubleManRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleMan != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoubleManRule::Compare", "双人:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
//////////////////////////双鹅///////////////////////////////
//----------------------------------------
CardsType TwoGooseRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==4 && vecCards[1].point == 4) 
		&& (vecCards[0].color ==true && vecCards[1].color ==true)&& (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_TwoGoose;
			Log.Debug("TwoGooseRule::Check", "双鹅:%d", type);
		}
		
	}
	return type;
}
//----------------------------------------
CompareResult TwoGooseRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_TwoGoose != stype || en_CardsType_None == stype)
	{
		Log.Debug("TwoGooseRule::Compare", "双鹅:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
//////////////////////////双梅///////////////////////////////
//----------------------------------------
CardsType DoublePlumRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==10 && vecCards[1].point == 10) 
		&& (vecCards[0].color == false && vecCards[1].color == false) && (vecCards[0].two == vecCards[1].two))
		{
			Log.Debug(" DoublePlumRule::Check", "双梅:%d", type);
			type = en_CardsType_DoublePlum;
		}
	
	}
	return type;
}
//----------------------------------------
CompareResult DoublePlumRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoublePlum != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoublePlumRule::Compare", "双梅:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
//////////////////////////双长三///////////////////////////////
//----------------------------------------
CardsType DoubleLongthreeRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==6 && vecCards[1].point ==6)
		&& (vecCards[0].color == false && vecCards[1].color == false) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_DoubleLongThree;
			Log.Debug(" DoubleLongthreeRule::Check", "双长三:%d", type);
		}
		
	}
	return type;
}
//----------------------------------------
CompareResult DoubleLongthreeRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleLongThree != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoubleLongthreeRule::Compare", "双长三:%d", stype);
		return en_PGCompareResult_None;
	
	}
	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////两板凳//////////////////////////////
//----------------------------------------
CardsType DoubleStoolRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==4 && vecCards[1].point == 4)
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_DoubleStool;
			Log.Debug(" DoubleStoolRule::Check", "凉板凳:%d", type);
		}
			
	}
	return type;
}
//----------------------------------------
CompareResult DoubleStoolRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleStool != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoubleStoolRule::Compare", "两板凳:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////两斧头//////////////////////////////
//----------------------------------------
CardsType DoubleAxeRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if (vecCards[0].point ==11 &&vecCards[1].point==11)
		{
			type = en_CardsType_DoubleAxe;
			Log.Debug(" DoubleAxeRule::Check", "两斧头:%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult DoubleAxeRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleAxe != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoubleAxeRule::Compare", "两斧头:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////红头十//////////////////////////////
//----------------------------------------
CardsType RedHreadRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==10 && vecCards[1].point  == 10)
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_RedHeadTen;
			Log.Debug(" RedHreadRule::Check", "红头十:%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult RedHreadRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_RedHeadTen != stype || en_CardsType_None == stype)
	{
		Log.Debug("RedHreadRule::Compare", "红头十:%d", stype);
		return en_PGCompareResult_None;
	}
	
	result = CompareCardtype(stype, otype);
	return result;
}
////////////////////////////双高七/////////////////////////////
//----------------------------------------
CardsType DoubleHightSevenRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==7 && vecCards[1].point == 7 )
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_DoubleHighSeven;
			Log.Debug(" DoubleHightSevenRule::Check", "双高七:%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult DoubleHightSevenRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DoubleHighSeven != stype || en_CardsType_None == stype)
	{
		Log.Debug("DoubleHightSevenRule::Compare", "双高七:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////铜锤//////////////////////////////
//----------------------------------------
CardsType PratiaRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==6 && vecCards[1].point == 6) 
		&& (vecCards[0].color == vecCards[1].color) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_Pratia;
			Log.Debug(" PratiaRule::Check", "铜锤 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult PratiaRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_Pratia != stype || en_CardsType_None == stype)
	{
		Log.Debug("PratiaRule::Compare", "铜锤:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////杂九//////////////////////////////
//----------------------------------------
CardsType MiscellaneousNineRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==9 && vecCards[1].point == 9) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_MiscellaneousNine;
			Log.Debug("  MiscellaneousNineRule::Check", "杂九 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult MiscellaneousNineRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_MiscellaneousNine != stype || en_CardsType_None == stype)
	{
		Log.Debug("MiscellaneousNineRule::Compare", "杂九:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////杂八//////////////////////////////
//----------------------------------------
CardsType MiscellaneousEightRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==8 && vecCards[1].point == 8) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_MiscellaneousEight;
			Log.Debug("MiscellaneousEightRule::Check", "杂八 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult MiscellaneousEightRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_MiscellaneousEight != stype || en_CardsType_None == stype)
	{
		Log.Debug("MiscellaneousEightRule::Compare", "杂八:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
/////////////////////////杂七////////////////////////////////
//----------------------------------------
CardsType MiscellaneousSevenRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==7 && vecCards[1].point == 7) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_MiscellaneousSeven;
			Log.Debug("MiscellaneousSevenRule::Check", "杂七 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult MiscellaneousSevenRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_MiscellaneousSeven != stype || en_CardsType_None == stype)
	{
		Log.Debug("MiscellaneousSevenRule::Compare", "杂七:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////杂五//////////////////////////////
//----------------------------------------
CardsType MiscellaneousFiveRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point ==5 && vecCards[1].point == 5) && (vecCards[0].two == vecCards[1].two))
		{
			type = en_CardsType_MiscellaneousFive;
			Log.Debug("MiscellaneousFiveRule::Check", "杂五 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult MiscellaneousFiveRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_MiscellaneousFive != stype || en_CardsType_None == stype)
	{
		Log.Debug("MiscellaneousFiveRule::Compare", "杂五:%d", stype);
		return en_PGCompareResult_None;
	}

	result = CompareCardtype(stype, otype);
	return result;
}
/////////////////////////天杠////////////////////////////////
//----------------------------------------
CardsType DayBarRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point == 12 && vecCards[1].point == 8))
		{
			type = en_CardsType_DayBar;
			Log.Debug("DayBarRule::Check", "天杠 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult DayBarRule::Compare(const uint32 &stype, const uint32 &otype, const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_DayBar != stype || en_CardsType_None == stype)
	{
		Log.Debug("DayBarRule::Compare", "天杠:%d", stype);
		return en_PGCompareResult_None;
	}
	result = CompareCardtype(stype, otype);
	return result;
}
///////////////////////////地杠//////////////////////////////
//----------------------------------------
CardsType EarthBarRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		if ((vecCards[0].point == 8 && vecCards[1].point == 2))
		{
			type = en_CardsType_EarthBar;
			Log.Debug("EarthBarRule::Check", "地杠 :%d", type);
		}
	}
	return type;
}
//----------------------------------------
CompareResult EarthBarRule::Compare(const uint32 &stype, const uint32 &otype, const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardsType_EarthBar != stype || en_CardsType_None == stype)
	{
		Log.Debug("EarthBarRule::Compare", "地杠:%d", stype);
		return en_PGCompareResult_None;
	}
	result = CompareCardtype(stype, otype);
	return result;
}
//////////////////////////非对牌///////////////////////////////
//----------------------------------------
CardsType OtherSingleRule::Check(std::vector<PaiGowType> &vecCards,SpecialType &spety)
{
	CardsType type = en_CardsType_None;
	if (vecCards.size() == 2)
	{
		//Log.Debug("OtherSingleRule::Check", "牌点数<(%d %d %d),(%d %d %d)>", 
	//	vecCards[0].point, vecCards[0].color, vecCards[0].two, vecCards[1].point, vecCards[1].color, vecCards[1].two);
		uint32 num;
		//天
		if (vecCards[0].point == 12 && vecCards[1].point != 12)
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Day;
			Log.Debug("OtherSingleRule::Check", "天%d", num);
		}
		//地
		else if (vecCards[1].point == 2 && vecCards[0].point != 2)
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Earth;
			Log.Debug("OtherSingleRule::Check", "地%d", num);
		}
		//人
		else if ((vecCards[0].point == 8 && vecCards[0].two == true && vecCards[0].color == true) 
		|| (vecCards[1].point == 8 && vecCards[1].two == true && vecCards[1].color == true))
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Man;
			Log.Debug("OtherSingleRule::Check", "人%d", num);
		}
		//鹅
		else if ((vecCards[0].point == 4 && vecCards[0].color == true && vecCards[0].two == true) 
		|| (vecCards[1].point == 4 && vecCards[1].color == true && vecCards[1].two == true))
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Goose;
			Log.Debug("OtherSingleRule::Check", "鹅%d", num);
		}
		//长
		else if (((vecCards[0].point == 10 && vecCards[0].color == false && vecCards[0].two == true) 
		|| (vecCards[0].point == 6 && vecCards[0].color == false)
		|| (vecCards[0].point == 4 && vecCards[0].color == false && vecCards[0].two == true))
		
		|| ((vecCards[1].point == 10 && vecCards[1].color == false && vecCards[1].two == true)
		|| (vecCards[1].point == 6 && vecCards[1].color == false) 
		|| (vecCards[1].point == 4 && vecCards[1].color == false && vecCards[1].two == true)))
		{

			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Long;
			Log.Debug("OtherSingleRule::Check", "长%d", num);
		}
		//短
		else if ((vecCards[0].point == 11
		|| (vecCards[0].point == 10 && vecCards[0].color == true) 
		|| (vecCards[0].point == 7 && vecCards[0].color == true && vecCards[0].two == true)
		|| (vecCards[0].point == 6 && vecCards[0].color == true && vecCards[0].two == true))

		|| (vecCards[1].point == 11
		|| (vecCards[1].point == 7 && vecCards[1].color == true && vecCards[1].two == true)
		|| (vecCards[1].point == 6 && vecCards[1].color == true && vecCards[1].two == true)))
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardsType_Short;
			Log.Debug("OtherSingleRule::Check", "短%d", num);
		}
		
		//杂
		else if((vecCards[0].point == 9
		||(vecCards[0].point == 8 && vecCards[0].two == false)
		||(vecCards[0].point == 7 && vecCards[0].two == false)
		||(vecCards[0].point == 5 && vecCards[0].two == false))

		||(vecCards[1].point == 5 && vecCards[0].two == false))
		{
			num = (vecCards[0].point + vecCards[1].point) % 10;
			type = en_CardCheck_Miscellaneous;
			Log.Debug("OtherSingleRule::Check", "杂%d", num);
		}
	}
	return type;
}
//----------------------------------------
CompareResult OtherSingleRule::Compare(const uint32 &stype, const uint32 &otype ,const uint32 &spoint, const uint32 &opoint)
{
	CompareResult result = en_PGCompareResult_None;
	if (en_CardCheck_Miscellaneous < stype || en_CardsType_None == otype)
		return result;
	//Log.Warning("OtherSingleRule::Compare", "spoint:%d opoint:%d", spoint, opoint);
	if (otype < en_CardsType_Day)//其他牌是特殊牌（包括天杠和地杠）
		result = en_PGCompareResult_Less;
	else
	{
		if (spoint > opoint) //先比较点数
			result = en_PGCompareResult_Greater;
		else if (spoint == opoint) //若点数相同比较牌型
		{
			if (spoint == 0) //毕十最小
				result = en_PGCompareResult_Greater;
			else
			{
				if(stype <= otype)
					result = en_PGCompareResult_Greater;
				else
					result = en_PGCompareResult_Less;
			}
		}
		else
			result = en_PGCompareResult_Less;
	}
	return result;

}
//----------------------------------------

#include "PGProtocolsManager.h"
#include "Channels.h"
#include "ChannelManager.h"
#include "Resource.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "Character.h"
#include "CharManager.h"
#include "Items.h"
#include "ItemManager.h"
#include "Users.h"
#include "UserManager.h"
#include "Towns.h"
#include "TownManager.h"
#include "Mails.h"
#include "MailManager.h"
#include "XMLData.h"
#include "Effects.h"
#include "EffectManager.h"
#include "EffectModels.h"
#include "EffectModelManager.h"
#include "Missions.h"
#include "Tools.h"
#include "PGOpcodesEx.h"
#include "OpcodesEx.h"
#include "PGPubStruct.h"
#include "PGWorldAIInterface.h"
#include "VipCardManager.h"
#include "GameDefine.h"
#include LOGIC_MGR_PATH
#include XMLDATA_MGR_PATH

//-----------------------------------------------------------------------
PGProtocolsManager::PGProtocolsManager(void)
{
}
//-----------------------------------------------------------------------
PGProtocolsManager::~PGProtocolsManager(void)
{
}
//-----------------------------------------------------------------------
bool PGProtocolsManager::CreateGoHallOrTablePacket(WorldPacket *packet, const uint8 where)
{
	return CreateSimplePacket(packet, SMSG_PG_GOHALLORTABLE, where);
}
// 创建棋牌圈玩家列表协议包 0x0140
bool PGProtocolsManager::CreatePlayerListPacket(WorldPacket *packet, std::vector<TablePlayer> vChars)
{
	if (!packet || vChars.empty())
		return false;

	uint16 packlen = 10;
	packet->clear();
	packet->SetOpcode(SMSG_CENTERBATTLE_PLAYERLIST);
	*packet << uint16(SMSG_CENTERBATTLE_PLAYERLIST) << uint16(packlen);

	uint32 offset = packet->size();
	uint8 num = 0;

	*packet << uint8(vChars.size()); // 总座位数量

	std::vector<TablePlayer>::iterator iter, ei = vChars.end();
	for (iter = vChars.begin(); iter != ei; ++iter)
	{
		CharPtr pChr = sCharMgr.load((*iter).player._handle);
		if (pChr.isNull())
		{
			continue;
		}
		else
		{
			*packet << uint32(pChr->getHandle()); // 角色ID

			String strIP = sGLMgr.GetCharVagueIp(pChr);
			double longitude = 0.0, latitude = 0.0, altitude = 0.0; // 经纬度海拔
			uint32 pid = 0;

			UserPtr pUser = sUserMgr.load(pChr->getUInt32Field("userid"));
			if (!pUser.isNull())
			{
				pid = pUser->getUInt32Field("platform_id");
				sGLMgr.GetUserLongitudeAndLatitude(pid, longitude, latitude, altitude);
			}

			*packet << uint32(pid)				 // PID
					<< uint8((*iter).pos)		 // 座位（从0开始）
					<< int32((*iter).curScore)   // 当前积分
					<< uint8((*iter).bFz)		 // 房主标志
					<< uint8((*iter).status)	 // 角色状态（1 空闲 2 准备 3 打牌）
					<< uint8((*iter).isOffLine)// 离线（0 在线 1 离线）
					<< uint8(pChr->getUInt32Field("sex"));
			AppendPacketString<uint8>(packet, pChr->getStringField("name")); // 名字
			AppendPacketString<uint8>(packet, pChr->getStringField("desc")); // 头像
			
			sGLMgr.PacketPlayerSameInfo(packet, pChr); // 地址

			AppendPacketString<uint8>(packet, strIP); // IP地址
			*packet << double(longitude)			  // 精度
					<< double(latitude)				  // 纬度
					<< double(altitude);			  // 海拔

			++num;
		}
	}

	Log.Warning("PGProtocolsManager::CreatePlayerListPacket", "vChars size[%u]", vChars.size());
	packet->SetOffset(offset, num);
	packet->SetLength(packet->size());
	return true;
}

//-----------------------------------------------------------------------
//牌桌状态
bool PGProtocolsManager::CreateTableStateUpdatePacket(WorldPacket *packet, const uint8 channelState, const uint32 seconds)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(64);
	packet->SetOpcode(SMSG_TABLESTATEUPDATE);
	*packet << uint16(SMSG_TABLESTATEUPDATE) << uint16(0)
			<< channelState
			<< seconds
			<< uint32(time(0));
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//广播骰子数
bool PGProtocolsManager::CreateBroadcastDiceResultPacket(WorldPacket *packet, const uint32 &dices1, const uint32 &dices2,const uint8 &speakpos , const uint32 &playerId)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(64);
	packet->SetOpcode(SMSG_DICENUMBER);
	*packet << uint16(SMSG_DICENUMBER) << uint16(0)
			<< uint8(dices1)
			<< uint8(dices2)
			<< uint8(playerId)
			<< uint8(speakpos)
			<< uint8(true);

		// 设置包真正的长度
		packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//发送已出牌列表
bool PGProtocolsManager::CreateGetOutcardsPacket(WorldPacket *packet, std::vector<TablePlayer>  &lstoutcards, const bool bShow)
{
	if (!packet)
		return false;
	packet->clear();
		// 预开辟空间
		packet->reserve(64);
		packet->SetOpcode(SMSG_GETOUTCARDS);
		*packet << uint16(SMSG_GETOUTCARDS) << uint16(0);
		*packet << uint8(4); // 玩家数量
		Log.Warning("PGProtocolsManager::CreateBankerLookCardsPacket", "列表大小2：%d", lstoutcards.size());

		// 牌数
		std::vector<TablePlayer>::iterator iter = lstoutcards.begin();
		for (; iter != lstoutcards.end(); ++iter)
		{
			CharPtr pChr = sCharMgr.getByHandle((*iter).player._handle);

			*packet << uint8((*iter).cards.firtype)			  // 前两张牌型
					<< uint8((*iter).cards.firpoint)		  // 前两张点数
					<< uint8((*iter).cards.vecCards[0].point) // 第一张牌点数
					<< uint8((*iter).cards.vecCards[0].color) //第一张牌的花色
					<< uint8((*iter).cards.vecCards[0].two)   //第一张牌的张数
					<< uint8((*iter).cards.vecCards[1].point) // 第二张牌点数
					<< uint8((*iter).cards.vecCards[1].color) //第二张牌的花色
					<< uint8((*iter).cards.vecCards[1].two)   //第二张牌的张数

					<< uint8((*iter).cards.sectype)							 // 后两张牌型
					<< uint8((*iter).cards.secpoint)						 // 后两张点数
					<< uint8((*iter).cards.vecCards[2].point)				 // 第三张牌点数
					<< uint8((*iter).cards.vecCards[2].color)				 //第三张牌的花色
					<< uint8((*iter).cards.vecCards[2].two)					 //第三张牌的张数
					<< uint8((*iter).cards.vecCards[3].point)				 // 第四张牌点数
					<< uint8((*iter).cards.vecCards[3].color)				 //第四张牌的花色
					<< uint8((*iter).cards.vecCards[3].two)					 //第四张牌的张数
					<< int((*iter).roundWins);								 //本轮输赢积分
			AppendPacketString<uint8>(packet, pChr->getStringField("name")); // 名字
			AppendPacketString<uint8>(packet, pChr->getStringField("desc")); // 头像
			Log.Warning("PGProtocolsManager::CreateBankerLookCardsPacket", "积分：%d    %d-%d  %d-%d ", int((*iter).roundWins),
						(*iter).cards.firtype, (*iter).cards.firpoint, (*iter).cards.sectype, (*iter).cards.secpoint);
		}

		*packet << uint8(true);
		// 设置包真正的长度
		packet->SetLength(packet->size());
		return true;
}
//-----------------------------------------------------------------------
//当前玩家选择切牌位置
bool PGProtocolsManager::CreateBroadCutCardsPosPacket(WorldPacket *packet, const uint8 pos,const uint32 cutcardspos)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(8);
	packet->SetOpcode(SMSG_CUTCARDSPOS);
	*packet << uint16(SMSG_CUTCARDSPOS) << uint16(0)
			<< uint8(pos)			//切牌玩家座位号
			<< uint32(cutcardspos); //切牌位置索引
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//下一玩家切牌信息
bool PGProtocolsManager::CreateBroadCutResultPacket(WorldPacket *packet, TablePlayer nextplayer, const uint8 pos, const uint32 cutplayernum,const uint32 cuttime, const bool bShow)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(8);
	packet->SetOpcode(SMSG_CUTCARDS);
	*packet << uint16(SMSG_CUTCARDS) << uint16(0)
			<< uint8((nextplayer).player._handle) //下一个切牌玩家ID
			<< uint8(pos)						  //切牌玩家座位号
			<< uint8(cutplayernum)				  //已完成操作人数
			<< uint32(cuttime) //时间
			<< uint8(bShow);   // 是否全部完成操作
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//庄家看牌
bool PGProtocolsManager::CreateBankerLookCardsPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos, const bool bShow)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(16);
	packet->SetOpcode(SMSG_LOOKCARDS);
	*packet << uint16(SMSG_LOOKCARDS) << uint16(0)
			<< uint8(vecPos.size()); // 人数
	// 下发信息
	std::vector<TablePlayer>::iterator iter = vecPos.begin();
	for (; iter != vecPos.end(); ++iter)
	{
		// if ((*iter).movecardresult == 2)
		// {
		// 	*packet << uint8((*iter).cards.sectype)			 // 前两张牌型
		// 			<< uint8((*iter).cards.secpoint)			 // 前两张点数
		// 			<< uint8((*iter).cards.vecCards[2].point) // 第一张牌点数
		// 			<< uint8((*iter).cards.vecCards[2].color) //第一张牌的花色
		// 			<< uint8((*iter).cards.vecCards[2].two)   //第一张牌的张数
		// 			<< uint8((*iter).cards.vecCards[3].point) // 第二张牌点数
		// 			<< uint8((*iter).cards.vecCards[3].color) //第二张牌的花色
		// 			<< uint8((*iter).cards.vecCards[3].two)   //第二张牌的张数

		// 			<< uint8((*iter).cards.firtype)			 // 后两张牌型
		// 			<< uint8((*iter).cards.firpoint)			 // 后两张点数
		// 			<< uint8((*iter).cards.vecCards[0].point) // 第三张牌点数
		// 			<< uint8((*iter).cards.vecCards[0].color) //第三张牌的花色
		// 			<< uint8((*iter).cards.vecCards[0].two)   //第三张牌的张数
		// 			<< uint8((*iter).cards.vecCards[1].point) // 第四张牌点数
		// 			<< uint8((*iter).cards.vecCards[1].color) //第四张牌的花色
		// 			<< uint8((*iter).cards.vecCards[1].two);  //第四张牌的张数
		// }
		// else
		// {
			*packet << uint8((*iter).cards.firtype)			  // 前两张牌型
					<< uint8((*iter).cards.firpoint)		  // 前两张点数
					<< uint8((*iter).cards.vecCards[0].point) // 第一张牌点数
					<< uint8((*iter).cards.vecCards[0].color) //第一张牌的花色
					<< uint8((*iter).cards.vecCards[0].two)   //第一张牌的张数
					<< uint8((*iter).cards.vecCards[1].point) // 第二张牌点数
					<< uint8((*iter).cards.vecCards[1].color) //第二张牌的花色
					<< uint8((*iter).cards.vecCards[1].two)   //第二张牌的张数

					<< uint8((*iter).cards.sectype)			  // 后两张牌型
					<< uint8((*iter).cards.secpoint)		  // 后两张点数
					<< uint8((*iter).cards.vecCards[2].point) // 第三张牌点数
					<< uint8((*iter).cards.vecCards[2].color) //第三张牌的花色
					<< uint8((*iter).cards.vecCards[2].two)   //第三张牌的张数
					<< uint8((*iter).cards.vecCards[3].point) // 第四张牌点数
					<< uint8((*iter).cards.vecCards[3].color) //第四张牌的花色
					<< uint8((*iter).cards.vecCards[3].two);  //第四张牌的张数
		// }
		
			CharPtr pChr = sCharMgr.getByHandle((*iter).player._handle);

			Log.Warning("PGProtocolsManager::CreateBankerLookCardsPacket", "玩家昵称:%s pos:%d  %d-%d  %d-%d result：%d",
						pChr->getStringField("name").c_str(), (*iter).pos, (*iter).cards.firtype, (*iter).cards.firpoint, (*iter).cards.sectype, (*iter).cards.secpoint, (*iter).movecardresult);
	}
	*packet << uint8(bShow); // 是否播放动画
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//----------------------------------------------------------------------
//玩家看牌
bool PGProtocolsManager::CreatePlayerLookCardTypePacket(WorldPacket *packet, const uint8 result, TablePlayer tp)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(13);
	packet->SetOpcode(SMSG_PLAYLOOKCARD);
	*packet << uint16(SMSG_PLAYLOOKCARD) << uint16(0);
	// 下发信息
	*packet << uint8(tp.cards.firtype)			 // 前两张牌型
			<< uint8(tp.cards.firpoint)			 // 前两张点数
			<< uint8(tp.cards.vecCards[0].point) // 第一张牌点数
			<< uint8(tp.cards.vecCards[0].color) //第一张牌的花色
			<< uint8(tp.cards.vecCards[0].two)   //第一张牌的张数
			<< uint8(tp.cards.vecCards[1].point) // 第二张牌点数
			<< uint8(tp.cards.vecCards[1].color) //第二张牌的花色
			<< uint8(tp.cards.vecCards[1].two)   //第二张牌的张数

			<< uint8(tp.cards.sectype)			 // 后两张牌型
			<< uint8(tp.cards.secpoint)			 // 后两张点数
			<< uint8(tp.cards.vecCards[2].point) // 第三张牌点数
			<< uint8(tp.cards.vecCards[2].color) //第三张牌的花色
			<< uint8(tp.cards.vecCards[2].two)   //第三张牌的张数
			<< uint8(tp.cards.vecCards[3].point) // 第四张牌点数
			<< uint8(tp.cards.vecCards[3].color) //第四张牌的花色
			<< uint8(tp.cards.vecCards[3].two)   //第四张牌的张数
			<< result 					 //一组牌和二组牌比较结果
			<< uint8(tp.pos);				   	//当前玩家座位号
			CharPtr pChr = sCharMgr.getByHandle(tp.player._handle);
			Log.Warning("PGProtocolsManager::CreatePlayerLookCardTypePacket", "被看玩家昵称%s pos:%d ", pChr->getStringField("name").c_str(), tp.pos);
			// 设置包真正的长度
			packet->SetLength(packet->size());
			return true;
}
//玩家摆牌
bool PGProtocolsManager::CreatePlayerMoveCardPacket(WorldPacket *packet, TablePlayer tp)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(13);
	packet->SetOpcode(SMSG_PLAYMOVECARD);
	*packet << uint16(SMSG_PLAYMOVECARD) << uint16(0);
	// 下发信息
	*packet << uint8(tp.cards.firtype)  // 前两张牌型
			<< uint8(tp.cards.sectype)  // 后两张牌型
			<< uint8(tp.cards.firpoint) // 前两张点数
			<< uint8(tp.cards.secpoint); // 后两张点数

	CharPtr pChr = sCharMgr.getByHandle(tp.player._handle);
	Log.Warning("PGProtocolsManager::CreatePlayerMoveCardResultPacket", "玩家昵称:%s  %d-%d  %d-%d ", pChr->getStringField("name").c_str(), tp.cards.firtype, tp.cards.firpoint, tp.cards.sectype, tp.cards.secpoint);
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//玩家摆牌最终结果
bool PGProtocolsManager::CreatePlayerMoveCardResultPacket(WorldPacket *packet, const uint8 result)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(13);
	packet->SetOpcode(SMSG_PLAYMOVECARDFINISH);
	*packet << uint16(SMSG_PLAYMOVECARDFINISH) << uint16(0);
	// 下发信息
	*packet << uint8(result);					 //一组牌和二组牌比较结果
	Log.Warning("PGProtocolsManager::CreatePlayerMoveCardResultPacket", "最终摆牌结果:%d ", result);
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//下注结果
bool PGProtocolsManager::CreateChipInResultPacket(WorldPacket *packet, const uint8 &chiprsult, std::map<uint32, uint32> &numchips, const uint32 &num, const uint32 &surscore, const uint8 &seatpos)
{
	if (!packet )
		return false;

	uint16 packlen = 5;
	packet->clear();
	packet->SetOpcode(SMSG_CHIPIN);
	*packet << uint16(SMSG_CHIPIN) << uint16(packlen)
			<< uint8(chiprsult)
			<< uint32(numchips[1])
			<< uint32(numchips[2])
			<< uint32(numchips[3])
			<< uint32(num)
			<< uint32(surscore)
			<< uint8(seatpos);
	Log.Warning("PGProtocolsManager::CreateChipInResultPacket", "numchips[1]:%d numchips[2]:%d numchips[3]:%d ", numchips[1], numchips[2], numchips[3]);
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//通知下庄
bool PGProtocolsManager::CreateAbandonBankerPacket(WorldPacket *packet, const uint8 status)
{
	return CreateSimplePacket(packet, SMSG_ABANDONBANKER, status);
}
//-----------------------------------------------------------------------
//通知玩家抢庄
bool PGProtocolsManager::CreateNoticeGetBankerPacket(WorldPacket *packet, const uint8 &mode)
{
	if (!packet)
		return false;

	uint16 packlen = 5;
	packet->clear();
	packet->SetOpcode(SMSG_PG_NOTICEGETBANKER);
	*packet << uint16(SMSG_PG_NOTICEGETBANKER) << uint16(packlen)
			<< uint8(0);
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//广播玩家抢庄选择
bool PGProtocolsManager::CreateGetBankerResultPacket(WorldPacket *packet, const uint8 &pos,const uint8 &bwantbanker)
{
	if (!packet)
		return false;

	uint16 packlen = 7;
	packet->clear();
	packet->SetOpcode(SMSG_PG_BROADGETBANKER);
	*packet << uint16(SMSG_PG_BROADGETBANKER) << uint16(packlen)
			<< uint8(pos)
			<< uint8(0)
			<< uint8(bwantbanker);

	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//玩家抢庄结果
bool PGProtocolsManager::CreateSetBankerPosPacket(WorldPacket *packet, const uint8 &mode)
{
	if (!packet)
		return false;

	uint16 packlen = 5;
	packet->clear();
	packet->SetOpcode(SMSG_PG_GETBANKER);
	*packet << uint16(SMSG_PG_GETBANKER) << uint16(packlen)
			<< uint8(mode);
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//询问是否连庄
bool PGProtocolsManager::CreateAskIsContinueBankerPacket(WorldPacket *packet, const uint8 seconds)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(64);
	packet->SetOpcode(SMSG_CONTINUEBANKER);
	*packet << uint16(SMSG_CONTINUEBANKER) << uint16(0)
			<< seconds;
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//小结算确认
bool PGProtocolsManager::CreateAskIsRoundPGCalcPacket(WorldPacket *packet, const uint8 & Ask)
{
	if (!packet)
		return false;
	packet->clear();
	// 预开辟空间
	packet->reserve(64);
	packet->SetOpcode(SMSG_ASKISROUNDPGCALC);
	*packet << uint16(SMSG_ASKISROUNDPGCALC) << uint16(0)
			<< Ask;
	// 设置包真正的长度
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//成为庄家扣除底分
bool PGProtocolsManager::CreateSetBankerDelScorePacket(WorldPacket *packet, const uint8 &bankerpos,const int &basescore)
{

	if (!packet)
		return false;

	uint16 packlen = 9;
	packet->clear();
	packet->SetOpcode(SMSG_PG_DELSCORE);
	*packet << uint16(SMSG_PG_DELSCORE) << uint16(packlen)
			<< uint8(bankerpos)
			<< int(basescore);
	packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//断线重连确认开牌
bool PGProtocolsManager::CreateReturnPacket(WorldPacket *packet)
{
	if (!packet)
		return false;

	uint16 packlen = 5;
	packet->clear();
        packet->SetOpcode(SMSG_RETURN);
		*packet << uint16(SMSG_RETURN) << uint16(packlen);
		packet->SetLength(packet->size());
		return true;
}
//-----------------------------------------------------------------------
//每局结算
bool PGProtocolsManager::CreateRoundPGCalcPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos, const uint8 &nBankerPos, const uint8 &rType, const int &basescore, const bool bShow)
{

	if (!packet)
		return false;

	uint16 packlen = 24;
	packet->clear();
	packet->SetOpcode(SMSG_PG_ROUNDCALCSCORE);
	*packet << uint16(SMSG_PG_ROUNDCALCSCORE) << uint16(packlen)
			<< uint8(rType) //结果类型
			<< uint8(4);	//闲家数量
	std::vector<TablePlayer>::iterator iter = vecPos.begin();
	for(; iter != vecPos.end(); ++iter)
	{
		// if ((*iter).pos != nBankerPos)
		// {
			*packet << uint8((*iter).pos)
					<< uint8((*iter).isWin)
					<< int((*iter).curScore)
					<< int((*iter).roundWins)
				    << uint8((*iter).cards.firtype)			   // 前两张牌型
					<< uint8((*iter).cards.firpoint)		   	// 前两张点数
					<< uint8((*iter).cards.vecCards[0].point)  	// 第一张牌点数
					<< uint8((*iter).cards.vecCards[0].color)  	//第一张牌的花色
					<< uint8((*iter).cards.vecCards[0].two)  	 //第一张牌的张数
					<< uint8((*iter).cards.vecCards[1].point)  	// 第二张牌点数
					<< uint8((*iter).cards.vecCards[1].color)  	//第二张牌的花色
					<< uint8((*iter).cards.vecCards[1].two)	  	 //第二张牌的张数

					<< uint8((*iter).cards.sectype)			  	 // 后两张牌型
					<< uint8((*iter).cards.secpoint)		   	// 后两张点数
					<< uint8((*iter).cards.vecCards[2].point)  	// 第三张牌点数
					<< uint8((*iter).cards.vecCards[2].color)  	//第三张牌的花色
					<< uint8((*iter).cards.vecCards[2].two)	    //第三张牌的张数
					<< uint8((*iter).cards.vecCards[3].point)   // 第四张牌点数
					<< uint8((*iter).cards.vecCards[3].color)	//第四张牌的花色
					<< uint8((*iter).cards.vecCards[3].two);	//第四张牌的张数
			Log.Warning("PGProtocolsManager::CreateRoundPGCalcPacket", "玩家座位:%d 本轮输赢：%d 当前积分:%d 本轮赢得积分:%d  "
			, (*iter).pos, (*iter).isWin, (*iter).curScore, (*iter).roundWins);
			CharPtr pChr = sCharMgr.getByHandle((*iter).player._handle);
			Log.Warning("PGProtocolsManager::CreateRoundPGCalcPacket", "玩家昵称%s pos:%d firtype:%d firpoint:%d sectype:%d secpoint:%d",
						pChr->getStringField("name").c_str(), (*iter).pos, (*iter).cards.firtype, (*iter).cards.firpoint, (*iter).cards.sectype, (*iter).cards.secpoint);
			// }
	}

        *packet << int(basescore) //剩余锅底
                << uint8(bShow)   //是否播放动画
                << uint8(nBankerPos);

        packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//最终结算
bool PGProtocolsManager::CreateEndPGCalcPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos,const bool bShow)
{

	if (!packet)
		return false;

	uint16 packlen = 14;
	packet->clear();
	packet->SetOpcode(SMSG_PG_ENDCALCSCORE);
	*packet << uint16(SMSG_PG_ENDCALCSCORE) << uint16(packlen)
			<< uint8(4); //玩家数量
	std::vector<TablePlayer>::iterator iter = vecPos.begin();
	for (; iter != vecPos.end(); ++iter)
	{
		*packet << uint8((*iter).pos)			//玩家座位号
				<< uint32((*iter).lose_count)   //输次数
				<< uint32((*iter).win_count)	//赢次数
				<< uint32((*iter).banker_count) //当庄次数
				<< int((*iter).curScore);		//最终积分
				Log.Warning("PGProtocolsManager::CreateEndPGCalcPacket", "玩家座位:%d 输次数：%d 赢次数:%d 当庄次数:%d 最终积分:%d "
			, (*iter).pos, (*iter).lose_count, (*iter).win_count,(*iter).banker_count,(*iter).curScore);
	}

	*packet << uint8(bShow);					 //是否播放动画

			   packet->SetLength(packet->size());
	return true;
}
//-----------------------------------------------------------------------
//玩家角色信息包
bool PGProtocolsManager::CreateCharacterInfoPacket(WorldPacket *packet, CharPtr &pChr)
{
	if (!packet || pChr.isNull())
		return false;

	UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
	if (pUser.isNull())
		return false;

	packet->clear();
	packet->reserve(64);
	packet->SetOpcode(SMSG_PG_PLAYERBASEINFO);
	*packet << uint16(SMSG_PG_PLAYERBASEINFO) << uint16(0);

	uint32 myDiamand = sGLMgr.GetItemNum(pChr, ItemModel_DiamondOpen);

	*packet << uint32(pChr->getSerial())					// 玩家ID
			<< uint32(pUser->getUInt32Field("platform_id")) // 角色PID
			<< uint8(pChr->getUInt32Field("sex"))			// 角色性别
			<< uint8(pChr->getUInt32Field("lv"))			// 等级
			<< uint32(pUser->getUInt32Field("reg_time"))	// 注册时间
			<< uint32(myDiamand);							// 钻石

	AppendPacketString<uint8>(packet, pChr->getCharName());			 // 昵称
	AppendPacketString<uint8>(packet, pChr->getStringField("desc")); // 头像地址
	sGLMgr.PacketPlayerSameInfo(packet, pChr);						 // 地址信息
	packet->SetLength(packet->size());
	return true;
}

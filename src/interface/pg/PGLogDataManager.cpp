#include "PGLogDataManager.h"
#include "Users.h"
#include "Character.h"
#include "ChannelManager.h"
#include "BSSocket.h"
#include "WSSocket.h"
#include "StringConverter.h"
#include "Missions.h"
#include "Channels.h"
#include "PGPublicLogic.h"
#include "GameGoodsManager.h"
#include "Tools.h"
#include "PGWorldAIInterface.h"
#include "PGPubStruct.h"

#include "UserGameLogsManager.h"

//-----------------------------------------------------------------------
PGLogDataManager::PGLogDataManager()
{}
//-----------------------------------------------------------------------
PGLogDataManager::~PGLogDataManager()
{}
//-----------------------------------------------------------------------
// String PGLogDataManager::GetUTF8String(String &str)
// {
	// if (StringUtil::checkStringUTF8(str))
		// return str;
	// return StringUtil::StringToUTF8(str);
// }
//-----------------------------------------------------------------------
void PGLogDataManager::LoginLog(UserPtr & pUser, const uint32 serverId)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || pUser.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pUser);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Login;
	pLog->sub_type = en_LLT_Login;
	pLog->src_id = pUser->getSerial();
	pLog->src_type = ResourceTypeUser;
	// 服务器id
	pLog->data1 = serverId;
	if (!pUser->m_Socket.isNull())
	{
		WSSocketPtr socket = (WSSocketPtr)pUser->m_Socket.getResourcePtr();
		if (socket.isNull())
		{
			sChannelMgr.freeDumyLog(pLog);
			return;
		}
		// 服务器Ip
		pLog->sdata1 = socket->GetIP();
	}
	pLog->data2 = pUser->getUInt32Field("status");	// 登录方式
	String str = "用户";
	str += StringConverter::toString(pLog->src_id);
	if (pLog->data2 == 2)
		str += "从PC端";
	else
		str += "从手机端";
	str += "登录ID为";
	str += StringConverter::toString(pLog->data1);
	str += "的世界服务器, ip是";
	str += pLog->sdata1;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
	
	// 新版日志记录
	sCGLogMgr.LoginOrLogoutGameLog(pUser, en_LLST_Login, GetUTF8String(str));
}
//-----------------------------------------------------------------------
void PGLogDataManager::LogoutLog(UserPtr & pUser, const uint32 serverId)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || 
		pUser.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pUser);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Login;
	pLog->sub_type = en_LLT_Logout;
	pLog->src_id = pUser->getSerial();
	pLog->src_type = ResourceTypeUser;
	// 服务器id
	pLog->data1 = serverId;
	pLog->data2 = pUser->getUInt32Field("status");	// 登录方式
	pLog->data3 = (time(0) - pUser->getUInt32Field("ll_time")) / 60; // 本次在线时长
	String str = "用户";
	str += StringConverter::toString(pLog->src_id);
	if (pLog->data2 == 2)
		str += "从PC端";
	else
		str += "从手机端";
	str += "退出ID为";
	str += StringConverter::toString(pLog->data1);
	str += "的世界服务器，本次在线时间";
	str += StringConverter::toString(pLog->data3);
	str += "分钟";
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
	
	// 新版日志记录
	sCGLogMgr.LoginOrLogoutGameLog(pUser, en_LLST_Logout, GetUTF8String(str));
}
//-----------------------------------------------------------------------
void PGLogDataManager::AboutTableLog(CharPtr & pChr, const uint32 subType, ChannelPtr &pChn)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || pChr.isNull() || pChn.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Table;
	pLog->sub_type = subType;
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 房间号
	pLog->target_id = pChn->getUInt32Field("town_id");
	// 牌桌号
	pLog->target_type = pChn->getHandle();
	// 轮次号
	pLog->data1 = pChn->getUInt32Field("mapid");
	String str = "用户 ";
	str += StringConverter::toString(pLog->src_id);
	switch (subType)
	{
		case en_TLT_Enter:
			str += " 进入";
			break;
		case en_TLT_Offline:
			str += " 中途离开";
			break;
		case en_TLT_Return:
			str += " 重新回到";
			break;
		case en_TLT_Leave:
			str += " 离开";
			break;
		default:
			break;
	}
	char buf[256];
	sprintf(buf, "房间【%u】牌桌【%u】第【%u】轮", pLog->target_id, pLog->target_type, pLog->data1);
	str += buf;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
	
	// 追加新版日志逻辑
	UserPtr pUser = sUserMgr.load(pChr->getUInt32Field("userid"));
	sCGLogMgr.AboutTableLog(pUser, pChn->getHandle(), enTableLogSubType(subType), GetUTF8String(str));
}
//-----------------------------------------------------------------------
void PGLogDataManager::TableTurnOverLog(ChannelPtr &pChn, const uint32 resultType, const uint32 bankerUserId, const uint32 chips, const uint32 pump)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Table;
	pLog->sub_type = en_TLT_TurnOver;
	// 房间号
	pLog->target_id = pChn->getUInt32Field("town_id");
	// 牌桌号
	pLog->target_type = pChn->getHandle();
	// 轮次号
	pLog->data1 = pChn->getUInt32Field("mapid");
	// 结果类型
	pLog->data2 = resultType;
	// 庄家ID
	pLog->data3 = bankerUserId;
	// 押注
	pLog->data4 = chips;
	// 抽水
	pLog->data5 = pump;
	String type;
	switch (resultType)
	{
		case 1:
			type = "普通";
			break;
		case 2:
			type = "通杀";
			break;
		case 3:
			type = "通赔";
			break;
		case 4:
			type = "和局";
		default:
			break;
	}
	char buf[256];
	sprintf(buf, "在房间【%u】牌桌【%u】第【%u】轮中，庄家【%u】，结果类型【%s】，总下注【%u】，抽水【%u】", pLog->target_id, pLog->target_type, 
			pLog->data1, bankerUserId, type.c_str(), chips, pump);
	String str(buf);
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::UpdateItemsInfoLog(CharPtr & pChr, const uint32 sourceType, const uint32 selfModelId, const UpdateItemNumberType operate, const uint32 number, const uint32 fromId, const uint32 data7, const uint32 data8)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || 
		pChr.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Item;
	// 物品所属用户ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 物品ID
	pLog->dest_id = selfModelId;
	// 来源对象ID
	pLog->target_id = fromId;
	// 来源类型（来自哪方面）
	pLog->data1 = sourceType;
	// 增减（1：增 2：减 3：删）
	pLog->data2 = operate;
	if (operate != en_UNT_Delete)
	{
		// 增减多少
		pLog->data3 = number;
		// 当前多少
		pLog->data4 = sPubLogic.GetItemNum(pChr, selfModelId);
	}
	pLog->data7 = data7;
	pLog->data8 = data8;
	String str = "物品数量更新：用户 ";
	str += StringConverter::toString(pLog->src_id);
	switch (sourceType)
	{
		case en_ST_Item:
			str += " 使用模式ID为 ";
			str += StringConverter::toString(fromId);
			str += " 的物品，";
			break;
		case en_ST_GiftBag:
			str += " 获得";
			switch (fromId)
			{
				case en_GiftType_Login:
					str += "登录奖励，";
					break;
				case en_GiftType_Subsidy:
					str += "补给包，";
					break;
				case en_GiftType_Online:
					str += "在线礼包，";
					break;
				case en_GiftType_NewDiamond:
					str += "黄钻新手礼包，";
					break;
				case en_GiftType_LoginDiamond:
					str += "黄钻每日礼包，";
					break;
				case en_GiftType_LevelUp:
					str += "黄钻用户升级奖励，";
					break;
				case en_GiftType_UserBuy:
					str += "充值赠送礼包，";
					break;
				case en_GiftType_Welfare:
					str += "福利卡礼包，";
					break;
				case en_GiftType_Compensate:
					str += "系统补偿，";
					break;
				default:
					break;
			}
			break;
		case en_ST_Bugle:
			str += " 通过购买聊天喇叭【";
			if (en_ChatType_Server == fromId)
				str += "全服通告";
			else if (en_ChatType_Room == fromId)
				str += "房间通告";
			str += "】，";
			break;
		case en_ST_Present:
			str += " 通过购买礼物【";
			switch (fromId)
			{
				case en_Present_Flower:
					str += "鲜花";
					break;
				case en_Present_Tomato:
					str += "番茄";
					break;
				case en_Present_Egg:
					str += "鸡蛋";
					break;
				default:
					break;
			}
			str += "】，";
			break;
		case en_ST_Effect:
			str += " 通过完成模式ID为 ";
			str += StringConverter::toString(fromId);
			str += " 的效果，";
			break;
		case en_ST_Exchange:
			str += " 通过兑换ID为 ";
			str += StringConverter::toString(fromId);
			str += " 的兑换卡，";
			break;
		case en_ST_Gateway:
			str += " 通过网关操作，";
			break;
		case en_ST_Morra:
			str += " 通过猜拳，";
			break;
		case en_ST_Roulette:
			str += " 通过猜轮盘抽奖，";
			break;
		case en_ST_CombItem:
			str += " 合并物品数量，";
			break;
		case en_ST_SMGame:
			str += " 通过玩小游戏，";
			break;
		case en_ST_HappyFarm:
			str += " 通过开心牧场，";
			break;
		default:
			str += " 通过未知方式，";
			break;
	}
	if (operate == en_UNT_Delete)
	{
		str += "删除了模式ID为 ";
		str += StringConverter::toString(selfModelId);
		str += " 的物品";
	}
	else
	{
		str += (operate == en_UNT_Add ? "增加了 " : "减少了 ");
		str += StringConverter::toString(number);
		str += " 模式ID为 ";
		str += StringConverter::toString(selfModelId);
		str += " 的物品，当前数量为 ";
		str += StringConverter::toString(pLog->data4);
	}
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
	
	UserPtr pUser = sUserMgr.load(pChr->getUInt32Field("userid"));
	
	// 新版日志记录
	stItemsChangeLog parma;
	parma.pSrcUser = pUser;												// 用户
	parma.sub_type = uint8(sourceType);									// 子类型
	parma.change_model_id = selfModelId;								// 改变的道具模式ID
	parma.change_num = operate == en_UNT_Add ? number : number * (-1);	// 改变数量
	// parma.org_num;													// 改变前数量
	parma.end_num = sGLMgr.GetItemNum(pChr, selfModelId);				// 改变后数量
	parma.congeal_num = sGLMgr.getCogealItemNum(pChr, selfModelId);		// 冻结数量
	
	sCGLogMgr.ItemsChangeLogWithSystem(parma);
}
//-----------------------------------------------------------------------
void PGLogDataManager::UpdateItemsByGameResultLog(CharPtr & pChr, const uint32 selfModelId, const UpdateItemNumberType operate, 
												const uint32 number, ChannelPtr &pChn, const UpdateOnTableType chkType)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || pChr.isNull() || pChn.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Item;
	// 物品所属用户ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 物品ID
	pLog->dest_id = selfModelId;
	// 轮次号
	pLog->dest_type = pChn->getUInt32Field("mapid");
	// 来源对象ID(房间号)
	pLog->target_id = pChn->getUInt32Field("town_id");
	// 牌桌号
	pLog->target_type = pChn->getHandle();
	// 来源类型（来自牌桌）
	pLog->data1 = en_ST_Table;
	// 增减（1：增 2：减 3：删）
	pLog->data2 = operate;
	// 增减多少
	pLog->data3 = number;
	// 当前多少
	pLog->data4 = sPubLogic.GetItemNum(pChr, selfModelId);
	// 牌桌哪方面
	pLog->data5 = chkType;
	
	String str = "物品数量更新：用户 ";
	str += StringConverter::toString(pLog->src_id);
	str += " 通过在房间ID号为 ";
	str += StringConverter::toString(pLog->target_id);
	str += " 中ID号是 ";
	str += StringConverter::toString(pLog->target_type);
	str += " 的牌桌玩第 ";
	str += StringConverter::toString(pLog->dest_type);
	str += " 轮游戏，";
	if (1 == chkType)
		str += "统计牌局";
	else
		str += "因为扣除对局费";
	str += (operate == en_UNT_Add ? "增加了 " : "减少了 ");
	str += StringConverter::toString(number);
	str += " 模式ID为 ";
	str += StringConverter::toString(selfModelId);
	str += " 的物品，当前数量为 ";
	str += StringConverter::toString(pLog->data4);
	
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::UpdateItemsByUserBuyLog(CharPtr & pChr, const uint32 selfModelId, const uint32 number)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || 
		pChr.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Item;
	// 物品所属用户ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 物品ID
	pLog->dest_id = selfModelId;
	// 来源类型（来自哪方面）
	pLog->data1 = en_ST_Gateway_Buy;
	// 增加
	pLog->data2 = en_UNT_Add;
	// 增减多少
	pLog->data3 = number;
	// 当前多少
	pLog->data4 = sPubLogic.GetItemNum(pChr, selfModelId);
	String str = "物品数量更新：用户 ";
	str += StringConverter::toString(pLog->src_id);
	str += " 通过充值物品，增加了 ";
	str += StringConverter::toString(number);
	str += " 模式ID为 ";
	str += StringConverter::toString(selfModelId);
	str += " 的物品，当前数量为 ";
	str += StringConverter::toString(pLog->data4);
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::GatwayBuyResultLog(CharPtr & pChr, String &goodsDesc, const uint32 cost)
{
	if (pChr.isNull())
		return;
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_GatwayBuyResult;
	
	// 用户ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 支出金额
	pLog->data1 = cost;
	String str = "用户 ";
	str += StringConverter::toString(pLog->src_id);
	str += "花费 ";
	str += StringConverter::toString(cost);
	str += " 的金钱成功购买了以下物品：";
	str += goodsDesc;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::AboutBankLog(CharPtr & pChr, const BankType subType, const uint32 modelId, const uint32 number)
{
	if (pChr.isNull())
		return;
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Bank;
	pLog->sub_type = subType;
	// 用户ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = ResourceTypeUser;
	// 模式ID
	pLog->data1 = modelId;
	// 金额
	pLog->data2 = number;
	// 余额
	pLog->data3 = sPubLogic.GetItemNum(pChr, modelId);
	String str = "用户 ";
	str += StringConverter::toString(pLog->src_id);
	char msg[256];
	sprintf(msg, "%s了 %u ID为 %u 的物品，当前余额 %u", subType == en_BankType_Save ? "存入" : "取出", number, modelId, pLog->data3);
	str += msg;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::PumpLog(const uint32 sourceType, const uint32 pump)
{
	if (0 == pump)
		return;
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Pump;
	// 来源类型（来自哪方面）
	pLog->data1 = sourceType;
	// 抽水
	pLog->data2 = pump;
	String pFieldName;
	switch (sourceType)
	{
		case en_ST_Table:
			pFieldName = "比大小牌桌";
			break;
		case en_ST_Bank:
			pFieldName = "银行";
			break;
		case en_ST_Morra:
			pFieldName = "猜拳";
			break;
		default:
			sChannelMgr.freeDumyLog(pLog);
			return;
	}
	char msg[256];
	sprintf(msg, "系统从【%s】抽税【%u】", pFieldName.c_str(), pump);
	String str = msg;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::SMGameMorraLog(const uint32 serial1, const uint32 serial2, const uint32 val1, const uint32 val2, const uint32 ante, const uint32 winner)
{
	if(!serial1 || !serial2)
		return;
	GameLog *pLog = sChannelMgr.newDumyLog();
	if(NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	pLog->status = 1;
	pLog->type = en_GLT_Morra;
	pLog->src_id= serial1;
	pLog->dest_id = serial2;
	pLog->data1= val1;
	pLog->data2= val2;
	pLog->data3= ante;
	pLog->data5= winner;
	
	char pstr[512] = "";
	sprintf(pstr, "用户[%u]出了[%u]用户[%u]出了[%u]，结果[%u]，赌注[%u]！", pLog->src_id, pLog->data1, pLog->dest_id, pLog->data2, pLog->data5, pLog->data3);
	String str = pstr;
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::AddGamePumpLog(CharPtr & pChr,
							const uint32 & fromId, 
							const uint32 & num, 
							const uint32 & data1 /* = 0 */, 
							const uint32 & data2 /* = 0 */)
{
	if(pChr.isNull() || !num)
		return ;
	
	GameLog *pLog = sChannelMgr.newDumyLog();
	if(NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	pLog->status = 1;
	pLog->type = en_GLT_Pump;
	pLog->src_id= pChr->getUInt32Field("userid");
	pLog->data1	= fromId;
	pLog->data2 = num;
	pLog->data3 = data1;
	pLog->data4 = data2;
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::ChangeFiguralLog(CharPtr & pChr, const SourceType sourceType, const uint32 figuralId, const uint32 fromId)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog || 
		pChr.isNull())
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Figural;
	// 玩家ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = RTT_USER;
	// 来源对象ID
	pLog->target_id = fromId;
	// 来源类型（来自哪方面）
	pLog->data1 = sourceType;
	// 人物形象ID
	pLog->data2 = figuralId;
	String str = "玩家 ";
	str += StringConverter::toString(pLog->src_id);
	str += " 的人物形象改变：";
	switch (sourceType)
	{
		case en_ST_Trade:
			str += " 在";
			if (fromId == en_ShopT_GoldShop)
				str += "元宝商城";
			else
				str += "兑换券商城";
			str += "消费，";
			break;
		default:
			str += "通过未知原因，";
			break;
	}
	str += "改变人物形象为 ";
	str += StringConverter::toString(figuralId);
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::ChangePracticalityLog(const uint32 userId, const uint32 goodsId, const String &name, const uint32 number, const uint32 cost, const char *billId)
{
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Practicality;
	// 用户ID
	pLog->src_id = userId;
	pLog->src_type = RTT_USER;
	// 兑换实物ID
	pLog->data1 = goodsId;
	// 数量
	pLog->data2 = number;
	// 花费兑换券
	pLog->data3 = cost;
	// 订单号
	pLog->sdata1 = billId;
	// 商品名称
	pLog->sdata2 = name;
	char buf[128];
	sprintf(buf, "用户 %d 在兑换券商城花费 %d 兑换券兑换了 %d 数量ID为 %d 的实物商品，订单号是 %s。", userId, cost, number, goodsId, billId);
	String str(buf);
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}
//-----------------------------------------------------------------------
void PGLogDataManager::UpdateGoldsLog(CharPtr & pChr, const SourceType sourceType, const UpdateItemNumberType operate, const uint32 number, const uint32 fromId)
{
	// 元宝没有删除的说法
	if (pChr.isNull() || operate == en_UNT_Delete)
		return;
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	AppendUserLog(pLog, pChr);
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_Gold;
	// 账号ID
	pLog->src_id = pChr->getUInt32Field("userid");
	pLog->src_type = RTT_USER;
	// 来源对象ID
	pLog->target_id = fromId;
	// 来源类型（来自哪方面）
	pLog->data1 = sourceType;
	// 增减（1：增 2：减 3：删）
	pLog->data2 = operate;
	// 增减多少
	pLog->data3 = number;
	// 当前多少
	pLog->data4 = sPubLogic.GetGoldMoney(pChr);
	String str = "元宝数量更新: 玩家 ";
	str += StringConverter::toString(pChr->getSerial());
	str += " (即用户 ";
	str += StringConverter::toString(pLog->src_id);
	str += " )";
	switch (sourceType)
	{
		case en_ST_Trade:
			str += "在商城消费，";
			break;
		default:
			str += "通过未知原因，";
			break;
	}
	str += (operate == en_UNT_Add ? "增加了 " : "减少了 ");
	str += StringConverter::toString(number);
	str += " 元宝，当前数量为 ";
	str += StringConverter::toString(pLog->data4);
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}

void PGLogDataManager::AreaWinloseLog(ChannelPtr &pChn, std::vector<TablePlayer> &vecPos)
{
	if(pChn.isNull() || vecPos.size() != 4)
		return ;
	
	GameLog *pLog = sChannelMgr.newDumyLog();
	if (NULL == pLog)
	{
		sChannelMgr.freeDumyLog(pLog);
		return;
	}
	
	// 填充日志信息
	pLog->status = 1;
	pLog->type = en_GLT_UserDefined;
	// 账号ID
	pLog->src_id = pChn->getHandle();
	pLog->src_type = RTT_CHANNEL;
	// 来源类型（来自哪方面）
	pLog->data1 = vecPos[0].isWin;
	pLog->data2 = vecPos[1].isWin;
	pLog->data3 = vecPos[2].isWin;
	pLog->data4 = vecPos[3].isWin;
	
	String str = "牌局结果：庄（";
	str += vecPos[0].isWin ? "赢）" : "输）";
	str += "顺（";
	str += vecPos[1].isWin ? "赢）" : "输）";
	str += "天（";
	str += vecPos[2].isWin ? "赢）" : "输）";
	str += "地（";
	str += vecPos[3].isWin ? "赢）" : "输）";
	
	pLog->info = GetUTF8String(str);
	pLog->create_time = sTools.GetCurDateTime();
	sChannelMgr.addLogDB(pLog);
	sChannelMgr.freeDumyLog(pLog);
}

String PGLogDataManager::GetLogCause(const uint32 &source)
{
	String szString  = "";
	switch (source)
	{
		// case en_ST_Game:
			// szString = "来自游戏结果";
		// case en_ST_Item:
			// szString =  ("来自物品");
		// case en_ST_Hortation:
			// szString = ("来自奖励");
		// case en_ST_Trade:
			// szString = ("来自交易");
		// case en_ST_Task:
			// szString = ("来自任务");
		// case en_ST_Effect:
			// szString = ("来自效果");
		// case en_ST_Gateway:
			// szString = ("来自网关");
		// case en_ST_Exchange:
			// szString = ("来自兑换卡");
		// case en_ST_GiveBack:
			// szString = ("系统返还");
		case en_ST_Gateway_Buy:
			szString = ("充值物品");
		// case en_ST_SignupMatch:
			// szString = ("参加比赛");
		// case en_ST_Match:
			// szString = ("比赛奖励");
		// case en_ST_Operation:
			// szString = ("对象操作");
		case en_ST_SMGame:
			szString = ("小游戏");
		// case en_ST_Chat:
			// szString = ("聊天");
		// case en_ST_Morra:
			// szString = ("猜拳游戏");
		// case en_ST_Roulette:
			// szString = ("轮盘赌");
		// case en_ST_Bank:
			// szString = ("银行");
		// case en_ST_Fee:
			// szString = ("服务费");
		// case en_ST_PTHortation:
			// szString = ("平台额外奖励");
		// case en_ST_TTL:
			// szString = ("推推乐相关");
		// case en_ST_BuZhu:
			// szString = ("游戏补给");
		// case en_ST_SystemGet:
			// szString = ("系统没收");
		// case en_ST_CreateChar:
			// szString = ("来源于注册角色");
		// case en_ST_LevelUp:
			// szString = ("等级成长相关");
		// case en_ST_Congeal:
			// szString = ("冻结");
		// case en_ST_UnCongeal:
			// szString = ("解除冻结");
		// case en_ST_OnlineBonus:
			// szString = ("在线礼包");
		// case en_ST_WinCombo:
			// szString = ("累计获胜");
		// case en_ST_Effort:
			// szString = ("来自成就");
		default:
			break;
	}
	
	return GetUTF8String(szString);
}

// bool PGLogDataManager::AppendUserLog(GameLog *pLog, UserPtr &pUser)
// {
	// if (pLog == NULL || pUser.isNull())
		// return false;
	// pLog->src_id = pUser->getSerial();
	// pLog->src_type = pUser->getUInt32Field("active_gz_id");
	// pLog->platform_id = pUser->getUInt32Field("platform_id");
	// pLog->user_name = pUser->getStringField("name");
	// pLog->user_nick = pUser->getStringField("nick");
	// pLog->user_ip = pUser->getStringField("ip");
	// pLog->user_addr = pUser->getStringField("addr");
	// pLog->user_id = pUser->getHandle();
	// pLog->char_id = pUser->getUInt32Field("char_id");
	// pLog->flag = pUser->getUInt32Field("status");
	// return true;
// }

// bool PGLogDataManager::AppendUserLog(GameLog *pLog, CharPtr &pChr)
// {
	// if (pChr.isNull())
		// return false;
	// UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
	// if (pUser.isNull())
		// return false;
	// return AppendUserLog(pLog, pUser);
// }

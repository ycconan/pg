#include "Channels.h"
#include "ChannelManager.h"
#include "Resource.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "Character.h"
#include "CharManager.h"
#include "MersenneTwister.h"
#include "ScriptMgr.h"
#include "BSSocket.h"
#include "BSSocketManager.h"
#include "ChannelManager.h"
#include "PGWorldAIInterface.h"
#include "GameDefine.h"
#include XMLDATA_MGR_PATH

using namespace AIScript;
// int ServerCheckUserHook(uint32 serverid)
// {
// static uint32 hook_checktimer = time(0);
// 	uint32 curTime = time(0);
// 	if(curTime - hook_checktimer >= sXmlDataMgr.GetConfXMLValue("USER_CHECK_INTERVAL"))
// 	{
// 		std::list<ResourcePtr> lstChars;
// 		sCharMgr.getResourceList(&lstChars);
// 		std::list<ResourcePtr>::iterator iter;
// 		for(iter = lstChars.begin(); iter != lstChars.end(); ++iter)
// 		{
// 			CharPtr pChr = CharPtr(*iter);
// 			if(!pChr->isLoaded() || pChr->isNPC())
// 			{
// 				continue;
// 			}
// 			PGCreatureInterface * pAIChrInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface());
// 			if(!pAIChrInfe)
// 			{
// 				continue;
// 			}
// 			if(pAIChrInfe->GetIsCanDelete())
// 			{
// 				Log.Debug("ServerCheckUserHook","Char[%u] deactivate, will be deleting!!", pChr->getHandle());
// 				uint32 uId = pChr->getUInt32Field("userid");
// 				pChr->Delete();
// 				ResourcePtr pUser = sUserMgr.getByHandle(uId);
// 				if(!pUser.isNull())
// 					pUser->Delete();
// 				Log.Debug("ServerCheckUserHook","Char[%u] deactivate, delete this!!", pChr->getHandle());
// 			}
// 		}
// 		hook_checktimer = curTime;
// 	}
	
// 	return 1;
// }





#include "tinyxml/tinyxml.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "WSSocket.h"
#include "PGXmlDataManager.h"
#include "PGPubStruct.h"
#include "NoticeManager.h"
#include <fstream>
#include "GameDefine.h"
#include LOGIC_MGR_PATH
#include PROTOCOLS_MGR_PATH



using std::map;
using std::list;
//--------------------------------------------------------
PGXmlDataManager::PGXmlDataManager()
{
	LoadLevelExpList();
	// LoadGameConfig();
	// LoadSystemNotices();
	LoadKeyWords();
	LoadNickKeyWords();
	LoadPresents();
	LoadDiamondLogin();
	LoadGZIDName();
	LoadChips();
	// LoadGameIDXml();
	LoadNewPlayerGift();
	LoadOnlineBounty(); // 加载在线奖励配置
	
	// LoadRoulette();
	// LoadBuyLargessXml();
	// LoadGiveLeCardsXml();
}
//--------------------------------------------------------
PGXmlDataManager::~PGXmlDataManager()
{
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadLevelExpList()
{
	TiXmlDocument doc("../scripts_xml/pg/about_level.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::InitLevelExpList", "load about_level.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("PGXmlDataManager::InitLevelExpList", "about_level.xml have not root node!");
		return false;
	}
	if (String(pRoot->Value()).compare("levels"))
	{
		Log.Error("PGXmlDataManager::InitLevelExpList", "about_level.xml's root node is error!");
		return false;
	}
	TiXmlElement* pChild = pRoot->FirstChildElement();
	while(pChild != NULL)
	{
		AboutLevel al;
		al.lv = atoi(pChild->Attribute("lv"));
		al.exps = atoi(pChild->Attribute("exps"));
		al.loginHR = StringConverter::parseUnsignedInt(pChild->Attribute("login_ht")); 
		al.levelUpHR = StringConverter::parseUnsignedInt(pChild->Attribute("levelup_ht"));
		m_mapAboutLevel.insert(std::make_pair(al.lv, al));
		pChild = pChild->NextSiblingElement();
	}
	Log.Debug("PGXmlDataManager::InitLevelExpList:", "load about_level.xml successfully!");
	return true;
}
bool PGXmlDataManager::ReloadLevelExpList()
{
	WGS_XML_WRITE_LOCK
	m_mapAboutLevel.clear();
	LoadLevelExpList();
	return true;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetMaxLevel() 
{
	WGS_XML_READ_LOCK
	map<uint8, AboutLevel>::const_reverse_iterator iter = m_mapAboutLevel.rbegin();
	if (iter != m_mapAboutLevel.rend())
		return iter->first;
	return 0;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetExpsOfLevel(const uint32 level) 
{
	WGS_XML_READ_LOCK
	map<uint8, AboutLevel>::const_iterator iter = m_mapAboutLevel.find(level);
	if (iter != m_mapAboutLevel.end())
		return iter->second.exps;
	return 0;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetLevelUpGiftOfLevel(const uint32 level) 
{
	WGS_XML_READ_LOCK
	map<uint8, AboutLevel>::const_iterator iter = m_mapAboutLevel.find(level);
	if (iter != m_mapAboutLevel.end())
		return iter->second.levelUpHR;
	return 0;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetLoginGiftOfLevel(const uint32 level) 
{
	WGS_XML_READ_LOCK
	map<uint8, AboutLevel>::const_iterator iter = m_mapAboutLevel.find(level);
	if (iter != m_mapAboutLevel.end())
		return iter->second.loginHR;
	return 0;
}
//--------------------------------------------------------
// bool PGXmlDataManager::LoadGameConfig(void)
// {
	// TiXmlDocument doc("../scripts_xml/pg/game_conf.xml");
	// if(!doc.LoadFile())
	// {
		// Log.Error("PGXmlDataManager::LoadGameConfig", "load game_conf.xml failed!");
		// return false;
	// }
	// TiXmlElement* pRoot = doc.RootElement();
	// if (NULL == pRoot)
	// {
		// Log.Error("PGXmlDataManager::LoadGameConfig", "game_conf.xml have not root node!");
		// return false;
	// }
	
	// Log.Debug("PGXmlDataManager::LoadGameConfig", "Start load ../scripts_xml/pg/game_conf.xml");
	// TiXmlElement* child = pRoot->FirstChildElement();
	// while(child)
	// {
		// String szName = child->Attribute("name");
		// uint32 val = StringConverter::parseUnsignedInt(child->Attribute("value"));
		// m_mapGameConfig.insert(std::make_pair(szName, val));
		// child = child->NextSiblingElement();
	// }
	// Log.Debug("PGXmlDataManager::LoadGameConfig:", "load game_conf.xml successfully!");
	// return true;
// }
//--------------------------------------------------------
// bool PGXmlDataManager::ReloadGameConfig(void)
// {
	// WGS_XML_WRITE_LOCK
	// m_mapGameConfig.clear();
	// LoadGameConfig();
	// return true;
// }
// //--------------------------------------------------------
// uint32 PGXmlDataManager::GetConfXMLValue(const String & str)
// {
	// WGS_XML_READ_LOCK
	// std::map<String, uint32>::iterator re = m_mapGameConfig.find(str);
	// if(re != m_mapGameConfig.end())
		// return m_mapGameConfig[str];
	// return 0;
// }
//--------------------------------------------------------
// bool PGXmlDataManager::LoadSystemNotices()
// {
	// TiXmlDocument doc("../scripts_xml/system_notice.xml");
	// if(!doc.LoadFile())
	// {
		// Log.Error("PGXmlDataManager::LoadSystemNotices:", "load system_notice.xml failed!");
		// return false;
	// }
	// TiXmlElement* pRoot = doc.RootElement();
	// m_lstNotices.clear();
	// Notice nt;
	// while (NULL != pRoot)
	// {
		// if (StringConverter::parseUnsignedInt(pRoot->Attribute("status")) > 0)
		// {
			// nt.msg = pRoot->Attribute("msg");
			// nt.interval = StringConverter::parseUnsignedInt(pRoot->Attribute("interval"));
			// nt.msgType = StringConverter::parseUnsignedInt(pRoot->Attribute("msgType"));
			// nt.gzId = StringConverter::parseUnsignedInt(pRoot->Attribute("gzId"));
			// nt.startDate = StringConverter::parseUnsignedInt(pRoot->Attribute("start"));
			// nt.finishDate = StringConverter::parseUnsignedInt(pRoot->Attribute("finish"));
			// nt.beginTime = StringConverter::parseUnsignedInt(pRoot->Attribute("begin"));
			// nt.endTime = StringConverter::parseUnsignedInt(pRoot->Attribute("end"));
			// m_lstNotices.push_back(nt);
		// }
		// pRoot = pRoot->NextSiblingElement();
	// }
	// Log.Debug("PGXmlDataManager::LoadSystemNotices:", "load system_notice.xml successfully!");
	// return true;
// }
// //--------------------------------------------------------
// bool PGXmlDataManager::ReloadSystemNotices()
// {
	// WGS_XML_WRITE_LOCK
	// m_lstNotices.clear();
	// LoadSystemNotices();
	// return true;
// }
//--------------------------------------------------------
bool PGXmlDataManager::LoadKeyWords()
{
	std::fstream fs;
	fs.open("../scripts_xml/deny_words.txt", std::ios::in);
	if (fs.bad())
	{
		Log.Debug("PGXmlDataManager::LoadKeyWords:", "load deny_words.txt failed!");
		return false;
	}
	while (!fs.eof())
	{
		String str;
		fs >> str;
		// 排除空行
		if (str.compare(""))
		{
			String strFilter("");
			for (size_t i = 0; i < str.length(); )
			{
				if (StringUtil::checkStringUTF8(str.substr(i, 3)))
					i += 3;
				else
					++i;
				strFilter += '*';
			}
			m_mapKeyWordsFilter.insert(std::make_pair(str, strFilter));
		}
	}
	fs.close();
	Log.Debug("PGXmlDataManager::LoadKeyWords:", "load deny_words.txt successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadKeyWords()
{
	WGS_XML_WRITE_LOCK
	m_mapKeyWordsFilter.clear();
	LoadKeyWords();
	return true;
}
//--------------------------------------------------------
void PGXmlDataManager::FilterKeyWords(String &strContent)
{
	WGS_XML_READ_LOCK
	if (!StringUtil::checkStringUTF8(strContent))
		strContent = StringUtil::StringToUTF8(strContent);
	std::map<String, String>::iterator iter = m_mapKeyWordsFilter.begin();
	for (; iter != m_mapKeyWordsFilter.end(); ++iter)
	{
		String::size_type pos;
		while (String::npos != (pos = strContent.find(iter->first.c_str(), 0)))
		{
			strContent.replace(pos, iter->first.length(), iter->second);
		}
	}
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadNickKeyWords()
{
	std::fstream fs;
	fs.open("../scripts_xml/pg/nick_deny_words.txt", std::ios::in);
	if (fs.bad())
	{
		Log.Debug("PGXmlDataManager::LoadKeyWords:", "load nick_deny_words.txt failed!");
		return false;
	}
	while (!fs.eof())
	{
		String str;
		fs >> str;
		// 排除空行
		if (str.compare(""))
		{
			String strFilter("");
			for (size_t i = 0; i < str.length(); )
			{
				if (StringUtil::checkStringUTF8(str.substr(i, 3)))
					i += 3;
				else
					++i;
				strFilter += '*';
			}
			m_mapNickKeyWordsFilter.insert(std::make_pair(str, strFilter));
		}
	}
	fs.close();
	Log.Debug("PGXmlDataManager::LoadKeyWords:", "load nick_deny_words.txt successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadNickKeyWords()
{
	WGS_XML_WRITE_LOCK
	m_mapNickKeyWordsFilter.clear();
	LoadKeyWords();
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::IsExistedKeyWords(String &strContent)
{
	WGS_XML_READ_LOCK
	if (!StringUtil::checkStringUTF8(strContent))
		strContent = StringUtil::StringToUTF8(strContent);
	std::map<String, String>::iterator iter = m_mapNickKeyWordsFilter.begin();
	for (; iter != m_mapNickKeyWordsFilter.end(); ++iter)
	{
		String::size_type pos;
		if (String::npos != (pos = strContent.find(iter->first.c_str(), 0)))
		{
			return true;
		}
	}
	return false;
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadPresents()
{
	TiXmlDocument doc("../scripts_xml/pg/present.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::LoadPresents", "load present.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("PGXmlDataManager::LoadPresents", "present.xml have not root node!");
		return false;
	}
	TiXmlElement* child = pRoot->FirstChildElement();
	Present p;
	while(child)
	{
		p.id = StringConverter::parseUnsignedInt(child->Attribute("id"));
		p.modelId = StringConverter::parseUnsignedInt(child->Attribute("model"));
		p.price = StringConverter::parseUnsignedInt(child->Attribute("price"));
		m_mapPresents.insert(std::make_pair(p.id, p));
		child = child->NextSiblingElement();
	}
	Log.Debug("PGXmlDataManager::LoadPresents:", "load present.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadPresents()
{
	WGS_XML_WRITE_LOCK
	m_mapPresents.clear();
	LoadPresents();
	return true;
}
//--------------------------------------------------------
Present PGXmlDataManager::GetPresentById(const uint32 id)
{
	WGS_XML_READ_LOCK
	std::map<uint32, Present>::iterator iter = m_mapPresents.find(id);
	if (iter != m_mapPresents.end())
		return iter->second;
	return Present();
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadDiamondLogin()
{
	TiXmlDocument doc("../scripts_xml/pg/diamond_login.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::LoadDiamondLogin", "load diamond_login.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("PGXmlDataManager::LoadDiamondLogin", "diamond_login.xml have not root node!");
		return false;
	}
	TiXmlElement* child = pRoot->FirstChildElement();
	DiamondLgBounty dlb;
	while(child)
	{
		dlb.level = StringConverter::parseUnsignedInt(child->Attribute("lv"));
		dlb.coins = StringConverter::parseUnsignedInt(child->Attribute("coins"));
		m_mapDiamonds.insert(std::make_pair(dlb.level, dlb));
		child = child->NextSiblingElement();
	}
	Log.Debug("PGXmlDataManager::LoadDiamondLogin:", "load diamond_login.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadDiamondLogin()
{
	WGS_XML_WRITE_LOCK
	m_mapDiamonds.clear();
	LoadDiamondLogin();
	return true;
}
//--------------------------------------------------------
uint32 PGXmlDataManager::GetDiamondLevelBounty(const uint32 lv)
{
	WGS_XML_READ_LOCK
	std::map<uint32, DiamondLgBounty>::iterator iter = m_mapDiamonds.find(lv);
	if (iter != m_mapDiamonds.end())
		return iter->second.coins;
	return 0;
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadGZIDName()
{
	TiXmlDocument doc("../scripts_xml/gz_id.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::LoadGZIDName", "load gz_id.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("PGXmlDataManager::LoadGZIDName", "gz_id.xml have not root node!");
		return false;
	}
	while(pRoot)
	{
		m_mapGZIDName.insert(std::make_pair(StringConverter::parseUnsignedInt(pRoot->Attribute("id")), pRoot->Attribute("name")));
		pRoot = pRoot->NextSiblingElement();
	}
	Log.Debug("PGXmlDataManager::LoadGZIDName:", "load gz_id.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadGZIDName()
{
	WGS_XML_WRITE_LOCK
	m_mapGZIDName.clear();
	LoadGZIDName();
	return true;
}
//--------------------------------------------------------
String PGXmlDataManager::GetGzIdName(const uint32 id)
{
	WGS_XML_READ_LOCK
	std::map<uint32, String>::iterator iter = m_mapGZIDName.find(id);
	if (iter != m_mapGZIDName.end())
		return iter->second;
	return String("");
}
//--------------------------------------------------------
// bool PGXmlDataManager::LoadRoulette(void)
// {
	// TiXmlDocument doc("../scripts_xml/pg/roulette.xml");
	// if(!doc.LoadFile())
	// {
		// Log.Error("PGXmlDataManager::LoadRoulette:", "load roulette.xml failed!");
		// return false;
	// }
	
	// TiXmlElement* pRoot = doc.RootElement();
	// if (NULL != pRoot)
	// {
		// m_tpConf.type = StringConverter::parseUnsignedInt(pRoot->Attribute("type"));
		// m_tpConf.torc = StringConverter::parseUnsignedInt(pRoot->Attribute("torc"));
		// m_tpConf.maxNum = StringConverter::parseUnsignedInt(pRoot->Attribute("max_n"));
		// m_tpConf.baseScore = StringConverter::parseUnsignedInt(pRoot->Attribute("base_scale"));
		// m_tpConf.beginTime = StringConverter::parseUnsignedInt(pRoot->Attribute("btime"));
		// m_tpConf.endTime = StringConverter::parseUnsignedInt(pRoot->Attribute("etime"));
		// pRoot = pRoot->NextSiblingElement();
	// }
	// m_listRoulettes.clear();
	// uint8 index = 0;
	// while (NULL != pRoot)
	// {
		// Roulette rt;
		// rt.id = ++index;
		// rt.chance = StringConverter::parseUnsignedInt(pRoot->Attribute("chance"));
		// rt.flag = StringConverter::parseUnsignedInt(pRoot->Attribute("flag"));
		// rt.model_id = StringConverter::parseUnsignedInt(pRoot->Attribute("model_id"));
		// rt.num = StringConverter::parseUnsignedInt(pRoot->Attribute("number"));
		// rt.desc = pRoot->Attribute("desc");
		// rt.showNum = StringConverter::parseUnsignedInt(pRoot->Attribute("show_num"));
		// m_listRoulettes.push_back(rt);
		// pRoot = pRoot->NextSiblingElement();		
	// }
	
	// return true;
// }
// //--------------------------------------------------------
// bool PGXmlDataManager::ReloadRoulette(void)
// {
	// WGS_XML_WRITE_LOCK
	// LoadRoulette();
	// return true;
// }
// //--------------------------------------------------------
// const Roulette * PGXmlDataManager::GetRoulette(void)
// {
	// WGS_XML_READ_LOCK
	// uint32 rand = RandomUInt(1, m_tpConf.baseScore);
	// uint32 chance = 0;
	// std::list<Roulette>::iterator ri, endri = m_listRoulettes.end();
	// for(ri = m_listRoulettes.begin(); ri != endri; ++ ri)
	// {
		// chance += (*ri).chance;
		// if(rand <= chance)
		// {
			// return &(*ri);
		// }
	// }
	
	// return NULL;
// }
// //--------------------------------------------------------
// bool PGXmlDataManager::GetRoulette(const uint32 id, Roulette &roulette)
// {
	// WGS_XML_READ_LOCK
	// std::list<Roulette>::iterator ri, endri = m_listRoulettes.end();
	// for(ri = m_listRoulettes.begin(); ri != endri; ++ ri)
	// {
		// if((*ri).id == id)
		// {
			// roulette = (*ri);
			// return true;
		// }
	// }
	
	// return false;
// }
// //--------------------------------------------------------
// TurnplateConf PGXmlDataManager::GetTPConf()
// {
	// WGS_XML_READ_LOCK
	// return m_tpConf;
// }
//--------------------------------------------------------
bool PGXmlDataManager::LoadChips(void)
{
	TiXmlDocument doc("../scripts_xml/pg/chips.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::LoadChips:", "load chips.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	while (NULL != pRoot)
	{
		Chip chip;
		chip.value = StringConverter::parseUnsignedInt(pRoot->Attribute("value"));
		chip.min = StringConverter::parseUnsignedInt(pRoot->Attribute("min"));
		chip.max = StringConverter::parseInt(pRoot->Attribute("max"));
		m_mapChips.insert(std::make_pair(chip.value, chip));
		pRoot = pRoot->NextSiblingElement();		
	}
	Log.Debug("PGXmlDataManager::LoadChips:", "load chips.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadChips(void)
{
	WGS_XML_WRITE_LOCK
	m_mapChips.clear();
	LoadChips();
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::CheckChips(const uint32 chip, const uint32 wealth)
{
	WGS_XML_READ_LOCK
	std::map<uint32, Chip>::iterator iter = m_mapChips.find(chip);
	if (iter != m_mapChips.end())
	{
		if (wealth >= iter->second.min && wealth < iter->second.max)
			return true;
	}
	return false;
}
//--------------------------------------------------------
uint32 PGXmlDataManager::RandomGetChip()
{
	WGS_XML_READ_LOCK
	if (m_mapChips.size() > 0)
	{
		uint32 rand = RandomUInt(m_mapChips.size());
		uint32 index = 0;
		std::map<uint32, Chip>::iterator iter = m_mapChips.begin();
		for (; iter != m_mapChips.end(); ++iter)
		{
			if (index++ == rand)
			{
				return iter->first;
			}
		}
	}
	return 0;
}
//--------------------------------------------------------
// bool PGXmlDataManager::LoadGameIDXml()
// {
	// TiXmlDocument doc("../scripts_xml/pg/game_ids.xml");
	// if(!doc.LoadFile())
	// {
		// Log.Error("load XML Error", "load game_ids.xml failed!");
		// return false;
	// }
	// TiXmlElement* pRoot = doc.RootElement();
	// if (NULL == pRoot)
	// {
		// Log.Error("load Xml Error", "game_ids.xml have not root node!");
		// return false;
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/game_ids.xml");
	// m_mapGzidInfo.clear();
	
	// while(pRoot)
	// {
		// GZIDInfo gz;
		// String pName = pRoot->Attribute("name");
		// gz.gzId = StringConverter::parseUnsignedInt(pRoot->Attribute("val"));
		// gz.minVersion = pRoot->Attribute("v_min");
		// gz.maxVersion = pRoot->Attribute("v_max");
		// gz.maxAddr = pRoot->Attribute("v_addr");
		// gz.maxSize = StringConverter::parseUnsignedInt(pRoot->Attribute("v_size"));
		// m_mapGzidInfo.insert(std::make_pair(pName, gz));
		// pRoot = pRoot->NextSiblingElement();
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/game_ids.xml successfully");
	// return true;
// }
//--------------------------------------------------------
// bool PGXmlDataManager::ReLoadGameIDXml()
// {
// WGS_XML_WRITE_LOCK
	// LoadGameIDXml();
	// return true;
// }
//--------------------------------------------------------
bool PGXmlDataManager::LoadNewPlayerGift()
{
	TiXmlDocument doc("../scripts_xml/pg/new_player_gift.xml");
	if(!doc.LoadFile())
	{
		Log.Error("PGXmlDataManager::LoadNewPlayerGift", "load new_player_gift.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("PGXmlDataManager::LoadNewPlayerGift", "new_player_gift.xml have not root node!");
		return false;
	}
	while(pRoot)
	{
		m_mapNewPlayerGift.insert(std::make_pair(StringConverter::parseUnsignedInt(pRoot->Attribute("id")), StringConverter::parseUnsignedInt(pRoot->Attribute("num"))));
		pRoot = pRoot->NextSiblingElement();
	}
	Log.Debug("PGXmlDataManager::LoadNewPlayerGift:", "load new_player_gift.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadNewPlayerGift()
{
	WGS_XML_WRITE_LOCK
	m_mapNewPlayerGift.clear();
	LoadNewPlayerGift();
	return true;
}
//--------------------------------------------------------
const std::map<uint32, uint32> & PGXmlDataManager::GetNewPlayerGiftList()
{
	WGS_XML_READ_LOCK
	return m_mapNewPlayerGift;
}
//--------------------------------------------------------
bool PGXmlDataManager::LoadOnlineBounty()
{
	TiXmlDocument doc("../scripts_xml/pg/online_gift.xml");
	if(!doc.LoadFile())
	{
		Log.Error("XZXmlDataManager::LoadOnlineBounty", "load online_gift.xml failed!");
		return false;
	}
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("XZXmlDataManager::LoadOnlineBounty", "online_gift.xml have not root node!");
		return false;
	}
	while(pRoot != NULL)
	{
		OnlineBounty ob;
		ob.id = StringConverter::parseUnsignedInt(pRoot->Attribute("id"));
		ob.second = StringConverter::parseUnsignedInt(pRoot->Attribute("second"));
		ob.bounty = StringConverter::parseUnsignedInt(pRoot->Attribute("bounty")); 
		m_mapOnlineBounty.insert(std::make_pair(ob.id, ob));
		pRoot = pRoot->NextSiblingElement();
	}
	Log.Debug("XZXmlDataManager::LoadOnlineBounty:", "load online_gift.xml successfully!");
	return true;
}
//--------------------------------------------------------
bool PGXmlDataManager::ReloadOnlineBounty()
{
	WGS_XML_WRITE_LOCK
	m_mapOnlineBounty.clear();
	LoadOnlineBounty();
	return true;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetBountyOfOnline(const uint32 id)
{
	WGS_XML_READ_LOCK
	std::map<uint32, OnlineBounty>::iterator iter = m_mapOnlineBounty.find(id);
	if (iter != m_mapOnlineBounty.end())
		return iter->second.bounty;
	return 0;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetSecondsOfOnline(const uint32 id)
{
	WGS_XML_READ_LOCK
	std::map<uint32, OnlineBounty>::iterator iter = m_mapOnlineBounty.find(id);
	if (iter != m_mapOnlineBounty.end())
		return iter->second.second;
	return 0;
}
//--------------------------------------------------------
const uint32 PGXmlDataManager::GetMaxOnlineId()
{
	WGS_XML_READ_LOCK
	// std::map<uint32, OnlineBounty>::iterator iter = m_mapOnlineBounty.rebegin();
	// if (iter != m_mapOnlineBounty.rend())
		// return iter->second.first;
	// return 0;
	return m_mapOnlineBounty.size();
}
//--------------------------------------------------------
// bool PGXmlDataManager::LoadBuyLargessXml()
// {
	// TiXmlDocument doc("../scripts_xml/buy_largess.xml");	
	// if(!doc.LoadFile())
	// {
		// Log.Error("load XML Error", "load buy_largess.xml failed!");
		// return false;
	// }
	// TiXmlElement* pRoot = doc.RootElement();
	// if (NULL == pRoot)
	// {
		// Log.Error("load Xml Error", "buy_largess.xml have not root node!");
		// return false;
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/buy_largess.xml");
	// m_lstBuyLargess.clear();
	
	// while(pRoot)
	// {
		// BuyLargess buy;
		// buy.buy = StringConverter::parseUnsignedInt(pRoot->Attribute("buy"));
		// buy.largess = StringConverter::parseUnsignedInt(pRoot->Attribute("largess"));
		// buy.number = StringConverter::parseUnsignedInt(pRoot->Attribute("number"));
		// buy.exp = StringConverter::parseUnsignedInt(pRoot->Attribute("exp"));
		// m_lstBuyLargess.push_back(buy);
		// pRoot = pRoot->NextSiblingElement();
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/buy_largess.xml successfully");
	// return true;
// }
// //--------------------------------------------------------
// const BuyLargess *PGXmlDataManager::GetBuyLargess(const uint32 & buy_id)
// {
	// WGS_XML_READ_LOCK
	// std::list<BuyLargess>::iterator iter, enditer = m_lstBuyLargess.end();
	// for (iter = m_lstBuyLargess.begin(); iter != enditer; ++ iter)
	// {
		// if ((*iter).buy == buy_id)
			// return &(*iter);
	// }
	// return NULL;
// }
// //--------------------------------------------------------
// bool PGXmlDataManager::LoadGiveLeCardsXml()
// {
	// TiXmlDocument doc("../scripts_xml/reg_give_lecards.xml");	
	// if(!doc.LoadFile())
	// {
		// Log.Error("load XML Error", "load reg_give_lecards.xml failed!");
		// return false;
	// }
	// TiXmlElement* pRoot = doc.RootElement();
	// if (NULL == pRoot)
	// {
		// Log.Error("load Xml Error", "reg_give_lecards.xml have not root node!");
		// return false;
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/reg_give_lecards.xml");
	// m_lstLeCardsGive.clear();
	
	// while(pRoot)
	// {
		// LeCardGive lc;
		// lc.days = StringConverter::parseUnsignedInt(pRoot->Attribute("days"));
		// lc.num = StringConverter::parseUnsignedInt(pRoot->Attribute("num"));
		// lc.type = StringConverter::parseUnsignedInt(pRoot->Attribute("type"));
		
		// m_lstLeCardsGive.push_back(lc);
		// pRoot = pRoot->NextSiblingElement();
	// }
	
	// Log.Debug("PGXmlDataManager init", "Start load ../scripts_xml/reg_give_lecards.xml successfully");
	// return true;
// }
// //--------------------------------------------------------
// bool PGXmlDataManager::ReLoadGiveLeCardsXml()
// {
// WGS_XML_WRITE_LOCK
	// LoadGiveLeCardsXml();
	// return true;
// }
// //--------------------------------------------------------
// std::vector<LeCardGive> PGXmlDataManager::getGiveLeCardsInfo(const uint32 & reg_days)
// {
	// std::vector<LeCardGive> leCards;
	// std::list<LeCardGive>::iterator iter,ei = m_lstLeCardsGive.end();
	// for(iter = m_lstLeCardsGive.begin(); iter!=ei; ++iter)
	// {
		// if(reg_days >= (*iter).days)
		// {
			// leCards.push_back(*iter);
		// }
	// }
	
	// return leCards;
// }

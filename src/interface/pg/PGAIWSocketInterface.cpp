#include "Common.h"
#include "String.h"
#include "SharedPtr.h"
#include "ResourceManager.h"
#include "Database/DatabaseEnv.h"
#include "StringConverter.h"
#include "NGLog.h"
#include "MersenneTwister.h"
#include "ScriptMgr.h"
#include "Threading/Mutex.h"
#include "Game.h"
#include "Util.h"
#include "WorldPacket.h"
#include "Character.h"
#include "CharManager.h"
#include "WSSocket.h"
#include "Message.h"
#include "PGWorldAIInterface.h"
#include "PGAIScript.h"
#include "CenterBankManager.h"
#include "GameDefine.h"
#include XMLDATA_MGR_PATH
#include LOG_MGR_PATH

namespace AIScript {
	//-----------------------------------------------------------------------
	PGSocketInterface::PGSocketInterface(Resource * pUnit):AIInterface(pUnit)
	{
		m_AIType = AITYPE_WSOCKET;
	}

	PGSocketInterface::~PGSocketInterface(void)
	{
	}
	
	void 	PGSocketInterface::Load(void)
	{
		AIInterface::Load();
	}
	
	void 	PGSocketInterface::OnActivate(ResourcePtr & pParter)
	{
		
	}
	
	void  PGSocketInterface::Deactivate( ResourcePtr & pParter )
	{
		
		// WSSocket * pSocket = (WSSocket*)m_Unit;
		// CharPtr pChr = pSocket->getCurrentCharacter();
		// UserPtr pUser = pSocket->getCurrentUser();
		// if(pChr.isNull() || pUser.isNull()) 
		// 	return;
		// PGCreatureInterface* pAiChrInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface());
		// if (NULL != pAiChrInfe)
		// {
		// 	//assert(0);
		// 	pAiChrInfe->UpdateCommonOnlineGiftData(true);
			
		// 	// ����������ϣ��뿪����
		// 	uint32 roomId = pAiChrInfe->GetTownId();
		// 	if (0 == roomId)
		// 		pAiChrInfe->SetTownAndChannelId();
		// 	else
		// 	{
		// 		assert(0);
		// 		// �ҵ�����
		// 		PGTownInterface *pAiTownInfe;
		// 		TownPtr pTown = sTownMgr.getByHandle(roomId);
		// 		if (pTown.isNull() || 
		// 			NULL == (pAiTownInfe = TO_TOWN_INTERFACE(pTown->getAIInterface())))
		// 			pAiChrInfe->SetTownAndChannelId();
		// 		else
		// 			pAiTownInfe->LeaveChannel(pChr, true);
		// 	}
		// }
		// // �˳�����־
		// if (NULL != pAiChrInfe && pAiChrInfe->GetLastLoginTime() < (int32)pUser->getUInt32Field("ll_time"))
		// {
		// 	// �������߷�����
		// 	if (pUser->getUInt32Field("ll_time"))
		// 	{
		// 		uint32 online = (time(0) - pUser->getUInt32Field("ll_time")) / 60;
		// 		pUser->setUInt32Field("points", pUser->getUInt32Field("points") + online);
		// 	}
		// 	sLogDataMgr.LogoutLog(pUser, sChannelMgr.getServerID());
		
		// }
		// if (sXmlDataMgr.GetConfXMLValue("ENABLE_YGA") && pChr->getStatus() == CharacterStatusFree)
		// {
		// 	// ��������������뿪ʱ��������
		// 	UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
		// 	if (!pUser.isNull())
		// 	{
		// 		sBankMgr.DisconnectServer(pUser, pChr->getUInt32Field("gz_id"));
		// 	}
		// }
		// pUser->setUInt32Field("status", UserStatus_Offline);
		// pUser->SaveDB();
		// pChr->SaveDB();
		WSSocket *pSocket = (WSSocket *)m_Unit;

		CharPtr pChr = pSocket->getCurrentCharacter();
		if (pChr.isNull())
			return;

		UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
		if (pUser.isNull())
			return;

		pUser->setUInt32Field("status", UserStatus_Offline);
		pUser->SaveDB();

		uint8 online_giftLV = pChr->getUInt32Field("ability");
		if (online_giftLV < sXmlDataMgr.GetMaxOnlineId()) //新手在线礼包
		{
			uint32 login_time = pChr->getUInt32Field("defence_ext");
			uint32 last_time = time(0) - login_time;

			uint32 online_maxTime = sXmlDataMgr.GetSecondsOfOnline(online_giftLV + 1);
			uint32 online_time = pChr->getUInt32Field("attack_ext");

			if (online_maxTime > online_time)
			{
				pChr->setUInt32Field("attack_ext", online_maxTime <= online_time + last_time ? online_maxTime : online_time + last_time);
			}
		}

		PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
		if (!aii)
			return;

		TownPtr town = aii->GetTown();
		if (!town.isNull())
		{
			PGTownInterface *aiTown = TO_TOWN_INTERFACE(town->getAIInterface());
			if (aiTown)
			{
				aiTown->LeaveChannel(pChr, true);
			}
		}

		AIInterface::Deactivate(pParter);
	}
	
}

#include "Missions.h"
#include "MissionModels.h"
#include "MissionModelManager.h"
#include "PGUserDefinedManager.h"


MissionBuildManager::MissionBuildManager(void)
{
	m_missionModels.clear();
	m_isInit = false;
}

void MissionBuildManager::Load(const uint32 & gmType/*  = 0 */)
{
	if(!m_isInit)
	{
		m_isInit = true;
		m_missionModels.clear();
		sMissionModelMgr.getMissionModelsList(&m_missionModels, 0, 1000, "status <> 255 AND icon = %u", gmType);
	}
}

MissionModelPtr		MissionBuildManager::BuildMissionModel(const uint32 & tmType, const uint32 & gmType/*  = 0 */)
{
	uint32 maxRand = 0;
	std::vector<ResourcePtr> vcTempModels;
	std::list<ResourcePtr>::iterator mi, endmi = m_missionModels.end();
	for(mi = m_missionModels.begin(); mi != endmi; ++ mi)
	{
		maxRand = maxRand + (uint32)((*mi)->getFloatField("fdata1") * 100.0f);
		uint32 pos = RandomUInt(vcTempModels.size());
		vcTempModels.insert(vcTempModels.begin()+pos, *mi);
	}
	
	uint32 rand = RandomUInt(maxRand);
	uint32 rIndex = 0;
	std::vector<ResourcePtr>::iterator vmi, endvmi = vcTempModels.end();
	for(vmi = vcTempModels.begin(); vmi != endvmi; ++ vmi)
	{
		rIndex = rIndex + (uint32)((*vmi)->getFloatField("fdata1") * 100.0f);
		if(rand <= rIndex)
			return (*vmi);
	}
	
	MissionModelPtr model;
	return model;
}


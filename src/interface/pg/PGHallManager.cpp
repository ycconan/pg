#include "PGHallManager.h"
#include "PGProtocolsManager.h"
#include "Towns.h"
#include "TownManager.h"
#include "WSSocket.h"
#include "WorldPacket.h"
#include "Users.h"
#include "UserManager.h"
#include "Character.h"
#include "CharManager.h"
#include "NGLog.h"
#include "ItemManager.h"
#include "Effects.h"
#include "EffectManager.h"
#include "Database/DatabaseEnv.h"
#include "Message.h"
#include "MD5.h"
#include "PGPubStruct.h"
#include "PGWorldAIInterface.h"
#include "PGManualThreadsManager.h"
#include "Tools.h"
#include "NoticeManager.h"
#include "VipCardManager.h"
#include "CenterBankManager.h"
#include "showip.h"
#include "PGOpcodesEx.h"
#include "DataTransferManager.h"
#include "GameDefine.h"
#include ENUM_PATH
#include LOGIC_MGR_PATH
#include XMLDATA_MGR_PATH
#include PROTOCOLS_MGR_PATH
#include LOG_MGR_PATH

using std::list;
using std::map;
using namespace AIScript;
// #include "json/json.h"

HallManager::HallManager()
{
	// std::map<String, GZIDInfo> mapGZIDInfo = sXmlDataMgr.GetGZIDInfo();
	// std::map<String, GZIDInfo>::iterator iter = mapGZIDInfo.begin();
	// for (; iter != mapGZIDInfo.end(); ++iter)
	// {
		// AccountDatabase.WaitExecute("update `users` set `status` = %u where `gz_id` = %u",UserStatus_Offline,iter->second.gzId);
	// }
}
//-------------------------------------------
HallManager::~HallManager()
{}
//-------------------------------------------
void HallManager::Init()
{
	// std::list<ResourcePtr> lstItem;
	// sItemMgr.getDBObjects(&lstItem,0,10000,"hp > 0");
	// std::list<ResourcePtr>::iterator iter = lstItem.begin();
	// for (; iter != lstItem.end(); ++iter)
	// {
		// uint32 chrId = (*iter)->getUInt32Field("container");
		// uint64 bank = (*iter)->getUInt32Field("hp");
		// ResourcePtr chr = sCharMgr.getDBObjectByHandle(chrId);
		// if (!chr.isNull())
		// {
			// uint32 uId = chr->getUInt32Field("userid");
			// ResourcePtr user = sUserMgr.getDBObjectByHandle(uId);
			// if (!user.isNull())
			// {
				// if (!CenterDatabase.WaitExecute("insert into game_bank(`platform_id`,`group_id`,`type`,`status`,`gz_id`,`uid`,`name`,`nick`,`coins`,`last_time`) values(%u,%u,1,1,%u,%u,'%s','%s',%u,%u)", 
											// user->getUInt32Field("platform_id"), user->getUInt32Field("group_id"), user->getUInt32Field("gz_id"), uId,  
											// user->getStringField("name").c_str(), user->getStringField("nick").c_str(), bank, time(0)))
				// {
					// Log.Error("xxxxxxxxx", "user[%u] transfe bank coins[%u] error!", uId, bank);
					// assert(0);
				// }
			// }
		// }
	// }
	// std::list<CharPtr> lstChrs;
	// sCharMgr.getCharsList(&lstChrs,0,1000,"user_data like '%VIP_ABATE\"%'");
	// std::list<CharPtr>::iterator iter = lstChrs.begin();
	// for (; iter != lstChrs.end(); ++iter)
	// {
		// XMLDataSet &dataSet = (*iter)->user_data;
		// if (dataSet.isPresented("VIP_CARDID"))
		// {
			// uint32 cardId = *dataSet.Get<uint32>("VIP_CARDID");
			// uint32 date = *dataSet.Get<uint32>("VIP_VIPDATE");
			// dataSet.removeXMLData("VIP_CARDID");
			// dataSet.removeXMLData("VIP_VIPDATE");
			// if (date)
				// dataSet.New<uint32>(VIPCARDDATE(cardId), date);
		// }
		// dataSet.removeXMLData("VIP_ABATE");
		// (*iter)->SaveDB();
	// }
	// Log.Error("xxxxxxxxx", "==================deal%d!", lstChrs.size());
}
//-------------------------------------------
void HallManager::Update()
{
	static bool bInit = false;	
	if(!bInit)
	{
		bInit = true;
		sHallMgr.Init();
		// 更新排名
		UpdatePlayerQueueTop();	
	}
	static bool bUpdate = false;
	if (g_localTime.tm_hour == 0 && g_localTime.tm_min == 0)
	{
		if (bUpdate)
			return;
		bUpdate = !bUpdate;
		UpdatePlayerQueueTop();
		// 轮盘重置
		// sTurnplate.ResetData();
	}
	else
		bUpdate = false;
}
//-------------------------------------------
void HallManager::UpdatePlayerQueueTop()
{
	m_bByGzid = (bool)sXmlDataMgr.GetConfXMLValue("IS_BY_GZID");
	if (m_bByGzid)
	{
		std::map<String, GZIDInfo> mapGZIDInfo = sXmlDataMgr.GetGZIDInfo();
		std::map<String, GZIDInfo>::iterator iter = mapGZIDInfo.begin();
		for (; iter != mapGZIDInfo.end(); ++iter)
		{
			for (int32 type = en_TopQueque_Coin; type <= en_TopQueque_BestScore; ++type)
				ThreadPool.ExecuteTask(new LoadPlayerTop(type, iter->second.gzId));
		}
	}
	else
	{
		for (int32 type = en_TopQueque_Coin; type <= en_TopQueque_BestScore; ++type)
			ThreadPool.ExecuteTask(new LoadPlayerTop(type, 0));
	}
}
//-------------------------------------------
void HallManager::UpdatePlayerTop(const uint32 gzId, const uint32 topType, std::list<QueueTop> &lstQT)
{
	WGS_HALL_INNER_LOCK
	std::map<uint32, std::map<uint32, TopList> >::iterator iter = m_mapTops.find(gzId);
	if (iter == m_mapTops.end())
	{
		std::map<uint32, TopList> myTop;
		myTop.insert(std::make_pair(topType, lstQT));
		m_mapTops.insert(std::make_pair(gzId, myTop));
	}
	else
	{
		std::map<uint32, TopList>::iterator iterTop = iter->second.find(topType);
		if (iterTop == iter->second.end())
		{
			iter->second.insert(std::make_pair(topType, lstQT));
		}
		else
		{
			iterTop->second.clear();
			iterTop->second = lstQT;
		}
	}
}

//-------------------------------------------
void HallManager::SendHallUIInfo(WSSocketPtr &socket, CharPtr &pChr)
{
	if (socket.isNull() || pChr.isNull())
		return;
	// 下发房间列表
	std::list<TownPtr> lstTowns;
	sTownMgr.getTownsList(&lstTowns, 0, 100, "flag = 1");
	if (lstTowns.empty())
	{
		Log.Error("HallManager::Init()", "town list is empty!");		
		return;
	}
	WorldPacket packet;
	// if (sProtocolsMgr.CreateRoomListPacket(&packet, lstTowns))
	// 	socket->SendPacket(&packet);
	// 下发未读邮件数量
	UserPtr pUser = socket->getCurrentUser();
	if (sProtocolsMgr.CreateSendUnreadMailNumberPacket(&packet, pUser))
		socket->SendPacket(&packet);
}
//-------------------------------------------
void HallManager::GetTopQueue(WSSocketPtr &socket, const uint32 gzId, const uint32 topType)
{
	if (socket.isNull())
		return;
	TopList lstTop;
	{
		WGS_HALL_INNER_LOCK
		std::map<uint32, std::map<uint32, TopList> >::iterator iter;
		if (m_bByGzid)
		{
			iter = m_mapTops.find(gzId);
		}
		else
		{
			iter = m_mapTops.begin();
		}
		if (iter != m_mapTops.end())
		{
			std::map<uint32, TopList>::iterator iterTop = iter->second.find(topType);
			if (iterTop != iter->second.end())
			{
				lstTop = iterTop->second;
			}
		}
	}
	WorldPacket packet;
	// if (sProtocolsMgr.CreateTopQueuePacket(&packet, topType, lstTop))
	// 	socket->SendPacket(&packet);
}
//-------------------------------------------
void HallManager::LoginServer(WSSocketPtr &socket, String &strSession, const uint32 isCenter)
{
	// std::vector<String> vecStr = StringUtil::split(strSession,":");
	// if(vecStr.size() < 3)
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s Error session authentication packet session:%s.", 
		// socket->getHandle(),socket->GetIP(), strSession.c_str());
		// socket->Disconnect();
		// return;
	// }
	// uint32 device = vecStr.size() > 3 ? StringConverter::parseUnsignedInt(vecStr[3]) : 0;
	// if (device > 0 && device != UserStatus_MobileByUnity)
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s telephone user is refused.", socket->getHandle(),socket->GetIP());
		// socket->Disconnect();
		// return;
	// }
	// String szUser = vecStr[0], szKey= vecStr[1], szMd5 = vecStr[2];
	// String szIP = "127.0.0.1";
	// //获得用户表
	// WorldPacket packet;
	// UserPtr pUser = sUserMgr.load(ServerDatabase.EscapeString(szUser));	
	// if(pUser.isNull())
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s Error User account  User:%s", 
		// socket->getHandle(),socket->GetIP(),szUser.c_str());
		// if (sProtocolsMgr.CreateUserLoginPacket(&packet, en_LS_InvalidUserOrSession))
			// socket->SendPacket(&packet);
		// socket->Disconnect();
		// return;
	// }
	// // 是否封号
	// if (pUser->getUInt32Field("ban_time") > time(0))
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s was baned, User account :%s", 
		// socket->getHandle(),socket->GetIP(),szUser.c_str());
		// if (sProtocolsMgr.CreateUserLoginPacket(&packet, en_LS_InvalidUserOrSession))
			// socket->SendPacket(&packet);
		// socket->Disconnect();
		// return;
	// }
	// // 验证密码
	// CMD5 md5;
	// String szPass = pUser->getStringField("passwd");
	// String transPass = md5.GenerateMD5(szPass.c_str());
	// String fkgame = "FKGAME";
	// uint32 uint_key = StringConverter::parseUnsignedInt(szKey);
	// uint_key ^= 0xC3;
	// String transKey = StringConverter::toString(uint_key);
	// String token = transPass + szIP + szUser + transKey + fkgame;
	// String md5Str = md5.GenerateMD5(token.c_str());	
	// if(md5Str != szMd5)
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s Error szPass:%s", socket->getHandle(),socket->GetIP(),szPass.c_str());
		// if (sProtocolsMgr.CreateUserLoginPacket(&packet, en_LS_InvalidUserOrSession))
			// socket->SendPacket(&packet);
		// socket->Disconnect();
		// return;
	// }
	// CharPtr pChr;
	// ResourceProxy old_socket;
	// // 获取角色信息
	// uint32 char_id = pUser->getUInt32Field("char_id");
	// if (sXmlDataMgr.GetConfXMLValue("ENABLE_YGA"))
	// {
		// if (isCenter == 2)
		// {// 释放游戏数据
			// old_socket = pUser->m_Socket;
			// pChr = sCharMgr.load(char_id);
			// if (pChr.isNull())
			// {
				// socket->Disconnect();
				// return;
			// }
			// uint32 sleep = sXmlDataMgr.GetConfXMLValue("DISCONNECT_SLEEP");
			// if (sleep == 0)
				// sleep = 50;
			// uint8 isError = !(pChr->getStatus() == CharacterStatusFree && old_socket.isNull());
			// packet.clear();
			// uint16 packlen = 5;
			// packet.reserve(packlen);
			// packet.SetOpcode(SMSG_GETOTHERSERVER_RESULT);	
			// packet << (uint16)SMSG_GETOTHERSERVER_RESULT << (uint16)packlen << (uint8)isError;		
			// if (isError)
			// {
				// socket->SendPacket(&packet);
				// Sleep(sleep);
				// socket->Disconnect();
				// Log.Warning("login test","pChr[%s] is error!", pChr->getStringField("name").c_str(), packet.size());	
			// }
			// else
			// {
				// sBankMgr.DisconnectServer(pUser, pUser->getUInt32Field("gz_id"));
				// socket->SendPacket(&packet);
				// if (!old_socket.isNull())
				// {
					// Sleep(sleep);
					// WSSocketPtr pSocket = old_socket.getResourcePtr();
					// pSocket->Disconnect();
					// socket->Disconnect();
				// }
				// Log.Warning("login test","pChr[%s] is error!", pChr->getStringField("name").c_str(), packet.size());				
			// }
			// return;
		// }
		// else
		// {
			// if (!sBankMgr.ExistBank(pUser)){
				// // 不存在银行，首次登陆
				// pUser->setUInt32Field("flag", 0);
				// // 创建银行，防止重复奖励
				// sBankMgr.AddBankInfo(pUser);
			// }
			// else
			// {
				// pUser->setUInt32Field("flag", 1);
			// }
		// }
	// }
	// else
	// {
		// // 游戏服务器,标记登陆模式
		// pUser->setUInt32Field("flag", isCenter);
	// }
	// // 如果没有角色，创建，有就返回角色对象指针
	// pChr = sPubLogic.CreateCreature(pUser, isCenter);
	// if(pChr.isNull())
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s Error user character:%d.", 
		// socket->getHandle(),socket->GetIP(),char_id);
		// if (sProtocolsMgr.CreateUserLoginPacket(&packet, en_LS_InvalidUserOrSession))
			// socket->SendPacket(&packet);
		// socket->Disconnect();
		// return;
	// }
	// using AIScript::PGCreatureInterface;
	// PGCreatureInterface *pChrAIInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	// if (pChrAIInfe)
		// pChrAIInfe->SetCanDelete(false);
	// WGS_LOGIN_INNER_LOCK
	// {
		// old_socket = pUser->m_Socket;
		// // 更新socket
		// if(!old_socket.isNull())
		// {
			// if(old_socket._handle != socket->getHandle())
			// {
				// WSSocketPtr old_sock = old_socket.getResourcePtr();
				// if(!old_sock.isNull())
				// {
					// old_sock->Disconnect();
				// }
			// }
		// }
		// socket->setCurrentUser(pUser);
		// socket->setCurrentCharacter(pChr);
		// pUser->m_Socket = (ResourceProxy)socket;
	// }

	// if (device > 0)
	// {
		// // 手机在线
		// pUser->setUInt32Field("status", device == 1 ? UserStatus_MobileOnline : device);
		// // 不检查ping
		// socket->setCheckTimetOut(false);
	// }
	// else
	// {
		// // pc在线
		// pUser->setUInt32Field("status", UserStatus_Online);
	// }
	// // 语言版本
	// if (vecStr.size() > 4)
	// {
		// pUser->setUInt32Field("country_id", StringConverter::parseInt(vecStr[4]));
	// }
	// else
	// {
		// Log.Debug("HallManager::LoginServer","Client[%d] %s Error language data:%s", socket->getHandle(),socket->GetIP(),strSession.c_str());
		// pUser->setUInt32Field("country_id", en_Language_Chinese);
	// }
	
	// uint32 gameid = pUser->getUInt32Field("gz_id");
	// if(pChr->getUInt32Field("ishere") == 0)
		// pChr->setUInt32Field("ishere", gameid);
	// // 财神驾到（免费赠送老玩家一次卡(仅此一次))
	// sPubLogic.MammonCome(pChr);
	// // 发送消息，登录成功
	// if (sProtocolsMgr.CreateUserLoginPacket(&packet, en_LS_Ok))
		// socket->SendPacket(&packet);
	// Log.Debug("HallManager::LoginServer","Client[%d] %s login ok. user = [%s] ========= Now[%u], isCenter[%u]", 
		// socket->getHandle(),socket->GetIP(), szUser.c_str(), now(), isCenter);
}
//-------------------------------------------
void HallManager::AfterLogin(WSSocketPtr &socket, UserPtr &pUser, CharPtr &pChr)
{
	// if (socket.isNull() || pUser.isNull() || pChr.isNull())
		// return;
	// WorldPacket packet;
	// uint16 currGZID = 0;
	// bool isJoin = sBankMgr.JoinGameServer(pUser, pUser->getUInt32Field("gz_id"), &currGZID);
	// if (sXmlDataMgr.GetConfXMLValue("ENABLE_YGA"))
	// {// 玩家还在其它游戏内，不允许登陆
		// pUser->setUInt32Field("flag", 1);
		// if (isJoin == false)
		// {
			// if (pUser->getUInt32Field("gz_id") != currGZID)
			// {
				// WorldPacket packet;
				// packet.clear();
				// uint16 packlen = 6;
				// packet.reserve(packlen);
				// packet.SetOpcode(SMSG_GETOTHERSERVER_DATA);	
				// packet << (uint16)SMSG_GETOTHERSERVER_DATA << (uint16)packlen << (uint16)currGZID;
				// socket->SendPacket(&packet);
				// pUser->setUInt32Field("flag", 2);
				// Log.Warning("login test","111char[%s] join gameserver error!", pUser->getStringField("nick").c_str(), packet.size());
			// }
			// else
			// {
				// Log.Warning("login test","222char[%s] join gameserver error!", pUser->getStringField("nick").c_str(), packet.size());
				// socket->Disconnect();
			// }
			// return;
		// }
	// }
	// sDataTranMgr.ResetRecevingData(pUser);
	// uint8 ishere =  currGZID ? currGZID : pUser->getUInt32Field("gz_id");
	// // 设置玩家当前位置
	// pChr->setUInt32Field("ishere", ishere);
	// // 告诉玩家目前的游戏
	// if(sBankMgr.CreateOpServerResult(&packet, 0, 0, ishere))
		// socket->SendPacket(&packet);
	// // 下发系统设置
	// if (sProtocolsMgr.CreateSystemSetPacket(&packet, pUser))
		// socket->SendPacket(&packet);
	// // 下发自身基本信息
	// if (sProtocolsMgr.CreateCharacterBaseInfoPacket(&packet, pChr))
		// socket->SendPacket(&packet);
	// // 背包信息
	// if(!pChr->isNPC() && sProtocolsMgr.CreateOpenBackpackPacket(&packet, pChr))
	// {
		// socket->SendPacket(&packet);
	// }
	// uint32 nowtime = time(0);
	// // 是否开户首充通道
	// BankPublicData bank_data;
	// sBankMgr.GetPublicData(pUser, &bank_data);
	// if (!(bank_data.FirstBuyTimer && sTools.GetOffsetDate(bank_data.FirstBuyTimer, nowtime) < 30))
	// {// 关闭首充通道
		// if (sProtocolsMgr.CreateTellToDoPacket(&packet, en_RemindType_OpenFirstBuy))
			// socket->SendPacket(&packet);
	// }
	// // 注册时间
	// if(sProtocolsMgr.CreateUserPublicDataPacket(&packet,pUser->getUInt32Field("reg_time")))
		// socket->SendPacket(&packet);
	// // 注册时间半年以上的玩家检测乐码赠送
	// if(sTools.GetOffsetDate(pUser->getUInt32Field("reg_time"), nowtime) >= 30)
	// {
		// uint32 pid = pUser->getUInt32Field("platform_id");
		// if (!bank_data.GetLemaTime || !sTools.IsSameWeek(nowtime,bank_data.GetLemaTime))
		// {
			// sPubLogic.CheckTimeOverLeCards(pUser->getUInt32Field("platform_id"));
			// if(sProtocolsMgr.CreateLeCardsCoolDownPacket(&packet,0))
				// socket->SendPacket(&packet);
		// }
		// else
		// {
			// time_t getleT = bank_data.GetLemaTime;
			// tm getLeTm = *(localtime(&getleT));
			// uint32 distence_days = getLeTm.tm_wday == 0 ? 1 : 8 - getLeTm.tm_wday;
			// if(sProtocolsMgr.CreateLeCardsCoolDownPacket(&packet,distence_days))
				// socket->SendPacket(&packet);
		// }
		
		// uint32 count = sItemMgr.getExchangeDBCount("(source = %u and (status = %u or status = %u) )",
											// pid,enExchangeStataus_Process,enExchangeStataus_YanZheng);
		// if(count)
		// {
			// std::list<GameExchange> exchanges;
			// sItemMgr.getExchangeListDB(&exchanges,0,count,"(source = %u and (status = %u or status = %u) )",
												// pid,enExchangeStataus_Process,enExchangeStataus_YanZheng);
			
			// std::list<GameExchange> realExchanges;
			// std::list<GameExchange>::iterator it2,ei2 = exchanges.end();
			// for(it2 = exchanges.begin(); it2!=ei2; ++it2)
			// {
				// if((*it2).data2 <= nowtime)
				// {
					// sItemMgr.deleteExchangeDB((*it2).exchange_id);
				// }
				// else
				// {
					// realExchanges.push_back(*it2);
				// }
			// }
			
			// if(sProtocolsMgr.CreateLeCardsListPacket(&packet,&realExchanges))
				// socket->SendPacket(&packet);
		// }
	// }
	// // 判断是否直接进牌桌接着打牌
	// using AIScript::PGCreatureInterface;
	// PGCreatureInterface *pChrAIInfe = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	// if (NULL == pChrAIInfe)
		// return;
	// pChrAIInfe->SetLastLoginTime(pUser->getUInt32Field("ll_time"));
	// // 房间ID
	// uint32 roomId = pChrAIInfe->GetTownId();
	// if (roomId > 0)
	// {
		// // 通知直接去牌桌
		// if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet, roomId))
			// socket->SendPacket(&packet);
		// // 如果不是unity版，直接进入房间
		// if (pUser->getUInt32Field("status") != UserStatus_MobileByUnity && !JoinTable(socket, pChr, roomId, pChrAIInfe->GetChannelId()))
		// {
			// // 如果失败，进入大厅
			// if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet))
				// socket->SendPacket(&packet);
			// // SendHallUIInfo(socket, pChr);
		// }
	// }
	// else
	// {
		// // 进入大厅
		// if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet))
			// socket->SendPacket(&packet);
		// // // 主动下发大厅相关信息
		// // SendHallUIInfo(socket, pChr);
	// }
	// // 下发大厅相关信息
	// SendHallUIInfo(socket, pChr);
	// // 下发欢迎消息
	// sPubLogic.LoginWellcomeMessage(pUser);
	// // 如果当天第一次登录，检测排行榜，如果前三，通知发分享
	// if (!sTools.IsSameDate(pUser->getUInt32Field("ll_time")))
	// {
		// WGS_HALL_INNER_LOCK
		// std::map<uint32, std::map<uint32, TopList> >::iterator iter;
		// if (m_bByGzid)
		// {
			// iter = m_mapTops.find(pChr->getUInt32Field("gz_id"));
		// }
		// else
		// {
			// iter = m_mapTops.begin();
		// }
		// if (iter != m_mapTops.end())
		// {
			// std::map<uint32, TopList>::iterator iterTop = iter->second.begin();
			// for (; iterTop != iter->second.end(); ++iterTop)
			// {
				// TopList::iterator myiter = iterTop->second.begin();
				// uint32 index = 0;
				// for (; myiter != iterTop->second.end(); ++myiter)
				// {
					// ++index;
					// if ((*myiter).id == pChr->getHandle())
					// {
						// if (sProtocolsMgr.CreateShareHonorPacket(&packet, 1, iterTop->first, index))
							// socket->SendPacket(&packet);
					// }
					// if (index == 3)
						// break;
				// }
			// }
		// }
	// }
	// // 角色响应登录完成事件
	// ResourceEvent event;
	// event.event_id = en_ResourceEvent_Login;
	// pChrAIInfe->SendEvent(&event);
	// // 更新用户信息
	// pUser->setUInt32Field("ll_time",time(0));
	// pUser->setStringField("ip", socket->GetIP());
	// pUser->setStringField("addr", sShowIP.getAddress(socket->GetIP()));
	// pUser->SaveDB();
	// // // 通知改名字
	// // if (pChr->getStringField("name2").empty())
	// // {
		// // if (sProtocolsMgr.CreateNotifyRenamePacket(&packet))
			// // socket->SendPacket(&packet);
		// // return;
	// // }
	// // 记录登录成功日志
	// sLogDataMgr.LoginLog(pUser, sChannelMgr.getServerID());
	// return;
}
//-------------------------------------------
void HallManager::AfterLoginWorldOk(WSSocketPtr &socket, UserPtr &pUser, CharPtr &pChr)
{
	if (socket.isNull() || pUser.isNull() || pChr.isNull())
		return;

	WorldPacket newP;
	if (sProtocolsMgr.CreateCharacterInfoPacket(&newP, pChr)) //角色信息
		socket->SendPacket(&newP);
	if (sProtocolsMgr.CreateOpenBackpackPacket(&newP, pChr)) //打开背包
		socket->SendPacket(&newP);
	if (sProtocolsMgr.CreateSystemSetPacket(&newP, pUser)) //下发系统设置
		socket->SendPacket(&newP);
	if (sVipCardMgr.CreateUpdateMorraNum(&newP, pChr)) //猜拳次数
		socket->SendPacket(&newP);
	// // 下发牌桌列表
	// GetRoomList(socket);

	PGCreatureInterface *aii = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if (!aii)
		return;

	WorldPacket packet;
	TownPtr pTown = aii->GetTown();
	if (!pTown.isNull())
	{
		//通知直接去牌桌
		if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet, pTown->getHandle()))
		{
			Log.Debug("HallManager::AfterLoginWorldOk", "通知直接去牌桌");
			socket->SendPacket(&packet);
		}

		// 进入房间
		if (!JoinRoom(pChr, pTown))
		{
			// 如果失败，进入大厅
			if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet))
				socket->SendPacket(&packet);

			Log.Debug("HallManager::AfterLoginWorldOk", "如果失败，进入大厅");
		}
	}
	else
	{
		// 进入大厅
		if (sProtocolsMgr.CreateGoHallOrTablePacket(&packet))
			socket->SendPacket(&packet);

		Log.Debug("HallManager::AfterLoginWorldOk", "pTown is null 进入大厅");
	}

}
bool HallManager::JoinRoom(CharPtr &pChr, TownPtr &pTown)
{
	if (pChr.isNull() || pTown.isNull())
		return false;

	PGTownInterface *pTownAiInfe = TO_TOWN_INTERFACE(pTown->getAIInterface());
	if (pTownAiInfe)
		return pTownAiInfe->JoinRoom(pChr);
	return false;
}

//-------------------------------------------
bool HallManager::JoinTable(WSSocketPtr &socket, CharPtr &pChr, const uint32 roomId, const uint32 tableId)
{
	// if (socket.isNull() || pChr.isNull() || 0 == roomId)
	// 	return false;
	// TownPtr pTown = sTownMgr.getByHandle(roomId);
	// if (pTown.isNull())
	// 	return false;
	// using AIScript::PGTownInterface;	
	// PGTownInterface *pAiTownInfe = TO_TOWN_INTERFACE(pTown->getAIInterface());
	// if (NULL == pAiTownInfe)
	// 	return false;
	// uint8 result = pAiTownInfe->JoinOneChannel(pChr, tableId);
	// //加入成功在内部下发 
	// if (en_JoinTableResult_Ok != result)
	// {
	// 	WorldPacket packet;
	// 	if (sProtocolsMgr.CreateJoinTableReultPacket1(&packet, result,tableId,true))
	// 		socket->SendPacket(&packet);
	// }
	return true;
}
//-------------------------------------------
void HallManager::Rename(WSSocketPtr &socket, WorldPacket &packet)
{
	if (socket.isNull())
		return;
	CharPtr pChr = socket->getCurrentCharacter();
	UserPtr pUser = socket->getCurrentUser();
	if (pChr.isNull() || pUser.isNull())
		return;
	String strNewName;
	GetPacketString<uint8>(&packet, strNewName);
	// bool bNotCheck = false; // 是否不检查唯一性
	// packet >> bNotCheck;
	// if (strNewName.length() > 18 || strNewName.empty())
		// return;
	// // 如果是腾讯平台，改过名字就不再改了
	// if (pChr->getStringField("name2") == strNewName
		// || pUser->getUInt32Field("reg_from") == en_Platform_Tencent && pChr->getStringField("name") != "" && pChr->getStringField("name") == pChr->getStringField("name2"))
		// return;
	// uint8 result = en_RenameResult_Successed;
	// // 是否含有敏感字符
	// if (sXmlDataMgr.IsExistedKeyWords(strNewName))
	// {
		// result = en_RenameResult_OutRule;
	// }
	// else
	// {
		// // 是否含有非法字符
		// for (uint32 i = 0; i < strNewName.length(); ++i)
		// {
			// char c = strNewName[i];
			// if ((i == 0 && c >= '0' && c <= '9') || c == ';' || c == ' ' || c == '\\' || c == '\'' || c == '\"' || c == ',' || c == '	')
			// {
				// result = en_RenameResult_OutRule;
				// break;
			// }
		// }
	// }
	// if (en_RenameResult_Successed == result)
	// {
		// strNewName = ServerDatabase.EscapeString(strNewName);
		// if (!bNotCheck)
		// {
			// WGS_RENAME_INNER_LOCK
			// // 检查昵称是否唯一
			// if (sCharMgr.getDBCount("`name2` = '%s'", strNewName.c_str()) > 0)
			// {
				// if (sProtocolsMgr.CreateChangeNicknamePacket(&packet, en_RenameResult_Existed))
					// socket->SendPacket(&packet);
				// return;
			// }
		// }
		// pChr->setStringField("name2", strNewName);
		// pChr->setStringField("name", strNewName);
		// pChr->SaveDB();
		
		// pUser->setStringField("nick", strNewName);
		// pUser->SaveDB();
		// // 下发自身基本信息
		// if (sProtocolsMgr.CreateCharacterBaseInfoPacket(&packet, pChr))
			// socket->SendPacket(&packet);
	// }
	// if (sProtocolsMgr.CreateChangeNicknamePacket(&packet, result))
		// socket->SendPacket(&packet);
	strNewName = CharacterDatabase.EscapeString(strNewName);
	int loc = strNewName.find("贵宾", 0);
	if (loc != -1)
	{
		strNewName = pChr->getStringField("name");
		loc = strNewName.find("贵宾", 0);
		if (loc != -1)
			strNewName = sGLMgr.GetNickWords();
	}
	
	if (pChr->getStringField("name") != strNewName)
	{
		strNewName = ServerDatabase.EscapeString(strNewName);
		pChr->setStringField("name", strNewName);
		pChr->SaveDB();
		
		pUser->setStringField("nick", strNewName);
		pUser->SaveDB();
		// 下发自身基本信息
		if (sProtocolsMgr.CreateCharacterInfoPacket(&packet, pChr))
			socket->SendPacket(&packet);
	}
}


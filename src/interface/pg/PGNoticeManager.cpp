#include "NoticeManager.h"
#include "PGPublicLogic.h"
#include "NGLog.h"
#include "PGXmlDataManager.h"

//-----------------------------------------------------------------------
NoticeManager::NoticeManager()
{
	Init();
}
void NoticeManager::Init()
{
	// 添加固定公告
	const std::list<Notice> &lstNotices = sXmlDataMgr.GetSystemNotice();
	std::list<Notice>::const_iterator iter = lstNotices.begin();
	for (; iter != lstNotices.end(); ++iter)
	{
		m_lstNotice.push_back(*iter);
	}
}
void NoticeManager::Clear()
{
	WGS_NOTICEMGR_INNER_LOCK
	m_lstNotice.clear();
	sXmlDataMgr.ReloadSystemNotices();
	Init();
}
//-----------------------------------------------------------------------
void NoticeManager::AddMsgToQueue(String &msg, const uint32 msgType, const uint32 gzId, const uint32 interval, const uint32 startDate, const uint32 finishDate, const uint32 beginTime, const uint32 endTime)
{
	if (msg == "" || 0 == msgType)
		return;
	Notice nt;
	nt.msg = msg;
	nt.msgType = msgType;
	nt.gzId = gzId;
	nt.interval = interval;
	nt.startDate = startDate;
	nt.finishDate = finishDate;
	nt.beginTime = beginTime;
	nt.endTime = endTime;
	AddMsgToQueue(nt);
}
//-----------------------------------------------------------------------
void NoticeManager::AddMsgToQueue(const char* cmsg, const uint32 msgType, const uint32 gzId, const uint32 interval, const uint32 startDate, const uint32 finishDate, const uint32 beginTime, const uint32 endTime)
{
	if (cmsg == NULL || 0 == msgType)
		return;
	Notice nt;
	nt.msg = cmsg;
	nt.msgType = msgType;
	nt.gzId = gzId;
	nt.interval = interval;
	nt.startDate = startDate;
	nt.finishDate = finishDate;
	nt.beginTime = beginTime;
	nt.endTime = endTime;
	AddMsgToQueue(nt);
}
//-----------------------------------------------------------------------
void NoticeManager::AddMsgToQueue(const Notice &nt)
{
	if (nt.msg == "" || 0 == nt.msgType || nt.startDate > nt.finishDate || nt.beginTime > nt.endTime)
	{
		Log.Error("NoticeManager::AddMsgToQueue", "Notice struct is not correct: msg = %s, msgType = %u", nt.msg.c_str(), nt.msgType);
		return;
	}
	WGS_NOTICEMGR_INNER_LOCK
	m_lstNotice.push_back(nt);
}
//-----------------------------------------------------------------------
void NoticeManager::Update(const uint32 t)
{
	WGS_NOTICEMGR_INNER_LOCK
	std::list<Notice>::iterator iter, iterTemp;
	time_t curTime = time(0);
	tm curTm = *localtime(&curTime);
	uint32 curSeconds = curTm.tm_hour * 3600 + curTm.tm_min * 60 + curTm.tm_sec;
	for (iter = m_lstNotice.begin(); iter != m_lstNotice.end();)
	{
		Notice &nt = *iter;
		// 即时公告
		if (0 == nt.startDate && 0 == nt.finishDate) 
		{
			sPubLogic.SendNotice(nt.msg, nt.msgType, nt.gzId);
			iterTemp = iter++;
			m_lstNotice.erase(iterTemp);
			// Log.Error("remove1", "%s", nt.msg.c_str());
			continue;
		}
		// 非即时公告
		if (curTime > nt.finishDate) // 公告时间结束后移除
		{
			iterTemp = iter++;
			m_lstNotice.erase(iterTemp);
			// Log.Error("remove2", "%s", nt.msg.c_str());
			continue;
		}
		if (curTime >= nt.startDate)
		{
			// 全天或者在设定时间段内，以一定的频率发送
			if ((0 == nt.beginTime && 0 == nt.endTime || 
				(curSeconds >= nt.beginTime && curSeconds <= nt.endTime)) && 
				curTime - nt.lastTime >= nt.interval)
			{
				nt.lastTime = curTime;
				sPubLogic.SendNotice(nt.msg, nt.msgType, nt.gzId);
				// Log.Error("send", "%s", nt.msg.c_str());
			}
		}
		else if (nt.lastTime)
			nt.lastTime = 0;
		++iter;
	}
}

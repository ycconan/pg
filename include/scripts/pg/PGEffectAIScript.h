#ifndef _PG_EFFECTAISCRIPT_H
#define _PG_EFFECTAISCRIPT_H

#include "PGAIScript.h"

namespace AIScript
{
	// 闲家连胜
	class DemosResultEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(DemosResultEffortAIScript);
		DemosResultEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~DemosResultEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 庄家通吃
	class BankerResultEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(BankerResultEffortAIScript);
		BankerResultEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~BankerResultEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 庄家更新（上庄，连庄）
	class BankerUpdateEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(BankerUpdateEffortAIScript);
		BankerUpdateEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~BankerUpdateEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 押到指定牌型
	class ChipTheCardsEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(ChipTheCardsEffortAIScript);
		ChipTheCardsEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~ChipTheCardsEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 押某一门获胜
	class ChipOneWinEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(ChipOneWinEffortAIScript);
		ChipOneWinEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~ChipOneWinEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 跟庄家同牌型比大小
	class SameTypeCompareEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(SameTypeCompareEffortAIScript);
		SameTypeCompareEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~SameTypeCompareEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 财富成就
	class WealthEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(WealthEffortAIScript);
		WealthEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~WealthEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 统计成就数量
	class CountEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(CountEffortAIScript);
		CountEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~CountEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
	// 赠送礼物
	class GivePresentEffortAIScript : public EffectAIScript
	{
	public:
		ADD_AI_FACTORY_FUNCTION(GivePresentEffortAIScript);
		GivePresentEffortAIScript(Resource * pUnit): EffectAIScript(pUnit){};
		~GivePresentEffortAIScript(){};
		uint8 OnEvent(ResourceEvent * event);
	};
}

#endif // _PG_EFFECTAISCRIPT_H

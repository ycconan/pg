#ifndef _PUB_DATA_MAMANAGER_H_
#define _PUB_DATA_MAMANAGER_H_

#include "Resource.h"
#include "ResourceManager.h"
#include "Singleton.h"
#include "String.h"
#include "Database/Field.h"
#include "DataManager.h"


//定义默认的数据载入XML文件
// #define DM_DB_DEFINE_FILE     "../scripts_xml/datadefine.xml"
// #define DM_DB_DATA_FILE       "../scripts_xml/database.xml"

#define DB_NAME_PUBDATA	"game_pubdata"

class Database;
class  CharPtr;
class  ItemPtr;
class  WorldPacket;

typedef struct game_pubdata_tag_
{
	uint32 pd_id;
	uint32 group_id;
	uint32 status;
	uint32 type;
	uint32 sub_type;
	uint32 flag;
	uint32 gz_id;
	uint32 src_gz_id;
	uint32 src_id;
	uint32 src_type;
	uint32 src_platform_id;
	String src_nick;
	String src_addr;
	uint32 dest_id;
	uint32 dest_type;
	uint32 dest_platform_id;
	String dest_nick;
	String dest_addr;
	uint32 ndata1;
	uint32 ndata2;
	uint32 ndata3;
	uint32 ndata4;
	String sdata1;
	String sdata2;
	String sdata3;
	String sdata4;
	String desc;
	String info;
	String create_time;
	void Init()
	{
		pd_id = 0;
		group_id = 0;
		status = 0;
		type = 0;
		sub_type = 0;
		flag = 0;
		gz_id = 0;
		src_gz_id = 0;
		src_id = 0;
		src_type = 0;
		src_platform_id = 0;
		src_nick = "";
		src_addr = "";
		dest_id = 0;
		dest_type = 0;
		dest_platform_id = 0;
		dest_nick = "";
		dest_addr = "";
		ndata1 = 0;
		ndata2 = 0;
		ndata3 = 0;
		ndata4 = 0;
		sdata1 = "";
		sdata2 = "";
		sdata3 = "";
		sdata4 = "";
		desc = "";
		info = "";
		create_time = "";
	}
	game_pubdata_tag_()
	{
		Init();
	}
}GamePubData;
//-----------------------公共数据--------------------------
class  SERVER_DECL PubDataManager:public Singleton<PubDataManager>
{
public:
	// 类型定义
	enum en_PubDataType
	{
		en_PubDataType_None = 0,
		en_PubDataType_LotteryTicket = 1,	// 彩票
		en_PubDataType_HongBao = 2,			// 红包
	};
public:
	/**
	 * @创建时间 2015-10-27
	 * @创建人 StarX
	 * @函数作用 添加一条PD数据,添加成功会对pd_id赋值
	 * @参数
	 * 	@pd	PD对象
	 */
	bool AddPubData(GamePubData &pd);
	/**
	 * @创建时间 2015-10-27
	 * @创建人 StarX
	 * @函数作用 更新一条PD数据，全字段更新
	 * @参数
	 * 	@pd	PD对象
	 */
	bool UpdatePubData(const GamePubData &pd);
	/**
	 * @创建时间 2015-10-27
	 * @创建人 StarX
	 * @函数作用 根据Fields的值来更新多个字段
	 * @参数
	 * 	@pd_id	PD_ID
	 *	@fields	字段键值对,格式：fields["key1"] = 1; fields["key2"] = 2;...
	 */
	bool UpdatePubData(Fields * fields, const char * QueryString, ...);
	bool UpdatePubData(const uint32 &pd_id, Fields * fields);
	/**
	 * @创建时间 2015-10-27
	 * @创建人 StarX
	 * @函数作用 根据PD_ID获取一条PD数据
	 * @参数
	 * 	@pd	PD对象
	 */
	bool GetPubData(const uint32 &pd_id, GamePubData *pd);
	/**
	 * @创建时间 2015-10-27
	 * @创建人 StarX
	 * @函数作用 根据查询SQL，获取PD列表
	 * @参数
	 * 	@pd	PD对象
	 */
	uint32 GetPubDataList(std::list<GamePubData> * lstPubData, const char * query,...);
};

#define   sPubDataMgr     PubDataManager::getSingleton()

// 红包领取记录
struct HongBaoDetail
{
	uint32 pid;			// 游戏ID
	String nick;		// 昵称
	String head_url;	// 图标
	uint32 hbNum;
	HongBaoDetail()
	{
		pid = 0;
		hbNum = 0;
		nick = "";
		head_url = "";
	}
};
// 红包排行榜
struct HongBaoTop
{
	uint32 pid;
	String nick;
	String head_url;
	uint32 hbNum;	
	HongBaoTop()
	{
		pid = 0;
		nick = "";
		head_url = "";
		hbNum = 0;
	}
};
//-------------------红包-----------------------
class  SERVER_DECL HongHaoManager:public Singleton<HongHaoManager>
{
public:
	// 玩家下注锁
	#define WGS_CHN_HONGBAO_INNER_MUTEX_NAME chn_add_chip_inner_mutex
	#define WGS_CHN_HONGBAO_INNER_MUTEX mutable boost::mutex WGS_CHN_HONGBAO_INNER_MUTEX_NAME;
	#define WGS_CHN_HONGBAO_INNER_LOCK		boost::lock_guard<boost::mutex> wgsChnAddChipInnerMutexLock(WGS_CHN_HONGBAO_INNER_MUTEX_NAME);
	WGS_CHN_HONGBAO_INNER_MUTEX
	// 红包状态
	enum en_HongBaoStatus
	{
		en_HongBaoStatus_None = 0,		// 无效
		en_HongBaoStatus_Normal = 1,	// 正常
		en_HongBaoStatus_Timeout = 2,	// 超时
		en_HongBaoStatus_GrabNull = 3,	// 抢光
	};
	HongHaoManager();
public:
	// 发红包
	bool AwardHongBao(CharPtr &pChr, const uint32 &people, const uint32 &hbCoins, const uint32 &type, const String &talk, GamePubData *phbData);
	// 抢红包	
	uint32 GrabHongBao(CharPtr &pChr, const uint32 &hbId, GamePubData *phbData);
	// 获取红包历史记录
	uint32 GetHongBaoHistory(std::map<uint32, GamePubData> *mapHistory, const uint32 &count);
	// 获取红包领取记录
	bool GetHongBaoDetail(const uint32 &hbId, std::list<HongBaoDetail> &lstDetail);
	// 获取排行榜
	void GetHongBaoTopList(std::list<HongBaoTop> * lstHBTop);
	// 获取可领取的红包列表
	void GetHongBaoCanGrabList(const uint32 &pid, uint8 count, std::map<uint32, GamePubData> *mapHBList);
	// 检测红包超时未领
	void CheckHongBaoTimeout();
	// 加载红包
	void LoadHongBao();
protected:
	// 生成红包列表
	void BuildHongBaoList(const uint32 &people, const uint32 &hbCoins, const uint32 &type, std::list<HongBaoDetail> &lstDetail);
	// JSON转详情结构
	void JsonToDetail(const String &strJson, std::list<HongBaoDetail> *plstHBD);
	// 详情结构转Json
	String DetailToJson(std::list<HongBaoDetail> &plstHBD);
	// 检查玩家是否能领取某个红包
	bool CheckCanGrab(const uint32 &pid, const GamePubData &gpd);
protected:
	// 红包记录
	std::map<uint32, GamePubData> m_mapHongBaoHistory;	
	// uint32 m_lastHBHistoryTimer;
	// 排行榜
	std::list<HongBaoTop> m_lstHongBaoTop;
	uint32 m_lastTOPTimer;
	// 超时未领红包,检测时间
	uint32 m_hongbao_timeout;
	// 红包初始化标志
	bool m_bInit;
};

#define   sHBMgr     HongHaoManager::getSingleton()

#endif

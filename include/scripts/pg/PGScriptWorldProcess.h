#ifndef __SCRIPT_PG_WORLD_PROCESS_H_
#define __SCRIPT_PG_WORLD_PROCESS_H_

#include "Common.h"

#include "WorldPacket.h"
#include "ScriptMgr.h"

class WSSocketPtr;
class TownPtr;

/*********************************************** 协议函数 **********************************************************/

//当前玩家切牌位置索引
bool SERVER_DECL pg_script_world_process_playercutcardspos(ResourcePtr &sock, WorldPacket &packet);
//下一玩家切牌I信息
bool SERVER_DECL pg_script_world_process_playercutcards(ResourcePtr &sock, WorldPacket &packet);

bool SERVER_DECL pg_script_world_process_playercutcardfinish(ResourcePtr &sock, WorldPacket &packet);
//申请上庄
bool SERVER_DECL pg_script_world_process_applybebanker(ResourcePtr &sock, WorldPacket &packet);
// 连庄或让庄
bool SERVER_DECL pg_script_world_process_continueorabandonbanker(ResourcePtr &sock, WorldPacket &packet);
// 下注
bool SERVER_DECL pg_script_world_process_chipin(ResourcePtr &sock, WorldPacket &packet);
//玩家获取提示牌型
bool SERVER_DECL pg_script_world_process_playerpromptcardsytpe(ResourcePtr &sock, WorldPacket &packet);
// 玩家看牌
bool SERVER_DECL pg_script_world_process_playerlookcards(ResourcePtr &sock, WorldPacket &packet);
//广播庄家单独看牌结果
bool SERVER_DECL pg_script_world_process_broadotherplayerlookcards(ResourcePtr &sock, WorldPacket &packet);
//玩家切牌
bool SERVER_DECL pg_script_world_process_playermovecards(ResourcePtr &sock, WorldPacket &packet);
//摆牌操作完成
bool SERVER_DECL pg_script_world_process_playermovecardsfinish(ResourcePtr &sock, WorldPacket &packet);
// 获取出牌列表
bool SERVER_DECL pg_script_world_process_getoutcards(ResourcePtr &sock, WorldPacket &packet);
//小结算确认
bool SERVER_DECL pg_script_world_process_askIsroundpgcalc(ResourcePtr &sock, WorldPacket &packet);

#endif

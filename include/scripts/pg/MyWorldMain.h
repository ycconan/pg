#ifndef _MY_WORLD_MAIN_H
#define _MY_WORLD_MAIN_H

#include "Common.h"
#include "Singleton.h"
#include "ScriptMgr.h"
#include "NGLog.h"
#include "Setup.h"
#include "WorldProcess.h"
#include "WorldScriptAI.h"
#include "Tools.h"
#include "PGProtocolsManager.h"
#include "PGPublicLogic.h"
#include "PGLogDataManager.h"
#include "PGXmlDataManager.h"
#include "PGHallManager.h"
#include "PGRuleManager.h"

#include "VipCardManager.h"
#include "CenterBankManager.h"
#include "DataTransferManager.h"
#include "GameGoodsManager.h"
#include "NoticeManager.h"
#include "GoldCoronal.h"
#include "Roulette.h"
#include "StringScreen.h"
#include "PubDataManager.h"

initialiseSingleton(Tools);
initialiseSingleton(PublicLogic);
initialiseSingleton(PGProtocolsManager);
initialiseSingleton(PGLogDataManager);
initialiseSingleton(GameGoodsManager);
initialiseSingleton(PGXmlDataManager);
initialiseSingleton(VipCardManager);
initialiseSingleton(HallManager);
initialiseSingleton(NoticeManager);
initialiseSingleton(RuleManager);
initialiseSingleton(CenterBankManager);
initialiseSingleton(DataTransferManager);
initialiseSingleton(GoldCoronal);
initialiseSingleton(Roulette);
initialiseSingleton(StringScreenMgr);
initialiseSingleton(HongHaoManager);
initialiseSingleton(PubDataManager);


#define CreateLibSingleton( type ) \
  static  type the##type


void my_exp_script_register(ScriptMgr* mgr) 
{
	// 创建单例
	CreateLibSingleton(Tools);
 	CreateLibSingleton(PublicLogic);
	CreateLibSingleton(PGProtocolsManager);
	CreateLibSingleton(PGLogDataManager);
 	CreateLibSingleton(GameGoodsManager);
	CreateLibSingleton(PGXmlDataManager);
	CreateLibSingleton(VipCardManager);
 	CreateLibSingleton(HallManager);
 	CreateLibSingleton(NoticeManager);
 	CreateLibSingleton(RuleManager);
	CreateLibSingleton(CenterBankManager);
	CreateLibSingleton(DataTransferManager);
	CreateLibSingleton(GoldCoronal);
	CreateLibSingleton(Roulette);
	CreateLibSingleton(StringScreenMgr);
	CreateLibSingleton(HongHaoManager);
	CreateLibSingleton(PubDataManager);

	// 注册协议及脚本
	Log.Debug("ScriptEngine", "Load World Server Script Modules");
	register_custom_world_process(mgr);
	register_custom_world_create_ai(mgr);
}


#endif // _MY_WORLD_MAIN_H

/**
 * @锟斤拷锟斤拷时锟斤拷 2011-09-07
 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
 * @锟斤拷锟斤拷锟斤拷 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷志
 */
#ifndef _PG_LOGDATAMANAGER_H
#define _PG_LOGDATAMANAGER_H

#include "Common.h"
#include "Resource.h"
#include "Singleton.h"
#include "String.h"
#include "PGEnum.h"
#include "Game.h"
#include "LogDataManager.h"
#include "PGPubStruct.h"
class UserPtr;
class CharPtr;
class ChannelPtr;
struct Position;
struct game_logs_tag_;
typedef struct game_logs_tag_ GameLog;

class PGLogDataManager : public LogDataManager, public Singleton<PGLogDataManager>
{
public:
	PGLogDataManager();
	~PGLogDataManager();
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-09-13
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷录锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pUser 锟斤拷录锟斤拷锟矫伙拷
	 *  @serverId 锟斤拷录锟斤拷锟斤拷锟斤拷id
	 */
	void LoginLog(UserPtr & pUser, const uint32 serverId);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-09-13
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟剿筹拷锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pUser 锟剿筹拷锟斤拷锟矫伙拷
	 *  @serverId 锟斤拷锟斤拷锟斤拷id
	 */
	void LogoutLog(UserPtr & pUser, const uint32 serverId);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-04-10
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷?
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @subType 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟�?
	 *  @pChn 锟斤拷锟斤拷
	 */
	void AboutTableLog(CharPtr & pChr, const uint32 subType, ChannelPtr &pChn);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-04-11
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 一锟斤拷锟斤拷戏锟斤拷锟斤拷锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pChn 锟斤拷锟斤拷
	 *  @resultType 锟斤拷锟斤拷锟斤拷锟�?
	 *  @bankerUserId 庄锟斤拷
	 *  @chips 押注
	 *  @pump 锟斤拷水
	 */
	void TableTurnOverLog(ChannelPtr &pChn, const uint32 resultType, const uint32 bankerUserId, const uint32 chips, const uint32 pump);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-10-20
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟斤拷锟斤拷品锟斤拷息锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @SourceType 锟斤拷源锟斤拷锟斤拷
	 *  @selfModelId 锟斤拷锟斤拷锟铰碉拷锟斤拷品锟斤拷模式ID
	 *  @Operate 锟斤拷锟斤拷锟斤拷品锟斤拷锟斤拷锟斤拷1锟斤拷锟斤拷锟斤拷2锟斤拷锟斤拷锟斤拷3锟斤拷删锟斤拷
	 *  @number 锟斤拷锟斤拷锟斤拷锟斤拷
	 *  @fromId 锟斤拷锟斤拷锟斤拷锟矫ｏ拷0锟斤拷锟节斤拷锟斤拷时锟斤拷锟斤拷示锟教城ｏ拷
	 */
	void UpdateItemsInfoLog(CharPtr & pChr, const uint32 sourceType, const uint32 selfModelId, const UpdateItemNumberType operate, 
							const uint32 number = 0, const uint32 fromId = 0, const uint32 data7 = 0, const uint32 data8 = 0);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-10-20
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟捷达拷锟狡斤拷锟斤拷锟斤拷锟斤拷锟狡凤拷锟较拷锟斤拷?
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @selfModelId 锟斤拷锟斤拷锟铰碉拷锟斤拷品锟斤拷模式ID
	 *  @Operate 锟斤拷锟斤拷锟斤拷品锟斤拷锟斤拷锟斤拷1锟斤拷锟斤拷锟斤拷2锟斤拷锟斤拷锟斤拷3锟斤拷删锟斤拷
	 *  @number 锟斤拷锟斤拷锟斤拷锟斤拷
	 *  @pChn 锟斤拷锟斤拷
	 *  @chkType 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷品锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
	 */
	void UpdateItemsByGameResultLog(CharPtr & pChr, const uint32 selfModelId, const UpdateItemNumberType operate, const uint32 number, ChannelPtr &pChn, const UpdateOnTableType chkType);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-12-06
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷值锟斤拷锟斤拷锟斤拷品锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @selfModelId 锟斤拷品模式ID
	 *  @number 锟斤拷锟斤拷
	 */
	void UpdateItemsByUserBuyLog(CharPtr & pChr, const uint32 selfModelId, const uint32 number);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-02-27
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷录通锟斤拷锟斤拷锟截癸拷锟斤拷锟斤拷品锟斤拷锟斤拷锟斤拷?
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @goodsDesc 锟斤拷锟斤拷锟斤拷品锟缴凤拷锟斤拷锟斤拷
	 *  @cost 支锟斤拷锟杰讹拷
	 */
	void GatwayBuyResultLog(CharPtr & pChr, String &goodsDesc, const uint32 cost);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-04-11
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷?
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @subType 锟斤拷锟斤拷锟酵ｏ拷1锟斤拷锟斤拷 2锟斤拷取锟斤拷
	 *  @modelId 锟斤拷品模式ID
	 *  @number 锟斤拷锟斤拷
	 */
	void AboutBankLog(CharPtr & pChr, const BankType subType, const uint32 modelId, const uint32 number);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-04-24
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 系统锟斤拷水锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @sourceType 锟斤拷源锟斤拷锟斤拷
	 *  @pump 锟斤拷税
	 */
	void PumpLog(const uint32 sourceType, const uint32 pump);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-04-24
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷拳
	 * @锟斤拷锟斤拷
	 *  @serial1 锟矫伙拷1ID
	 *  @serial2 锟矫伙拷2ID
	 *  @val1 锟矫伙拷1锟斤拷拳
	 *  @val2 锟矫伙拷2锟斤拷拳
	 *  @ante 锟斤拷锟�?
	 *  @winner 胜锟斤拷锟斤拷ID
	 */
	void SMGameMorraLog(const uint32 serial1, const uint32 serial2, const uint32 val1, const uint32 val2, const uint32 ante, const uint32 winner);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2012-12-11
	 * @锟斤拷锟斤拷锟斤拷 StarX
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷水锟斤拷志
	 *	@pChr 锟斤拷色Ptr	src_id
	 * 	@fromId	锟斤拷源	sub_type
	 * 	@num	锟斤拷锟斤拷	data1
	 *  @data2  锟斤拷锟斤拷1	data2
	 *  @data3  锟斤拷锟斤拷2	data3
	 */
	void AddGamePumpLog(CharPtr & pChr,const uint32 & fromId, const uint32 & num, const uint32 & data2 = 0, const uint32 & data3 = 0);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-11-04
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟侥憋拷锟斤拷锟斤拷锟斤拷锟斤拷
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @sourceType 锟斤拷源锟斤拷锟斤拷
	 *  @figuralId 锟斤拷锟斤拷图锟斤拷ID
	 *  @fromId 锟斤拷锟斤拷锟斤拷锟矫ｏ拷0锟斤拷锟节斤拷锟斤拷时锟斤拷锟斤拷示锟教城ｏ拷
	 */
	void ChangeFiguralLog(CharPtr & pChr, const SourceType sourceType, const uint32 figuralId, const uint32 fromId = 0);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-12-16
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷录锟揭伙拷实锟斤拷锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @userId 锟矫伙拷ID
	 *  @goodsId 锟斤拷品ID
	 *  @name 锟斤拷品锟斤拷锟斤拷
	 *  @number 锟揭伙拷锟斤拷锟斤拷
	 *  @cost 锟斤拷锟窖诧拷券
	 *  @billId 锟斤拷锟斤拷
	 */
	void ChangePracticalityLog(const uint32 userId, const uint32 goodsId, const String &name, const uint32 number, const uint32 cost, const char *billId);
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-10-20
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 元锟斤拷锟斤拷锟斤拷锟斤拷志
	 * @锟斤拷锟斤拷
	 *  @pChr 锟斤拷色
	 *  @sourceType 锟斤拷源锟斤拷锟斤拷
	 *  @Operate 锟斤拷锟铰诧拷锟斤拷锟斤拷1锟斤拷锟斤拷锟斤拷2锟斤拷锟斤拷锟斤拷3锟斤拷删锟斤拷
	 *  @number 锟斤拷锟斤拷锟斤拷锟斤拷
	 *  @fromId 锟斤拷锟斤拷锟斤拷锟矫ｏ拷0锟斤拷锟节斤拷锟斤拷时锟斤拷锟斤拷示锟教城ｏ拷
	 */
	void UpdateGoldsLog(CharPtr & pChr, const SourceType sourceType, const UpdateItemNumberType operate, const uint32 number, const uint32 fromId = 0);
	
	/**
	 * @锟斤拷锟斤拷时锟斤拷 	2018-02-22
	 * @锟斤拷锟斤拷锟斤拷 		Roach
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 	锟斤拷录锟斤拷戏锟斤拷赢锟斤拷锟斤拷锟斤拷赢锟斤拷录
	 * @锟斤拷锟斤拷
	 *	@pChn		锟斤拷锟斤拷锟斤拷锟斤拷
	 * 	@vecPos		锟斤拷赢锟斤拷位
	 */
	void AreaWinloseLog(ChannelPtr &pChn, std::vector<TablePlayer> &vecPos);
	
	// 锟斤拷锟斤拷锟街撅拷锟斤拷锟皆拷锟�?
	String GetLogCause(const uint32 &source);
private:
	/**
	 * @锟斤拷锟斤拷时锟斤拷 2011-10-10
	 * @锟斤拷锟斤拷锟斤拷 锟斤拷志锟斤拷
	 * @锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟街凤拷锟斤拷转锟斤拷锟斤拷UTF8锟斤拷锟斤拷锟街凤拷锟斤拷
	 * @锟斤拷锟斤拷
	 *  @str 源锟街凤拷锟斤拷
	 */
	// String GetUTF8String(String &str);
	
	// bool AppendUserLog(GameLog *pLog, UserPtr &pUser);
	// bool AppendUserLog(GameLog *pLog, CharPtr &pChr);
};

// #define sLogDataMgr PGLogDataManager::getSingleton()

#endif // _PG_LOGDATAMANAGER_H

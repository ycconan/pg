#ifndef _PG_PROTOCOLDEALENUMS_H
#define _PG_PROTOCOLDEALENUMS_H

#include "ProtocolDealEnums.h"

// 閻ц缍嶉悩鑸碘偓锟�
// enum LoginState
// {
	// en_LS_Ok = 1, // 閻ц缍嶉幋鎰�?
	// en_LS_InvalidUserOrSession = 2, // 閺冪姵鏅ラ悽銊﹀煕閸氬秵鍨╯ession
	// en_LS_Full = 4, // 閺堝秴濮熼崳銊ゆ眽閺佹澘鍑″⿰锟�
	// en_LS_CreateNick = 5, // 闂団偓鐟曚礁鍨卞鐑樻█缁夛�?
// };
// 鏉╂稑鍙嗛悧灞绢�?
enum JoinTableResult
{
	en_JoinTableResult_Faild = 0, // 婢惰精瑙�?
	en_JoinTableResult_Ok = 1, // 鏉╂稑鍙嗛幋鎰�?
	en_JoinTableResult_Full = 2, // 閹村潡妫挎禍鐑樻殶瀹稿弶寮�?
	en_JoinTableResult_BeHardUp = 3, // 濮濓絽婀崚顐ゆ畱閻楀本顢�?
	en_JoinTableResult_ChipsScanty = 4, // 闁藉彉绗夋径鐔荤箻閸忋儴顕氶幋鍧楁？
	en_JoinTableResult_ChipsGreater = 5, // 闁借精绉撮崙杞扮啊閹村潡妫块梽鎰煑閺堚偓婢堆冣偓锟�
};
// ????
enum JoinRoomResult
{
	en_JoinRoom_Faild = 0,		  // ??
	en_JoinRoom_Ok = 1,			  // ????
	en_JoinRoom_Full = 2,		  // ??????
	en_JoinRoom_InOtherRoom = 3,  // ??????
	en_JoinRoom_ChipsScanty = 4,  // ????????
	en_JoinRoom_ChipsGreater = 5, // ???????????
	en_JoinRoom_LevelLittle = 6,  // ????
};
// ????
enum LeaveRoomResult
{
	en_LeaveRoom_Faild = 0,		  // ??
	en_LeaveRoom_Ok = 1,		  // ??
	en_LeaveRoom_Timeout = 2,	 // ??
	en_LeaveRoom_ChipsScanty = 3, // ???
	en_LeaveRoom_NotMatch = 4,	// ??????
};
// 缁傝绱戦悧灞绢�?
enum LeaveTable
{
	en_LeaveTable_Faild = 0, // 婢惰精瑙�?
	en_LeaveTable_Ok = 1, // 閹存劕濮�?
	en_LeaveTable_NotExisted = 2, // 瀹歌尙顬囧鈧�
	en_LeaveTable_Chipin = 3, // 瀹稿弶濮囧▔顭掔礉娑撳秷鍏樼粋璇茬磻
	en_LeaveTable_Scanty = 4, // 闁藉彉绗夋径鐕傜礉鐞氼偉娑崙楦款嚉閹村潡妫块敍鍫熷灇閸旂喓顬囧鈧敍锟�
};
// 閻㈠疇顕稉濠傜盀缂佹挻鐏�?
enum ApplyBankerResult
{
	en_ApplyBankerRt_Faild = 0, // 婢惰精瑙�?
	en_ApplyBankerRt_Ok = 1, // 閹存劕濮�?
	en_ApplyBankerRt_Exsited = 2, // 瀹歌尙绮￠崷銊╂Е閸掓ぞ鑵�?
	en_ApplyBankerRt_NotInChips = 3, // 閼奉亜鐣炬稊澶岊劜閻椒绗夐崷銊ょ瑐鎼村嫰鍣炬０婵嗗隘闂傦拷
	en_ApplyBankerRt_ChipsScanty = 4, // 缁涘湱鐖滅亸蹇庣艾閻㈠疇顕粵鍦垳
};
// 閹舵洘鏁炵紒鎾寸�?
enum ChipsInResult
{
	en_ChipsInResult_Faild = 0, // 婢惰精瑙�?
	en_ChipsInResult_Ok = 1, // 閹存劕濮�?
	en_ChipsInResult_ChipsScanty = 2, // 缁涘湱鐖滄稉宥咁�?
	en_ChipsInResult_NotChipTime = 3, // 娑撳秵妲搁幎鏇熸暈閺冨爼妫�
	en_ChipsInResult_BankerChipsScanty = 4, // 鎼村嫬顔嶇粵鍦垳娑撳秴顧�
	en_ChipsInResult_OverLimit = 5, // 缁涘湱鐖滃鑼舵彧閸掔増婀版潪顔荤瑓濞夈劋绗傞梽锟�?
};
// 閼卞﹤銇夌紒鎾寸�?
enum ChatResult
{
	en_ChatResult_Faild = 0, // 婢惰精瑙�?
	en_ChatResult_Ok = 1, // 閹存劕濮�?
	en_ChatResult_MoneyScanty = 2, // 闁藉彉绗夋径锟�
	en_ChatResult_StopTalk = 3, // 缁備浇鈻�?
	en_ChatResult_Offline = 4, // 娑撳秴婀痪锟�
};
// 妫板棗褰囩粈鐓庡瘶缂佹挻鐏�
enum GetGiftResult
{
	en_GetGift_Faild = 0, // 婢惰精瑙�?
	en_GetGift_Ok = 1, // 閹存劕濮�?
	en_GetGift_CountOver = 2, // 妫板棗褰囧▎鈩冩殶瀹歌尙鏁ょ€癸拷
};
// 鐠х娀鈧胶銇滈悧鈺冪波閺嬶拷
enum GivePresentResult
{
	en_GivePresent_Faild = 0, // 婢惰精瑙�?
	en_GivePresent_Ok = 1, // 閹存劕濮�?
	en_GivePresent_Scanty = 2, // 闁藉彉绗夋径锟�
};
// // 閹垮秳缍旀禍宀€楠囩€靛棛鐖滅紒鎾寸亯
// enum Operator2PwdResult
// {
	// en_Operator2Pwd_Faild = 0, // 婢惰精瑙�?
	// en_Operator2Pwd_Success = 1, // 閹存劕濮�?
	// en_Operator2Pwd_OldPwdError = 2, // 鐎靛棛鐖滈柨娆掝�?
	// en_Operator2Pwd_Locked = 3 // 鐎靛棛鐖滈柨浣哥�?
// };
// // 闁炬儼顢戠€涙ɑ顑欑紒鎾寸�?
// enum BankCoinsResult
// {
	// en_BankCoins_Faild = 0, // 婢惰精瑙�?
	// en_BankCoins_Ok = 1, // 閹存劕濮�?
	// en_BankCoins_Scanty = 2, // 闁藉彉绗夋径锟�
// };
// 闁炬儼顢戦崣鏍儥缂佹挻鐏�
// enum GetBankCoinsResult
// {
	// en_GetBankCoins_Faild = 0, // 婢惰精瑙�?
	// en_GetBankCoins_Ok = 1, // 閹存劕濮�?
	// // (2,3娑撳搫鐣ㄩ崗銊ョ槕閻胶绮ㄩ弸婊愮礉閸欏倽顫哋perator2PwdResult)
	// en_GetBankCoins_Overflow = 4, // 鐡掑懓绻冮幖鍝勭敨娑撳﹪妾�
	// en_GetBankCoins_Scanty = 5, // 鐎涙ɑ顑欐稉宥咁檮閸欙�?
// };
// 闁氨鏁ょ紒鎾寸�?
enum UpdateDiamond
{
	en_Nomal_Faild = 0, // 婢惰精瑙�?
	en_Nomal_Ok = 1, // 閹存劕濮�?
};
// 閻溾晛顔嶉崝鐘插弳閹存牜顬囧鈧稉濠傜盀閸掓銆�?
enum JoinOrLeaveBankerList
{
	en_JoinLeave_Join = 1,
	en_JoinLeave_Leave = 2
};


// // 鐠愵厺鎷遍崯鍡楁�?
// enum BuyGoodsResult
// {
	// en_BGR_Unknown = 0, // 閺堫亞鐓￠柨娆掝�?
	// en_BGR_Successed = 1, // 閹存劕濮�?
	// en_BGR_BeHardUp = 2, // 闁藉彉绗夋径锟�
	// en_BGR_PlayingGame = 3, // 濮濓絽婀潻娑滎攽濞撳憡鍨�
	// en_BGR_Pwd2Error = 4, // 娴滃瞼楠囩€靛棛鐖滄稉宥嗩劀绾�?
	// en_BGR_Pwd2Locked = 5 // 娴滃瞼楠囩€靛棛鐖滅悮顐︽敚鐎癸�?
// };


// // 閼惧嘲褰囬柇顔绘閸掓銆冮悩鑸碘偓锟�
// enum MailListStatus
// {
	// en_MailListStatus_All = 1, // 閹碘偓閺堬拷
	// en_MailListStatus_Readed = 2, // 瀹歌尪顕�?
	// en_MailListStatus_Unread = 3 //	閺堫亣顕�?
// };
// // 閹垮秳缍旈柇顔绘�?
// enum OperatorMail
// {
	// en_OperatorMail_Read = 1, // 鐠囪褰�?
	// en_OperatorMail_Delete = 2, // 閸掔娀娅�
// };
// // 閹垮秳缍旈柇顔绘缂佹挻鐏�
// enum OperatorMailResult
// {
	// en_OperatorMailResult_Successed = 1, // 閹存劕濮�?
	// en_OperatorMailResult_NoMail = 2, // 濞屸剝婀侀幍鎯у煂闁喕娆�
	// en_OperatorMailResult_UnkownFaild = 3, // 閸忔湹绮径杈Е
// };
// // 閺囧瓨鏁奸弰鐢敌炵紒鎾寸�?
// enum PGRenameResult
// {
	// en_RenameResult_Successed = 1, // 閹存劕濮�?
	// en_RenameResult_OutRule = 2, // 閺勭數袨娑撳秶顑侀崥鍫ｎ潐閸掞�?
	// en_RenameResult_Existed = 3, // 閺勭數袨瀹告彃鐡ㄩ崷锟�
// };

// // 閸忔垶宕查崗鎴炲床閸楋紕绮ㄩ弸锟�?
// enum ExchangeResult
// {
	// en_ExchangeResult_Success = 1, // 閹存劕濮�?
	// en_ExchangeResult_PlayerExchanged = 2, // 閻溾晛顔嶅鑼病閸忔垶宕叉潻锟�?
	// en_ExchangeResult_TicketExchanged = 3, // 閸忔垶宕查崡鈥冲嚒缂佸繐鍘幑銏ｇ箖
	// en_ExchangeResult_TicketNotExisited = 4, // 閸忔垶宕查崡鈥茬瑝鐎涙ê婀�?
	// en_ExchangeResult_TicketOverdue = 5, // 閸忔垶宕查崡陇绻冮張锟�
// };

// enum enCheckCardResult
// {
	// en_CardCheck_None = 0,		//閺堫亞鐓￠柨娆掝�?
	// en_CardCheck_CanUse = 1,	//閸欘垯濞囬悽锟�
	// en_CardCheck_YanZheng = 2,	//瀹告煡鐛欑拠锟�
	// en_CardCheck_Used = 3,		//鐠囥儱宕卞韫▏閻拷
	// en_CardCheck_TimeOver = 4,	//鏉╁洦婀�?
	// en_CardCheck_NotExist = 5,	//娑撳秴鐡ㄩ崷锟�
	// en_CardCheck_ErrorCode = 6,	//闁挎瑨顕ゆ宀冪槈閻�?
	// en_CardCheck_Lock = 7,	//闁夸礁鐣鹃敍鍫ユ晩鐠囶垱顐奸弫鎷岀箖婢舵熬绱�
// };



#endif // PROTOCOLDEALENUMS_H

#ifndef _MY_INTERFACE_MAIN_H
#define _MY_INTERFACE_MAIN_H

#include "Common.h"
#include "Singleton.h"
#include "Tools.h"
#include "PGProtocolsManager.h"
#include "PGPublicLogic.h"
#include "PGLogDataManager.h"
#include "PGXmlDataManager.h"
#include "PGHallManager.h"
#include "PGRuleManager.h"

#include "VipCardManager.h"
#include "CenterBankManager.h"
#include "DataTransferManager.h"
#include "GameGoodsManager.h"
#include "NoticeManager.h"
#include "GoldCoronal.h"
#include "PubDataManager.h"
// #include "Roulette.h"

initialiseSingleton(Tools);
initialiseSingleton(PublicLogic);
initialiseSingleton(PGProtocolsManager);
initialiseSingleton(PGLogDataManager);
initialiseSingleton(GameGoodsManager);
initialiseSingleton(PGXmlDataManager);
initialiseSingleton(VipCardManager);
initialiseSingleton(HallManager);
initialiseSingleton(NoticeManager);
initialiseSingleton(RuleManager);
initialiseSingleton(CenterBankManager);
initialiseSingleton(DataTransferManager);
initialiseSingleton(GoldCoronal);
initialiseSingleton(PubDataManager);
initialiseSingleton(HongHaoManager);
// initialiseSingleton(Roulette);

#define CreateLibSingleton( type ) \
  static  type the##type

#include "PGWorldAIInterface.h"
#include "NGLog.h"

void myinitlibs(void)
{
	// 创建单例
	CreateLibSingleton(Tools);
 	CreateLibSingleton(PublicLogic);
 	CreateLibSingleton(PGProtocolsManager);
 	CreateLibSingleton(PGLogDataManager);
 	CreateLibSingleton(GameGoodsManager);
 	CreateLibSingleton(PGXmlDataManager);
	CreateLibSingleton(VipCardManager);
 	CreateLibSingleton(HallManager);
 	CreateLibSingleton(NoticeManager);
 	CreateLibSingleton(RuleManager);
	CreateLibSingleton(CenterBankManager);
	CreateLibSingleton(DataTransferManager);
	CreateLibSingleton(GoldCoronal);
	CreateLibSingleton(PubDataManager);
	CreateLibSingleton(HongHaoManager);
	// CreateLibSingleton(Roulette);
	return ;
}


#endif // _MY_INTERFACE_MAIN_H

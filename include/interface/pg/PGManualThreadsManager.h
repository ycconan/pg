#ifndef _PG_MANUALTHREADMANAGER_H
#define _PG_MANUALTHREADMANAGER_H

#include "Common.h"
#include "Resource.h"
#include "Singleton.h"
#include "String.h"
#include "Towns.h"
#include "Threading/ThreadStarter.h"
#include "Threading/QueueThreadPool.h"
#include "WSSocket.h"

//机器人生成线程
class PGSpawnThread:public ThreadBase
{
protected:
	uint32  mNum;
	uint32 mTownID;		
	
public:
	PGSpawnThread(const uint32 & nTownID, const uint32 & mNum);	
    bool  run();
	void 	Spawn(const uint32 & num);
};
//--------------------------------------------------
// 登陆信息结构体
struct PGUserAuthenticInfo
{
	uint8 isCenter;
	WSSocketPtr pSocket;
	String session; 
};

//玩家登陆队列分配线程
class PGAuthenticationQueueThread : public ThreadBase 
{
public:
	PGAuthenticationQueueThread(void);
	bool run(void);
	// 登录队列
	static LockedQueue<PGUserAuthenticInfo> m_authenQueue;
	// 当前正在处理登陆数
	static Threading::AtomicCounter m_queueCount;
protected:
	CQueueThreadPool< PGUserAuthenticInfo > m_QueueCreatePool; 
};

class PGAuthenQueueThreadRunable : public ThreadRunableBase
{
public:
	bool run(void *p);
protected:
	
};
//-------------------------------------------------------
struct QueueTop;

// 加载玩家排行信息
class LoadPlayerTop : public ThreadBase
{
public:
	LoadPlayerTop(const uint32 topType, const uint32 gzId);
	bool run();
private:
	uint32 m_topType;
	uint32 m_gzId;
	std::list<QueueTop> m_lstQT;
};


#endif // _PG_MANUALTHREADMANAGER_H

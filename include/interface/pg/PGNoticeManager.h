#ifndef _PG_NOTICEMANAGER_H
#define _PG_NOTICEMANAGER_H

#include "Common.h"
#include "String.h"
//#include "Mutex.h"

enum NoticeLoop
{
	en_NoticeLoop_None = 0, // 不循环
	en_NoticeLoop_Everyday = 1 // 每天
};

struct Notice
{
	String msg;		// 公告内容
	uint32 msgType;		// 播放类型
	uint32 gzId;	// 分区ID
	uint32 interval;	// 间隔时间（单位秒）
	uint32 startDate;	// 开始日期
	uint32 finishDate;	// 结束日期
	uint32 beginTime;	// 开始时间（秒）
	uint32 endTime;		// 结束时间（秒）
	uint32 lastTime;	// 最近发布时间
	
	Notice() : msgType(1), gzId(0), interval(0), startDate(0), finishDate(0), beginTime(0), endTime(0), lastTime(0){}
	
};

#define WGS_NOTICEMGR_INNER_MUTEX_NAME notice_inner_mutex
#define WGS_NOTICEMGR_INNER_MUTEX mutable boost::mutex WGS_NOTICEMGR_INNER_MUTEX_NAME;
#define WGS_NOTICEMGR_INNER_LOCK		boost::lock_guard<boost::mutex> wgsNoticeMgrInnerMutexLock(WGS_NOTICEMGR_INNER_MUTEX_NAME);

class NoticeManager : public Singleton<NoticeManager>
{
	WGS_NOTICEMGR_INNER_MUTEX
public:
	NoticeManager();
	void Init();
	/**
	 * 添加公告
	 * 参数
	 * @msg 公告内容
	 * @interval 间隔时间（秒）
	 */
	void AddMsgToQueue(String &msg, const uint32 msgType, const uint32 gzId = 0, const uint32 interval = 0, const uint32 startDate = 0, const uint32 finishDate = 0, const uint32 beginTime = 0, const uint32 endTime = 0); 
	void AddMsgToQueue(const char* cmsg, const uint32 msgType, const uint32 gzId = 0, const uint32 interval = 0, const uint32 startDate = 0, const uint32 finishDate = 0, const uint32 beginTime = 0, const uint32 endTime = 0);
	void AddMsgToQueue(const Notice &nt);
	void Update(const uint32 t);
	void Clear();
private:
	// 广播列表
	std::list<Notice> m_lstNotice;
};

#define   sNoticeMgr   NoticeManager::getSingleton()

#endif // _PG_NOTICEMANAGER_H

#ifndef _PG_HALLMANAGER_H
#define _PG_HALLMANAGER_H

#include "Common.h"
#include "Singleton.h"
#include "String.h"

class WSSocketPtr;
class CharPtr;
class UserPtr;
class TownPtr;
struct BigRoom;
struct QueueTop;
class WorldPacket;

// 排行锁
#define WGS_HALL_INNER_MUTEX_NAME hall_inner_mutex
#define WGS_HALL_INNER_MUTEX mutable boost::mutex WGS_HALL_INNER_MUTEX_NAME;
#define WGS_HALL_INNER_LOCK		boost::lock_guard<boost::mutex> wgsHallInnerMutexLock(WGS_HALL_INNER_MUTEX_NAME);
// 改名锁
#define WGS_RENAME_INNER_MUTEX_NAME hall_rename_mutex
#define WGS_RENAME_INNER_MUTEX mutable boost::mutex WGS_RENAME_INNER_MUTEX_NAME;
#define WGS_RENAME_INNER_LOCK		boost::lock_guard<boost::mutex> wgsHallRenameMutexLock(WGS_RENAME_INNER_MUTEX_NAME);
// 账号登录锁
#define WGS_LOGIN_INNER_MUTEX_NAME hall_login_mutex
#define WGS_LOGIN_INNER_MUTEX mutable boost::mutex WGS_LOGIN_INNER_MUTEX_NAME;
#define WGS_LOGIN_INNER_LOCK		boost::lock_guard<boost::mutex> wgsHallLoginMutexLock(WGS_LOGIN_INNER_MUTEX_NAME);

// 大厅管理
class HallManager : public Singleton<HallManager>
{
	WGS_HALL_INNER_MUTEX
	WGS_RENAME_INNER_MUTEX
	WGS_LOGIN_INNER_MUTEX
public:
	HallManager();
	~HallManager();
	void Init();
	// 主动下发大厅相关信息
	void SendHallUIInfo(WSSocketPtr &socket, CharPtr &pChr);
	// 获取排名
	void GetTopQueue(WSSocketPtr &socket, const uint32 gzId, const uint32 topType);
	// 更新玩家排行列表
	void UpdatePlayerTop(const uint32 gzId, const uint32 topType, std::list<QueueTop> &lstQT);
	// 时间更新
	void Update();
	// 登录服务器
	void LoginServer(WSSocketPtr &socket, String &strSession, const uint32 isCenter);
	// 登录成功后
	void AfterLogin(WSSocketPtr &socket, UserPtr &pUser, CharPtr &pChr);
	void AfterLoginWorldOk(WSSocketPtr &socket, UserPtr &pUser, CharPtr &pChr);
	bool JoinRoom(CharPtr &pChr, TownPtr &pTown);
	// 加入牌桌
	bool JoinTable(WSSocketPtr &socket, CharPtr &pChr, const uint32 roomId, const uint32 tableId);
	// 更改昵称
	void Rename(WSSocketPtr &socket, WorldPacket &packet);
private:
	// 执行更新排行
	void UpdatePlayerQueueTop();
private:
	typedef std::list<QueueTop> TopList;
	std::map<uint32, std::map<uint32, TopList> > m_mapTops;
	bool m_bByGzid; // 排行是否区分分区
};

#define   sHallMgr   HallManager::getSingleton()

#endif // _PG_HALLMANAGER_H

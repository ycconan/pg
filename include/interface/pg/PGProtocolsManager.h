/**
 *	协议封包
 */
#ifndef _PG_PROTOCOLMGR_H
#define _PG_PROTOCOLMGR_H

#include "Common.h"
#include "String.h"
#include "Resource.h"
#include "WorldPacket.h"
#include "PGEnum.h"
#include "ProtocolsManager.h"
#include "PGPubStruct.h"
using std::vector;

class CharPtr;
class ItemPtr;
class ChannelPtr;
class Channel;
class UserPtr;
class TownPtr;
namespace AIScript
{
struct Banker;
}
struct PlayerPos;
struct QueueTop;

struct game_mail_tag_;
typedef game_mail_tag_ GameMail;
struct game_server_tag_;
typedef game_server_tag_ GameServer;
struct game_bank_tag_;
typedef game_bank_tag_ GameBank;
struct game_logs_tag_;
typedef game_logs_tag_ GameLog;
struct game_march_tag_;
typedef game_march_tag_ GameMarch;
struct GZIDInfo;
struct game_exchange_tag_;
typedef game_exchange_tag_ GameExchange;


class PGProtocolsManager : public ProtocolsManager, public Singleton<PGProtocolsManager>
{
  public:
	PGProtocolsManager(void);
	~PGProtocolsManager(void); 
	bool CreateGoHallOrTablePacket(WorldPacket *packet, const uint8 where = 0);
	//棋牌圈玩家信息列表
	bool CreatePlayerListPacket(WorldPacket *packet, std::vector<TablePlayer> vChars);
	//玩家角色信息包
	bool CreateCharacterInfoPacket(WorldPacket *packet, CharPtr &pChr);
	//牌桌状态更新
	bool CreateTableStateUpdatePacket(WorldPacket * packet, const uint8 channelState, const uint32 seconds);
	//广播骰子数
	bool CreateBroadcastDiceResultPacket(WorldPacket *packet, const uint32 &dices1, const uint32 &dices2,const uint8 &speakpos , const uint32 &playerId);
	//发送已出牌列表
	bool CreateGetOutcardsPacket(WorldPacket *packet, std::vector<TablePlayer> &lstoutcards, const bool bShow);
	//下注结果
	bool CreateChipInResultPacket(WorldPacket *packet, const uint8 &chiprsult, std::map<uint32, uint32> &numchips, const uint32 &num, const uint32 &surscore, const uint8 &seatpos);
	//当前玩家选择切牌位置
	bool CreateBroadCutCardsPosPacket(WorldPacket *packet, const uint8 pos,const uint32 cutcardspos);
	//下一玩家切牌信息
	bool CreateBroadCutResultPacket(WorldPacket *packet, TablePlayer nextplayer, const uint8 pos, const uint32 cutplayernum,const uint32 cuttime, const bool bShow);
	//庄家看牌
	bool CreateBankerLookCardsPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos, const bool bShow = true);
	//玩家看牌
	bool CreatePlayerLookCardTypePacket(WorldPacket *packet, const uint8 result, TablePlayer tp);
	//玩家摆牌最终结果
	bool CreatePlayerMoveCardPacket(WorldPacket *packet, TablePlayer tp);
	//玩家摆牌操作结果
	bool CreatePlayerMoveCardResultPacket(WorldPacket *packet, const uint8 result);
	//通知玩家抢庄
	bool CreateNoticeGetBankerPacket(WorldPacket *packet, const uint8 &mode);
	//广播玩家抢庄选择
	bool CreateGetBankerResultPacket(WorldPacket *packet, const uint8 &pos,const uint8 &bwantbanker);
	//玩家抢庄结果
	bool CreateSetBankerPosPacket(WorldPacket *packet, const uint8 &mode);
	//询问是否连庄
	bool CreateAskIsContinueBankerPacket(WorldPacket *packet, const uint8 seconds);
	//成为庄家扣除底分
	bool CreateSetBankerDelScorePacket(WorldPacket *packet, const uint8 &bankerpos,const int &basescore);
	//每局结算
	bool CreateRoundPGCalcPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos, const uint8 &nBankerPos, const uint8 &rType, const int &basescore, const bool bShow);
	//最终结算
	bool CreateEndPGCalcPacket(WorldPacket *packet, std::vector<TablePlayer> &vecPos,const bool bShow);
	//下庄原因
	bool CreateAbandonBankerPacket(WorldPacket * packet, const uint8 status);
	//小结算确认
	bool CreateAskIsRoundPGCalcPacket(WorldPacket *packet, const uint8 &Ask);
	//断线重连确认开牌
	bool CreateReturnPacket(WorldPacket *packet);
};
#endif

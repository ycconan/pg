#ifndef _PG_ENUM_H
#define _PG_ENUM_H

#include "GameDataEnum.h"
#include "PGProtocolDealEnums.h"

// 事件
enum ResourceEventEx
{
	en_ResourceEvent_Login = 1001, // 登录事件
	en_ResourceEvent_GameOver = 1002, // 游戏结束
	en_ResourceEvent_GivePresent = 1003, // 赠送礼物
	en_ResourceEvent_BankerUpdate = 1004, // 庄家数据更新（上庄和连庄）
	en_ResourceEvent_CoinsUpdate = 1005,	// 铜钱数据更新
	en_ResourceEvent_GetEffort = 1006,	// 获得成就
	en_ResourceEvent_DiamondUpdate = 1007,	// 黄钻更新
};
/*****************************************日志相关**************************************/
// 来源类型
enum PGSourceType
{
	en_ST_Table = 1, // 来自牌桌（二八杠）
	en_ST_GiftBag = 3, // 来自礼包
	en_ST_Bugle = 101, // 购买聊天喇叭
	en_ST_Present = 102, // 赠送礼物
	en_ST_CombItem = 103, // 合并物品
};
// 牌桌上物品变动类型
enum UpdateOnTableType
{
	en_UpdateOnTableType_Play = 1,	// 打牌输赢
	en_UpdateOnTableType_Cost = 2,	// 对局费
};
/*****************************************日志子类型**************************************/

// 牌桌相关类型
enum PGTableLogType
{
	// en_TLT_Enter = 1, // 进入牌桌
	// en_TLT_Offline = 2, // 中途掉线
	// en_TLT_Return = 3, // 返回牌桌
	en_TLT_Leave = 4, // 离开牌桌
	// en_TLT_Complete = 5, // 玩家完成游戏
	en_TLT_TurnOver = 6, // 一轮结束
};
// 银行相关
enum BankType
{
	en_BankType_Save = 1,	// 存款
	en_BankType_Get = 2,	// 取款
};


// 玩家数据排行
enum TopQuequeType
{
	en_TopQueque_Coin = 1, // 铜钱
	en_TopQueque_Level = 2, // 等级
	en_TopQueque_FullWin = 3, // 通吃
	en_TopQueque_Flower = 4, // 花王
	en_TopQueque_Egg = 5, // 蛋皇
	en_TopQueque_Aubergine = 6, // 茄圣
	en_TopQueque_BestScore = 7, // 单局最高
};
// 发起投票结果
enum enSendVote
{
	enSendVote_Default = 0,		// 失败
	enSendVote_Sucess = 1,		// 成功
	enSendVote_Voting = 2,		// 正在投票中
	enSendVote_ErrorPlayer = 3, // 错误的玩家
};
// 情景ID
enum SceneId
{
	en_SceneId_LoginOver = 1, // 登录完成
	en_SceneId_TableLoaded = 2, // 牌桌加载完成
};

// 聊天类型
enum ChatType
{
	en_ChatType_Server = 1,	// 全服通告
	en_ChatType_Room = 2,	// 房间通告
	en_ChatType_Normal = 3,	// 一般
	en_ChatType_P2P = 4,	// 私聊
};
// 礼包类型(公共定义)
enum GiftType
{
	en_GiftType_Login = 1, // 登录礼包
	en_GiftType_Subsidy = 2, // 救济礼包
	en_GiftType_Online = 14, // 在线礼包
	en_GiftType_NewDiamond = 11, // 黄钻新手礼包
	en_GiftType_LoginDiamond = 13, // 黄钻每日礼包
	en_GiftType_LevelUp = 3, // 升级奖励
	// en_GiftType_Turn = 8,	// 轮盘奖励
	en_GiftType_Welfare = 22, // 福利奖励
	en_GiftType_UserBuy = 21, // 充值附加奖励
	en_GiftType_Apptoqq = 23, // 添加到QQ面板
	en_GiftType_Subsidy1 = 24, // 领取救济卡奖励
	en_GiftType_Compensate = 9, // 系统补偿
};

// 礼物类型
enum Presents
{
	en_Present_Flower = 1,	// 鲜花
	en_Present_Tomato = 2,	// 番茄
	en_Present_Egg = 3,		// 鸡蛋
	en_Present_Beer = 4,	// 啤酒
	en_Present_Chook = 5,	// 鸡
	en_Present_Dog = 6,		// 狗
};
// 平台
enum Platform
{
	en_Platform_Tencent = 1004, // 腾讯
};

#endif // _PG_ENUM_H

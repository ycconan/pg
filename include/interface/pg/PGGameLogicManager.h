#ifndef _PG_GAMELOGIC_H_
#define _PG_GAMELOGIC_H_

#include "GameLogicManager.h"
#include "PGPubStruct.h"
class Effect;
class MissionModelPtr;

class PGGameLogicManager : public GameLogicManager, public Singleton<PGGameLogicManager>
{
public:
	PGGameLogicManager(void);
	
private:

};

#endif // _PG_GAMELOGIC_H_

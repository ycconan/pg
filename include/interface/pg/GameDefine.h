#ifndef _GAME_DEFINE_H_
#define _GAME_DEFINE_H_


////////////////////////////////////////////////////		// XML文件管理器
// #define _USE_DEFAULT_XML_DATA_MGR_		// 使用默认XML管理器,否则注释此定义

// #define XMLDATA_MGR_PATH	"PGXmlDataManager.h"
// #define LOGIC_MGR_PATH	"PGPublicLogic.h"
// #define LOG_MGR_PATH	"PGLogDataManager.h"
// #define PROTOCOLS_MGR_PATH	"PGProtocolsManager.h"
// #define ENUM_PATH	"PGEnum.h"

// #define	sGLMgr	sPubLogic
// #endif

#define START_PLAYER_NUM	4	// 开始游戏人数
#define _GAME_XMLFILE_PATH_		"../scripts_xml/pg"
#ifdef GAME_PATH
#undef GAME_PATH
#define GAME_PATH "pg"
#endif

#define ENUM_PATH	"PGEnum.h"

#define _NOUSE_DEFAULT_CHANNEL_ASSIGN__	// 不使用默认分配逻辑
// #define _USE_DEFAULT_GAME_LOGIC_MGR_	// 使用默认逻辑类,否则注释此定义

#define 	LOGIC_MGR_PATH		"PGPublicLogic.h"
#define		LOGIC_MGR_NAME		PublicLogic
#define		sGLMgr				LOGIC_MGR_NAME::getSingleton()

////////////////////////////////////////////////////

//#define _USE_DEFAULT_LOG_DATA_MGR_		// 使用默认日志管理器,否则注释此定义
#define 	LOG_MGR_PATH 		"PGLogDataManager.h"		// 日志管理器头路径,默认为:"LogDataManager.h"
#define 	LOG_MGR_NAME 		PGLogDataManager				// 日志管理器类名,默认为:LogDataManager
#define 	sLogDataMgr 		LOG_MGR_NAME::getSingleton()	

////////////////////////////////////////////////////
#define 	HALL_MGR_PATH 		"PGHallManager.h"		// "大厅\登陆\排行,处理"头文件


////////////////////////////////////////////////////		// 写包文件
#define 	PROTOCOLS_MGR_PATH	"PGProtocolsManager.h"
#define 	PROTOCOLS_MGR_NAME	PGProtocolsManager
#define		sProtocolsMgr		PROTOCOLS_MGR_NAME::getSingleton()

////////////////////////////////////////////////////		// XML文件管理器
// #define _USE_DEFAULT_XML_DATA_MGR_		// 使用默认XML管理器,否则注释此定义

#define 	XMLDATA_MGR_PATH	"PGXmlDataManager.h"
#define 	XMLDATA_MGR_NAME	PGXmlDataManager
#define		sXmlDataMgr			XMLDATA_MGR_NAME::getSingleton()

// #define 	GAMLOGIC_MGR_PATH		"PGGameLogicManager.h"
// #define		GAMLOGIC_MGR_NAME		PGGameLogicManager
// #define		sGLogicMgr			GAMLOGIC_MGR_NAME::getSingleton()

#endif

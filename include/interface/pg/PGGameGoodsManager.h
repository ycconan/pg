#ifndef _PG_GAMEGOODS_MANAGER_H
#define _PG_GAMEGOODS_MANAGER_H

#include "Common.h"
#include "String.h"
#include "Resource.h"
#include "ItemManager.h"

// 商品标志位
enum GoodsFlagDef
{
	GoodsFlag_Gold = 0x8, // 元宝购买
	GoodsFlag_Lottery = 0x10, // 兑换券购买
	GoodsFlag_ToGateway = 0x20, // 去网关
	GoodsFlag_Coins = 0x40, // 铜钱购买
};
// 标签类型
enum AssortType
{
	en_AT_Common = 1, // 一般商品
	en_AT_Equip= 2,	// 装备
	en_AT_Prop = 3, // 道具
	en_AT_Character = 4, // 角色类
	en_AT_RealObject = 5, // 实物兑换 
	en_AT_Honor = 6, // 勋章
};
// 商城类型
enum ShopType
{
	en_ShopT_GoldShop = 1, // 元宝商城
	en_ShopT_LotteryShop = 2, // 兑换券商城
	en_ShopT_Largess = 3,	// 系统赠送
};

#define WGS_GOODSMGR_INNER_MUTEX_NAME goodsMgr_inner_mutex
#define WGS_GOODSMGR_INNER_MUTEX mutable boost::mutex WGS_GOODSMGR_INNER_MUTEX_NAME;
#define WGS_GOODSMGR_INNER_LOCK		boost::lock_guard<boost::mutex> wgsGoodsMgrInnerMutexLock(WGS_GOODSMGR_INNER_MUTEX_NAME);

class GameGoodsManager : public Singleton<GameGoodsManager>
{
public:
	GameGoodsManager(void);
	~GameGoodsManager(void);
	
	void load(void);
	void Reload();
	/**获取商品属性
	*参数:
	*	model_id:物品模式号	buy_type:兑换货币类型,默认为元宝
	*/
	const GameGoods * GetGameGoods(const uint32 & model_id, const GoodsFlagDef & buy_type = GoodsFlag_Gold);
	/**
	 * 获取指定ID的商品
	 * 参数：
	 * 	@goodsId 商品ID
	 * 返回：商品
	 */
	const GameGoods * GetGameGoodsById(const uint32 & goodsId);
	
	/**获取商品价值
	*参数:
	*	model_id:物品模式号	buy_type:兑换货币类型
	*/
	uint32 GetGoodsPrice(const uint32 & model_id, const GoodsFlagDef & buy_type = GoodsFlag_Gold);
	/**
	 * 获取指定类型某页的商品列表
	 * 参数：
	 * 	@pGoodsList 存放获取的商品
	 *  @shopType 商城类型
	 *  @supId 运营商ID
	 *  @type 分类类型
	 *  @page 请求页数
	 *  @number 每页数量
	 * 返回：总页数
	 */
	uint8 GetGoodsList(std::list<GameGoods*> *pGoodsList, const uint8 shopType, const uint32 supId, const uint8 labelType, const uint8 page, const uint8 number);
protected:
	WGS_GOODSMGR_INNER_MUTEX
	std::list<GameGoods>  m_goodsList;
	// 游戏商城
	std::map<uint8, std::list<GameGoods*> > m_mapTypeGameGoods;
	// 兑换券商城
	std::list<GameGoods*> m_lstRealGoods;
};

#define   sGoodsMgr   GameGoodsManager::getSingleton()

#endif // _PG_GAMEGOODS_MANAGER_H

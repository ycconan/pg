#ifndef _PG_PUBSTRUCT_H
#define _PG_PUBSTRUCT_H

#include "Common.h"
#include "String.h"
#include "Character.h"
#include "Threading/Mutex.h"
#include "GameStruct.h"

// 数据排行对象
struct QueueTop
{
	uint32 id; // id
	String name; // 昵称
	String desc; // 头像url
	uint64 data; // 排名数据
	uint16 vipId;	// VIP_ID
};
//牌值
struct PaiGowType
{
	uint32 point;
	bool color;
	bool two;
};
//特殊牌型选项
struct SpecialType
{
	uint8 Istwj;
	uint8 Isdjnn;
	uint8 Isgz;
	uint8 IsZdy;
	void Init(uint8 iszdy,uint8 isdjnn,uint8 istwj,uint8 isgz)
	{
		Istwj = istwj;
		Isdjnn = isdjnn;
		Isgz = isgz;
		IsZdy = iszdy;
	}
};
// 产生的牌
struct Cards
{
	std::vector<PaiGowType> vecCards;
	uint32 firtype;
	uint32 firpoint;
	uint32 sectype;
	uint32 secpoint;

	Cards(){Init();}
	void Init();

	uint32 InsertCard(PaiGowType &card, SpecialType  &spety);
};
// 玩家信息
struct TablePlayer
{
	ResourceProxy player; 	// 玩家
	Cards cards;			// 牌
	Cards Originalcards;	//原始牌
	uint32 chipsNum;		// 总的下注筹码数
	uint8 isWin;			// 是否赢
	int curScore;			//当前积分
	int roundWins;			//每轮输赢
	uint8 iswinScore;		//赢得积分
	uint8 status;			//游戏状态
	uint8 pos;				//座位号	
	uint8 nAgreeSameIp;		//是否同意进行游戏
	uint8 cutresult;		//切牌选择结果
	uint8 cutcardfinsh;		//庄家是否完成切牌
	uint8 movecardresult;	//摆牌操作最终结果
	uint32 banker_count;	//当庄赢得得次数
	uint32 lose_count;		//游戏输的轮数
	uint32 win_count;		//赢的轮数
	uint32 timeout;			
	uint32 surplusScore;	//可下注积分
	uint32 curNum;			//当前道数
	bool cutcardflag;		//已切牌标志
	bool IsMovecard;		//是否换过牌
	bool IsLookcard;		//是否已看牌
	bool IsCalC;			//结算确认
	bool IsMovefinish;		//是否完成摆牌操作
	bool IsPrompt;			//是否已经提示 
	bool bFz;				//是否房主
	bool onceBanker;		//至少当过一次庄
	bool isOnceChin;        //下注过一次
	bool isChin;			//已全部下注
	bool isOffLine;			//是否离线
	bool isLeave;			//是否退出游戏
	TablePlayer() { Init(); everyInit();}
	//每轮初始化
	void Init();
	//开局初始化
	void everyInit();
	void InitPlayer();
	// 座位玩家投注
	void AddSeatChip(const uint32 seatPos, const uint32 addChip);
	//下注积分情况
	std::map<uint32, uint32> mapNumScore;
	std::vector<Cards> CorrectCardsType;

  private: 
};

#endif // _PG_PUBSTRUCT_H

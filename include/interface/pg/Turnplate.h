#ifndef _PG_TURNPLATE_H
#define _PG_TURNPLATE_H

#include "Common.h"
#include "Singleton.h"
#include "String.h"

class CharPtr;

#define 	RCOMBAT_NUM		"RTCOMBAT"	// 当前对局次数
#define 	RROUND_NUM		"RTROUND"	// 当天已转次数
#define		RSURPLUS_NUM	"RTSURPLUS"	// 当天积攒次数

class Turnplate : public Singleton<Turnplate>
{
public:
	Turnplate();
	~Turnplate();
	// 添加次数
	void AddCombat(CharPtr & pChr);
	// 转轮盘
	void Turn(CharPtr &pChr, uint32 &id);
	// 获取转盘ID
	uint32 GetTurnplatePos(CharPtr &pChr);
	// 领取奖励
	bool GetBounty(CharPtr &pChr, const uint32 id);
	// 重置领奖次数
	void ResetData();
	// 检查轮盘次数
	bool CheckCombat(CharPtr & pChr, const bool fromLogin = true);
private:
	std::map<uint32, uint32> m_mapShowNum; // 奖励出现次数
	std::vector<String> m_vecTopList;	// 得奖列表
};

#define   sTurnplate   Turnplate::getSingleton()

#endif // _PG_TURNPLATE_H

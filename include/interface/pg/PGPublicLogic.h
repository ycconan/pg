#ifndef _PG_PUBLICLOGIC_H
#define _PG_PUBLICLOGIC_H

#include "Common.h"
#include "String.h"
#include "Resource.h"
#include "Vector3.h"
#include "Threading/AtomicCounter.h"
#include "GameLogicManager.h"

class  WorldPacket;
class  CharPtr;
class  ItemPtr;
class  ChannelPtr;
class  Channel;
class  UserPtr;
class  EffectPtr;
class  TownPtr;
struct CardModel;
//struct UserAuthenticInfo;
struct game_exchange_tag_;
typedef game_exchange_tag_ GameExchange;

#define WGS_PLOGIC_INNER_MUTEX_NAME plogic_inner_mutex
#define WGS_PLOGIC_INNER_MUTEX mutable boost::mutex WGS_PLOGIC_INNER_MUTEX_NAME;
#define WGS_PLOGIC_INNER_LOCK		boost::lock_guard<boost::mutex> wgsPLogicInnerMutexLock(WGS_PLOGIC_INNER_MUTEX_NAME);

class PublicLogic : public GameLogicManager, public Singleton<PublicLogic>
{
WGS_PLOGIC_INNER_MUTEX
public:
	/** 为USER创建角色,若数据库里面有该USER绑定的角色则返回该角色 */
	CharPtr CreateCreature(UserPtr & user, bool isCenter = true);
	// 创建牌桌
	ChannelPtr CreateNewChannel(ResourceProxy town,const uint32 & march_id);
	// 获取元宝
	uint32 GetGoldMoney(CharPtr & chr);
	// 设置元宝
	void SetGoldMoney(CharPtr & chr, const uint32 gMoney);
	// 创建比赛
	uint32 	CreateGameMarch(CharPtr & chr, const String & info = "", const uint32 & def_inte = 0);
	/*********物品相关***************/
	// 创建物品
	ItemPtr CreateItem(CharPtr & container, const uint32 & model_id, const uint32 & num);
	// 获取物品
	ItemPtr GetItem(CharPtr & chr, const uint32 & model_id); 
	// 设置物品数量,如果没有当前物品,则自动增加新物品
	bool SetItemNum(CharPtr & chr, const uint32 & model_id, const uint32 & num);
	// 增加物品数量
	ItemPtr addItemNum(CharPtr & chr, const uint32 & model_id, const uint32 & addnum, bool bUpdate = true);
	// 减少物品数量
	bool reduceItemNum(CharPtr & chr, const uint32 & model_id, const uint32 & rNum, bool bUpdate = true);
	// 牌桌上增加物品数量
	ItemPtr TableAddItemNum(CharPtr & chr, const uint32 & model_id, const uint32 & addnum);
	// 牌桌上减少物品数量
	bool TableReduceItemNum(CharPtr & chr, const uint32 & model_id, const uint32 & rNum);
	// 获取某物品数量
	uint32 GetItemNum(CharPtr & chr, const uint32 & model_id);
	// 获取银行存款
	uint64 GetBankerNum(CharPtr & chr, const uint32 & model_id);
	// 获取物品总资产
	uint64 GetAllNum(CharPtr & chr, const uint32 & model_id);
	// 更新背包物品信息
	void UpdateItemsInfoInBag(CharPtr & chr);
	// 取得物品的名称
	String GetItemName(const uint32 & model_id);
	// 合并相同数据
	void CombItemData(CharPtr & pChr);
	/*******************************************************************************************/
	// 发送协议给玩家
	void SendProtocolsToChr(CharPtr & pChr, WorldPacket * packet);
	// 发送系统消息
	bool SendSystemMessage(CharPtr & chr, const char* pStr, const uint8 & type = 2);
	// 初始效果挂载
	void BindInitEffects(CharPtr & chr);
	// 添加效果
	EffectPtr AddEffect(ResourceProxy owner, 
						const uint32 & model_id, 
						const uint32 & begin_time,
						const uint32 & data1 = 0, 
						const uint32 & data2 = 0, 
						const uint32 & data3 = 0, 
						const uint32 & data4 = 0,
						const float & fdata1 = 0.0f,
						const float & fdata2 = 0.0f);
	// 获取效果								
	EffectPtr GetEffect(ResourceProxy owner, const uint32 & model_id);
	// 广播除某id以外的玩家
	void Broadcast(WorldPacket &packet, std::vector<CharPtr> &vecChars, const uint32 exceptId = 0);
	// 屏蔽玩家昵称
	bool FilterChars(String str, String &outStr);
	
	// 清除所有人的邮件
	bool CleanAllMails();
	// 清除个人邮件
	bool CleanUserMails(const uint32 userId);
	// 下发二级密码锁定消息
	void Pwd2LockedMessage(UserPtr &pUser, const uint32 leftTime);
	// 下发登录欢迎消息
	void LoginWellcomeMessage(UserPtr &pUser);
	// 领取活动卡
	uint8 CkExchageTicket(CharPtr &pChr, String &ticket);
	// 分区广播
	void WholeServerBroadcast(WorldPacket *pPacket, const uint32 gzId = 0);
	// 平台广播
	void WholeBroadcastByPlateId(WorldPacket *pPacket, const uint32 fromId = 0);
	// 大厅广播
	void HallBroadcast(WorldPacket * pPacket);
	// 全局广播
	void BroadcastEx(WorldPacket * pPacket);
	// 公告
	void SendNotice(String &strMsg, const uint32 type = 1, const uint32 gzId = 0);
	// 发消息给某个玩家
	void SendMsgToPlayer(const uint32 userId, String &strMsg, const uint32 type = 2);
	// 删除数据库敏感字符
	String RvRpDBCharacter(String &str, bool bRemove = true);
	// 添加成就
	bool AddCreatureTitle(CharPtr & pChr, EffectPtr & pEffect);
	// 效果计数更新，返回是否可以继续统计
	bool CountEffectNumber(EffectPtr &pEffect, CharPtr &pChr);
	// 检测是否超过每日输赢上限
	bool CheckOverLimitPerDay(CharPtr &pChr, const bool bSendMsg = true);
	
private:
	// 更新char表金钱
	void UpdateAboutMoney(CharPtr &pChr, const uint32 modelId, const uint32 num, bool bSaveDB);
	// 新手包
	void AddNewPlayerGift(CharPtr &pChr, const bool isCenter);
};

#define   sPubLogic   PublicLogic::getSingleton()


#endif // _PG_PUBLICLOGIC_H

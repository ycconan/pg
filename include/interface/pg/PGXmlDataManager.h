/******************************
 * @作用 管理XML数据
 * @创建人 李志勇 
 * @创建日期 2011-10-22
 *****************************/
#ifndef _PG_XMLDATAMANAGER_H
#define _PG_XMLDATAMANAGER_H

#include "Common.h"
#include "Singleton.h"
#include "String.h"
#include "XmlDataManager.h"


#define WGS_XML_READ_WRITE_MUTEX_NAME      xml_read_write_shared_mutex
#define WGS_XML_READ_WRITE_MUTEX   mutable boost::shared_mutex WGS_XML_READ_WRITE_MUTEX_NAME;
#define WGS_XML_READ_LOCK             boost::shared_lock<boost::shared_mutex>  wgsXmlSharedMutexLock(WGS_XML_READ_WRITE_MUTEX_NAME);
#define WGS_XML_WRITE_LOCK 		boost::unique_lock<boost::shared_mutex>  wgsXmlWriteMutexLock(WGS_XML_READ_WRITE_MUTEX_NAME); 

// 等级相关配置数据
struct AboutLevel
{
	uint32 lv;
	uint32 exps;
	uint32 loginHR; // 登录奖励
	uint32 levelUpHR; // 升级奖励
	
	AboutLevel():lv(0),exps(0),loginHR(0),levelUpHR(0){}
};
// 礼物
struct Present
{
	uint32 id;
	uint32 modelId;
	uint32 price;
	Present():id(0),modelId(0),price(0){}
};
// 黄钻奖励
struct DiamondLgBounty
{
	uint32 level;
	uint32 coins;
	DiamondLgBounty(void):level(0),coins(0){}
};
// 筹码
struct Chip
{
	uint32 value;
	uint32 min;
	uint32 max;
	
	Chip():value(0),min(0),max(0){}
};
// 在线奖励
struct OnlineBounty
{
	uint32 id;
	uint32 second;
	uint32 bounty;
	OnlineBounty():id(0),second(0),bounty(0){}
};

class TiXmlElement;
class WorldPacket;
class CharPtr;
struct Notice;

class PGXmlDataManager : public XmlDataManager, public Singleton<PGXmlDataManager>
{
	WGS_XML_READ_WRITE_MUTEX
public:

	PGXmlDataManager();
	~PGXmlDataManager();
	/**********************等级相关***************************/
	// 获取最大等级
	const uint32 GetMaxLevel();
	// 获取升到指定等级的经验
	const uint32 GetExpsOfLevel(const uint32 level);
	// 获取相应等级的升级奖励
	const uint32 GetLevelUpGiftOfLevel(const uint32 level);
	// 获取相应等级登录奖励
	const uint32 GetLoginGiftOfLevel(const uint32 level);
	/*********************************************************/
	// 获取配置信息
	// uint32 	GetConfXMLValue(const String & str);
	// 获取系统公告
	// inline const std::list<Notice> & GetSystemNotice() const {return m_lstNotices;}
	/************************关键字相关**************************/
	// 过滤关键字
	void FilterKeyWords(String &strContent);
	// 检查是否存在关键字
	bool IsExistedKeyWords(String &strContent);
	/*********************************************************/
	// 获取指定ID礼物
	Present GetPresentById(const uint32 id);
	// 获取指定黄钻等级的登录奖励
	uint32 GetDiamondLevelBounty(const uint32 lv);
	// 获取分区ID
	String GetGzIdName(const uint32 id);
	// 获取轮盘奖励
	// const Roulette *GetRoulette(void);
	// bool GetRoulette(const uint32 id, Roulette &roulette);
	// TurnplateConf GetTPConf();
	// 检查筹码是否合法
	bool CheckChips(const uint32 chip, const uint32 wealth);
	// 随机获取一个筹码
	uint32 RandomGetChip();
	// 获取游戏分区ID的vector
	// std::map<String, GZIDInfo> GetGZIDInfo(){ return m_mapGzidInfo; }
	// 获取新手礼包
	const std::map<uint32, uint32> & GetNewPlayerGiftList();
	/**********************在线奖励相关**************************/
	// 获取当前在线时间段的奖励
	const uint32 GetBountyOfOnline(const uint32 id);
	// 获取当前在线时间段的在线时长
	const uint32 GetSecondsOfOnline(const uint32 id);
	// 获取最大ID
	const uint32 GetMaxOnlineId();
	/*********************************************************/
	// //购买赠送小物品
	// const BuyLargess *GetBuyLargess(const uint32 & buy_id);
	// // 乐码赠送配置
	// std::vector<LeCardGive> getGiveLeCardsInfo(const uint32 & reg_days);
public:
	// 重载等级经验列表
	bool ReloadLevelExpList();
	// 重载游戏配置文件
	// bool ReloadGameConfig();
	// 重新加载系统公告
	// bool ReloadSystemNotices();
	// 重载关键字
	bool ReloadKeyWords();
	// 重载昵称关键字
	bool ReloadNickKeyWords();
	// 重新加载礼物
	bool ReloadPresents();
	// 重新加载黄钻奖励
	bool ReloadDiamondLogin();
	// 重新加载分区名字
	bool ReloadGZIDName();
	// 重新加载轮盘赌配置
	// bool ReloadRoulette(void);
	// 重新加载筹码配置
	bool ReloadChips(void);
	// 加载游戏分区ID配置
	// bool ReLoadGameIDXml();
	// 重新加载新手礼包
	bool ReloadNewPlayerGift();
	// 重载在线奖励配置
	bool ReloadOnlineBounty();
	// //购买赠送小物品
	// bool LoadBuyLargessXml();
	// // 加载赠送打折卡配置
	// bool ReLoadGiveLeCardsXml();
	
private:
	// 加载等级经验列表
	bool LoadLevelExpList();
	// 加载游戏配置文件
	// bool LoadGameConfig();
	// 加载系统公告
	// bool LoadSystemNotices();
	// 加载关键字
	bool LoadKeyWords();
	// 加载昵称关键字
	bool LoadNickKeyWords();
	// 加载礼物出售信息
	bool LoadPresents();
	// 加载黄钻每日登录奖励
	bool LoadDiamondLogin();
	// 加载分区名字
	bool LoadGZIDName();
	// 加载轮盘赌配置
	// bool LoadRoulette(void);
	// 加载筹码配置
	bool LoadChips(void);
	// 加载游戏分区ID配置
	// bool LoadGameIDXml();
	// 加载新手礼包
	bool LoadNewPlayerGift();
	// 加载在线奖励配置
	bool LoadOnlineBounty();
	// // 加载赠送打折卡配置
	// bool LoadGiveLeCardsXml();
	
private:
	// 等级相关
	std::map<uint8, AboutLevel> m_mapAboutLevel;
	// 游戏配置XML
	// std::map<String, uint32> m_mapGameConfig;
	// 系统公告
	// std::list<Notice> m_lstNotices;
	// 关键字
	std::map<String, String> m_mapKeyWordsFilter;
	// 昵称关键字
	std::map<String, String> m_mapNickKeyWordsFilter;
	// 礼物
	std::map<uint32, Present> m_mapPresents;
	// 黄钻奖励
	std::map<uint32, DiamondLgBounty> m_mapDiamonds;
	// 分区名字
	std::map<uint32, String> m_mapGZIDName;
	// // 轮盘赌物品列表
	// std::list<Roulette> m_listRoulettes;
	// TurnplateConf m_tpConf;
	// 筹码
	std::map<uint32, Chip> m_mapChips;
	// 游戏分区ID
	// std::map<String, GZIDInfo> m_mapGzidInfo;
	// 新手礼包
	std::map<uint32, uint32> m_mapNewPlayerGift;
	// 在线奖励
	std::map<uint32, OnlineBounty> m_mapOnlineBounty;
	// // 购买赠送小物品
	// std::list<BuyLargess> m_lstBuyLargess;
	// // 赠送打折卡列表
	// std::list<LeCardGive> m_lstLeCardsGive;
};

 //#define sXmlDataMgr PGXmlDataManager::getSingleton()

#endif // _PG_XMLDATAMANAGER_H
